import operator
from dataclasses import dataclass
from functools import reduce

from vminstructions import Instruction

_debug_repr = False

def enable_type_debug_repr():
    global _debug_repr
    _debug_repr = True

class Type:
    def __repr__(self):
        out = ""
        for k, v in self.__dict__.items():
            out += f"{v} "
        out = out[:-1]  # trim trailing space
        return out

    def serialize(self):
        try:
            name = getattr(self, "_serde_name")
        except AttributeError:
            name = str(self.__class__)

        out = []
        for k, v in self.__dict__.items():
            if isinstance(v, Type):
                out.append(v.serialize())
            else:
                out.append(v)
        return {name: out[0] if len(out) == 1 else out}

    @classmethod
    def deserialize(cls, code, arg):
        if code == "n":
            return Nil()
        elif code == "e":
            return Empty()
        elif code == "i":
            return Integer(arg)
        elif code == "f":
            return Float(arg)
        elif code == "s":
            return String(arg)
        elif code == "b":
            return Byte(arg)
        elif code == "v":
            return Vector([Type.deserialize(list(a.keys())[0], list(a.values())[0]) for a in arg])
        elif code == "l":
            return List([Type.deserialize(list(a.keys())[0], list(a.values())[0]) for a in arg])
        elif code == "h":
            raise TypeError("Cannot deserialize hashmaps yet")
        elif code == "fn":
            return Function(*arg)
        elif code == "cl":
            return Closure(Type.deserialize("fn", arg[0]), arg[1])
        elif code == "cd":
            return Closed(arg)
        elif code == "ue":
            return UnreadExec(arg)
        else:
            raise TypeError(f"Unknown type code '{code}'")

class Nil(Type):
    _serde_name = "n"

    def __repr__(self):
        return "<nil>"

class Unbound(Type):
    _serde_name = "ub"

    def __repr__(self):
        return "<unbound>"

class Empty(Type):
    _serde_name = "e"

    def __repr__(self):
        return "()"

@dataclass(repr=False)
class Integer(Type):
    _serde_name = "i"
    value: int

@dataclass(repr=False)
class Float(Type):
    _serde_name = "f"
    value: float

@dataclass(repr=False)
class String(Type):
    _serde_name = "s"
    value: str

@dataclass(repr=False)
class Byte(Type):
    _serde_name = "b"
    value: chr

@dataclass(repr=False)
class Vector(Type):
    _serde_name = "v"
    value: list[Type]

    def serialize(self):
        return {"v": [k.serialize() for k in self.value]}

@dataclass(repr=False)
class List(Type):
    _serde_name = "l"
    value: list[Type]

    def __repr__(self):
        out = "("
        cur = self
        while True:
            out += repr(cur.value[0])
            cur = cur.value[1]
            if isinstance(cur, List) and (isinstance(cur.value[1], List) or \
                                          isinstance(cur.value[1], Empty)):
                out += " "
                continue
            elif isinstance(cur, Empty):
                break
            else:
                out += f" {repr(cur)}"
                break
        out += ")"
        return out

@dataclass(repr=False)
class HashMap(Type):
    _serde_name = "h"
    value: {String: Type}

@dataclass(repr=False)
class Function(Type):
    _serde_name = "fn"
    name: String
    arity: Integer
    localct: Integer
    bytecode: list[Instruction]
    consts: list[Type]

    def __repr__(self):
        if _debug_repr:
            return f"func {self.name}:\ncode: {self.bytecode}\nconsts: {self.consts}"
        else:
            return f"#<function {self.name}>"

    def serialize(self):
        return {self._serde_name: [self.name, self.arity, self.localct,
                reduce(operator.add, (x.serialize() for x in self.bytecode)),
                [x.serialize() for x in self.consts]]}

@dataclass(repr=False)
class Closure(Type):
    _serde_name = "cl"
    func: Function
    ctx: list[Type]

    def __repr__(self):
        if _debug_repr:
            return f"closure {self.func}, {self.ctx}"
        else:
            return f"#<closure of {self.func.name}>"

@dataclass(repr=False)
class Closed(Type):
    _serde_name = "cd"
    value: tuple[int, int, int]

@dataclass(repr=False)
class Frame(Type):
    _serde_name = "fr"

@dataclass(repr=False)
class UnreadExec(Type):
    _serde_name = "ue"
    sym: str

    def __repr__(self):
        return f"#<unread-exec \"{self.sym}\">"
