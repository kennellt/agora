from dataclasses import dataclass
from enum import Enum
from typing import Union

from code import *
from vminstructions import *
from vmtypes import *

class ClosedValueFrom(Enum):
    Argument = 0
    Local = 1
    Global = 2
    Duplicate = 3

@dataclass
class ClosedValue:
    sym: str
    locat: ClosedValueFrom
    param: Union[int, str]

    def __eq__(self, other):
        return self.sym == other.sym

class FuncArgumentError(Exception):
    pass

class FuncFrame:
    def __init__(self, func, args, locals, init_consts=None):
        self.func = func
        self.args = {a: idx for idx,a in enumerate(args)}
        self.locals = {l: idx for idx,l in enumerate(locals)}
        self.closed = []

    def get_arg_pos(self, arg):
        if arg not in self.args:
            raise FucnArgumentError(f"invalid argument `{arg}'")
        else:
            return self.args[arg]

    def needs_closure(self):
        return len(self.closed) > 0

class FuncStackError(Exception):
    pass

class FuncStack:
    def __init__(self, init_func=None):
        self.stack = []
        if init_func:
            self.push(init_func)

    def __len__(self):
        return len(self.stack)

    def push(self, ff: FuncFrame):
        if len(self.stack) == 255:
            raise FuncStackError("call is stack too deep")
        self.stack.append(ff)

    def pop(self):
        return self.stack.pop(-1)

    def peek(self):
        return self.stack[-1]

    def _check_add_closed(self, idx, cl):
        if cl not in self.stack[idx].closed:
            self.stack[idx].closed.append(cl)

        for j in range(idx + 1, len(self.stack)):
            dup = ClosedValue(cl.sym, ClosedValueFrom.Duplicate, self.stack[j-1].closed.index(cl))
            if cl in self.stack[j].closed:
                self.stack[j].closed[self.stack[j].closed.index(cl)] = dup
            else:
                self.stack[j].closed.append(dup)

    def read_sym(self, sym, dst_reg):
        source = self.stack[-1]
        for idx,ff in reversed(list(enumerate(self.stack))):
            if sym in ff.args and idx < len(self.stack) - 1:  # need to close
                cl = ClosedValue(sym, ClosedValueFrom.Argument, ff.args[sym])
                # add one to level index since we create ht closure outside of its own scope
                self._check_add_closed(idx + 1, cl)
                return Code(LoInst(dst_reg, source.closed.index(cl)), out=dst_reg)
            elif sym in ff.args:  # regular argument load
                return Code(LaInst(dst_reg, ff.args[sym], 0), out=dst_reg)
            elif sym in ff.locals and idx < len(self.stack) - 1:  # need to close
                cl = ClosedValue(sym, ClosedValueFrom.Local, ff.locals[sym])
                self._check_add_closed(idx + 1, cl)
                return Code(LoInst(dst_reg, source.closed.index(cl)), out=dst_reg)
            elif sym in ff.locals:  # regular local load
                return Code(LlInst(dst_reg, ff.locals[sym], 0), out=dst_reg)
            else:
                continue

        if self.stack.index(source) == 0:  # global scope
            return Code([LcInst(dst_reg, self.add_const(String(sym))),
                         LdInst(dst_reg, dst_reg)], out=dst_reg)
        else:
            cl = ClosedValue(sym, ClosedValueFrom.Global, sym)
            if cl not in source.closed:
                source.closed.append(cl)
            return Code(LoInst(dst_reg, source.closed.index(cl)), out=dst_reg)

    def write_sym(self, sym, src_reg, tmp_reg):
        source = self.stack[-1]
        for idx,ff in reversed(list(enumerate(self.stack))):
            if sym in ff.args and idx < len(self.stack) - 1:  # need to close
                cl = ClosedValue(sym, ClosedValueFrom.Argument, ff.args[sym])
                self._check_add_closed(idx + 1, cl)
                return Code(SoInst(src_reg, source.closed.index(cl)))
            elif sym in ff.args:  # regular argument store
                return Code(SaInst(src_reg, ff.args[sym], 0))
            elif sym in ff.locals and idx < len(self.stack) - 1:  # need to close
                cl = ClosedValue(sym, ClosedValueFrom.Local, ff.locals[sym])
                self._check_add_closed(idx + 1, cl)
                return Code(SoInst(src_reg, source.closed.index(cl)))
            elif sym in ff.locals:
                return Code(SlInst(src_reg, ff.locals[sym], 0))
            else:
                continue

        if self.stack.index(source) == 0:  # global scope
            return Code([LcInst(tmp_reg, self.add_const(String(sym))),
                         StInst(src_reg, tmp_reg)])
        cl = ClosedValue(sym, ClosedValueFrom.Global, sym, False)
        if cl not in source.closed:
            source.closed.append(cl)
        return Code(SoInst(src_reg, source.closed.index(cl)))

    def add_const(self, const):
        if const not in self.stack[-1].func.consts:
            self.stack[-1].func.consts.append(const)
        return self.stack[-1].func.consts.index(const)

    def get_const_pool_len(self):
        return len(self.stack[-1].func.consts)

    def find_symbol(self, sym):
        frame = self.stack[-1]
        closed_syms = [cl.sym for cl in frame.closed]
        if sym in frame.args:
            return ("a", frame.args[sym])
        elif sym in frame.locals:
            return ("l", frame.locals[sym])
        elif sym in closed_syms:
            return ("c", closed_syms.index(sym))
        else:
            return ("g", sym)
