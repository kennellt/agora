#! /usr/bin/env python3

import msgpack
import operator
import sys

from vminstructions import *
from vmopcodes import *
from vmtypes import *

class ExecBlob:
    def __init__(self, version, ident, func):
        self.version = version
        self.ident = ident
        self.func = func

    def __repr__(self):
        return f"ExecBlob<{self.version}, {self.ident}, {self.func}>"

    def serialize(self):
        return [self.version, self.ident, list(self.func.serialize().values())[0]]

class InputError(Exception):
    pass

def parse_input(text):
    lines = [x.split(";")[0].strip() for x in text]
    lines = [x.split(" ") for x in lines if len(x) > 0]
    meta = list(filter(lambda x: x[0] == ".meta", lines))
    rest = list(filter(lambda x: x[0] != ".meta", lines))

    version = 0
    ident = ""
    entry = "$entry"

    for m in meta:
        if m[1] == "version":
            version = force_eight(convert_int(m[2]))
        elif m[1] == "id":
            ident = " ".join(m[2:])
        elif m[1] == "func":
            entry = m[2]

    funcs = {}
    for i in range(len(rest)):
        if rest[i][0] == ".funcstart":
            if len(rest[i]) != 4:
                raise InputError("malformed .funcstart directive")
            name = rest[i][1]
            arity = force_eight(convert_int(rest[i][2]))
            localct = force_eight(convert_int(rest[i][3]))
            if name in funcs:
                raise InputError(f"{name}: defined twice")

            instrs = []
            consts = []
            end = False
            for j in range(i+1, len(rest)):
                if rest[j][0] == ".funcend":
                    end = True
                    break
                elif rest[j][0] == ".const":
                    if len(rest[j]) < 2:
                        raise InputError(f"{name}: improper const definition")
                    consts.append(eval(" ".join(rest[j][1:])))
                elif rest[j][0] == ".funcref":
                    if len(rest[j]) != 2:
                        raise InputError(f"{name}: improper funcref definition")
                    consts.append(funcs[rest[j][1]])
                else:
                    instrs.append(opcode_map[rest[j][0]](*rest[j][1:]))
            if not end:
                raise InputError(f"{name}: unterminated function")
            i += j
            funcs[name] = Function(name, arity, localct, instrs, consts)

    if entry not in funcs:
        raise InputError("no entry function defined")

    return ExecBlob(version, ident, funcs[entry])

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: asm.py INPUT [OUTPUT]")
        sys.exit(1)

    infile = sys.argv[1]
    outfile = sys.argv[2] if len(sys.argv) >= 3 else "asm.out"

    with open(infile) as fp:
        exe = parse_input(fp.readlines())

    with open(outfile, "wb") as fp:
        fp.write(msgpack.dumps(exe.serialize()))
    # TODO: handle jump offsets in a sane manner
