from enum import Enum

class OpCode(Enum):
    Nop = 0x00

    Add = 0x10
    Subtract = 0x11
    Multiply = 0x12
    Divide = 0x13
    Modulo = 0x14

    And = 0x20
    Or = 0x21
    Not = 0x22
    Xor = 0x23

    Less = 0x30
    Equal = 0x31
    Greater = 0x32
    LessEqual = 0x33
    GreaterEqual = 0x34
    NotEqual = 0x35
    CmpType = 0x36

    Car = 0x40
    Cdr = 0x41
    Cons = 0x42

    Push = 0x50
    Pop = 0x51

    LoadConst = 0x60
    LoadSym = 0x61
    StoreSym = 0x62
    LoadRet = 0x63
    LoadArg = 0x64
    StoreArg = 0x65
    LoadLocal = 0x66
    StoreLocal = 0x67
    Export = 0x68
    Move = 0x69
    CloseSym = 0x6A
    CloseArg = 0x6B
    CloseLocal = 0x6C
    LoadClosed = 0x6D
    StoreClosed = 0x6E

    Get = 0x70
    Set = 0x71
    Insert = 0x72
    Append = 0x73
    Remove = 0x74
    Length = 0x75

    Call = 0x80
    TailCall = 0x81
    Return = 0x82
    Jump = 0x83
    JumpIf = 0x84
    JumpImm = 0x85
    JumpImmIf = 0x86

    New = 0x90
    StackVec = 0x91
    CloseFunc = 0x92
    LitInt = 0x93
    DupClosed = 0x94

    LockSym = 0xa0
    UnlockSym = 0xa1
    LockClosed = 0xa2
    UnlockClosed = 0xa3

    PrintReg = 0xf0
