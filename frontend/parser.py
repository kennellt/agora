from lexer import Token, TokenType
from quote import Quote

class ParserError(Exception):
    pass

class Parser:
    def __init__(self, lex):
        self.lex = lex

    def _eat_whitesp(self):
        tok = self.lex.lex()
        while tok.tp in {TokenType.WHITESPACE, TokenType.COMMENT}:
            tok = self.lex.lex()
        return tok

    def _lparen(self):
        ret = []
        while True:
            tok = self._eat_whitesp()
            if tok.tp == TokenType.LPAREN:
                ret.append(self._lparen())
            elif tok.tp == TokenType.RPAREN:
                return ret
            elif tok.tp == TokenType.SINGLEQUOTE:
                ret.append(self._squote())
            elif tok.tp == TokenType.DOUBLEQUOTE:
                ret.append(self._dquote())
            elif tok.tp == TokenType.QUASIQUOTE:
                ret.append(self._qquote())
            elif tok.tp == TokenType.COMMA:
                ret.append(self._comma())
            elif tok.tp in {TokenType.INTEGER, TokenType.FLOAT, TokenType.SYMBOL}:
                ret.append(Quote(tok.obj))
            elif tok.tp == TokenType.COMMENT:
                continue
            else:
                raise ParserError(f"bad token {repr(tok)}")

    def _multiplex_quote(self, qtp, ret):
        if qtp == TokenType.SINGLEQUOTE:
            return [Quote("quote"), ret]
        elif qtp == TokenType.QUASIQUOTE:
            return [Quote("quasiquote"), ret]
        elif qtp == TokenType.COMMA:
            return [Quote("unquote"), ret]
        else:
            raise ParserError(f"unknown quote type '{qtp}'")

    def _quote_handler(self, qtp):
        tok = self._eat_whitesp()
        if tok.tp == TokenType.LPAREN:
            return self._multiplex_quote(qtp, self._lparen())
        elif tok.tp == TokenType.RPAREN:
            raise ParserError(f"unmatched rparen")
        elif tok.tp in {TokenType.SINGLEQUOTE, TokenType.QUASIQUOTE}:
            return self._multiplex_quote(qtp, self._quote_handler(tok.tp))
        elif tok.tp == TokenType.DOUBLEQUOTE:
            return self._dquote()
        elif tok.tp in {TokenType.INTEGER, TokenType.FLOAT, TokenType.SYMBOL}:
            return self._multiplex_quote(qtp, Quote(tok.obj))
        else:
            raise ParserError(f"unable to quote type '{tok.tp}'")

    def _squote(self):
        return self._quote_handler(TokenType.SINGLEQUOTE)

    def _qquote(self):
        return self._quote_handler(TokenType.QUASIQUOTE)

    def _comma(self):
        return self._quote_handler(TokenType.COMMA)

    def _dquote(self):
        tok = self.lex.lex()
        if tok.tp != TokenType.STRING:
            raise ParserError("expected string after double quote")
        nxt = self.lex.lex() # eat double quote
        if nxt.tp != TokenType.DOUBLEQUOTE:
            raise ParserError("expected closing double quote")
        return tok.obj

    def reset(self):
        self.lex.reset()

    def parse(self):
        tok = self._eat_whitesp()

        if tok.tp == TokenType.LPAREN:
            return self._lparen()
        elif tok.tp == TokenType.RPAREN:
            raise ParserError("unmatched rparen")
        elif tok.tp == TokenType.SINGLEQUOTE:
            return self._squote()
        elif tok.tp == TokenType.DOUBLEQUOTE:
            return self._dquote()
        elif tok.tp == TokenType.QUASIQUOTE:
            return self._qquote()
        elif tok.tp == TokenType.COMMA:
            return self._comma()
        elif tok.tp in {TokenType.INTEGER, TokenType.FLOAT, TokenType.SYMBOL}:
            return Quote(tok.obj)
        else:
            raise ParserError(f"unable to match token '{tok.tp}'")

if __name__ == "__main__":
    import sys
    from lexer import Lexer, Stream

    stream = Stream(sys.stdin)
    lexer = Lexer(stream)
    parser = Parser(lexer)

    while True:
        print("> ", end="", flush=True)
        try:
            print(repr(parser.parse()))
        except EOFError:
            break
