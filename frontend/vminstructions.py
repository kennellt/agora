from enum import Enum
from vmopcodes import *

def convert_int(st):
    if isinstance(st, str) and st.startswith("0x"):
        return int(st, base=16)
    else:
        return int(st)

class ConvertError(Exception):
    pass

def force_eight(val):
    if val > 127:
        raise ConvertError(f"{val} is greater than 127")
    elif val < -128:
        raise ConvertError(f"{val} is less than -128")
    elif val >= 0:
        return val
    else:
        return 256 + val

class Operand(Enum):
    REGISTER = 0
    IMMEDIATE = 1

class InstructionError(Exception):
    pass

class Instruction:
    opcode = OpCode.Nop
    operand_types = []
    size = 1

    def __init__(self, *opers):
        if len(opers) != len(self.operand_types):
            raise InstructionError(f"{self.opcode}: operand count mismatch")

        self.operands = []

        for idx, (a, b) in enumerate(zip(self.operand_types, opers)):
            if a is Operand.REGISTER:
                if b[0] == "r":
                    reg = int(b[1:])
                    if 0 <= reg <= 255:
                        self.operands.append(reg)
                    else:
                        raise InstructionError(f"{self.opcode}: invalid register at position {idx}")
                else:
                    raise InstructionError(f"{self.opcode}: expected register in position {idx}")
            elif a is Operand.IMMEDIATE:
                if isinstance(b, int) or (isinstance(b, str) and b[0] != "r"):
                    self.operands.append(force_eight(convert_int(b)))
                else:
                    raise InstructionError(f"{self.opcode}: expected immediate in position {idx}")

    def __repr__(self):
        out = f"{self.opcode.name} "
        for op in zip(self.operand_types, self.operands):
            if op[0] == Operand.REGISTER:
                out += f"r{op[1]} "
            else:
                out += f"{op[1]} "
        return out

    def serialize(self):
        return [self.opcode.value, *self.operands]

class NopInst(Instruction):
    pass

class OneRegInst(Instruction):
    operand_types = [Operand.REGISTER]
    size = 2

class PushInst(OneRegInst):
    opcode = OpCode.Push
class PopInst(OneRegInst):
    opcode = OpCode.Pop
class LrInst(OneRegInst):
    opcode = OpCode.LoadRet
class RetInst(OneRegInst):
    opcode = OpCode.Return
class JInst(OneRegInst):
    opcode = OpCode.Jump
class PrInst(OneRegInst):
    opcode = OpCode.PrintReg
class UlsInst(OneRegInst):
    opcode = OpCode.UnlockSym

class JimInst(Instruction):
    operand_types = [Operand.IMMEDIATE]
    opcode = OpCode.JumpImm
    size = 2
class UlcInst(Instruction):
    operand_types = [Operand.IMMEDIATE]
    opcode = OpCode.UnlockClosed
    size = 2

class TwoRegInst(Instruction):
    operand_types = [Operand.REGISTER, Operand.REGISTER]
    size = 3

class NotInst(TwoRegInst):
    opcode = OpCode.Not
class CarInst(TwoRegInst):
    opcode = OpCode.Car
class CdrInst(TwoRegInst):
    opcode = OpCode.Cdr
class LdInst(TwoRegInst):
    opcode = OpCode.LoadSym
class StInst(TwoRegInst):
    opcode = OpCode.StoreSym
class MovInst(TwoRegInst):
    opcode = OpCode.Move
class CsInst(TwoRegInst):
    opcode = OpCode.CloseSym
class AppInst(TwoRegInst):
    opcode = OpCode.Append
class LenInst(TwoRegInst):
    opcode = OpCode.Length
class CallInst(TwoRegInst):
    opcode = OpCode.Call
class TcallInst(TwoRegInst):
    opcode = OpCode.TailCall
class JifInst(TwoRegInst):
    opcode = OpCode.JumpIf
class SvInst(TwoRegInst):
    opcode = OpCode.StackVec
class LksInst(TwoRegInst):
    opcode = OpCode.LockSym

class LcInst(Instruction):
    operand_types = [Operand.REGISTER, Operand.IMMEDIATE]
    opcode = OpCode.LoadConst
    size = 3
class JimifInst(Instruction):
    operand_types = [Operand.IMMEDIATE, Operand.REGISTER]
    opcode = OpCode.JumpImmIf
    size = 3
class NewInst(Instruction):
    operand_types = [Operand.REGISTER, Operand.IMMEDIATE]
    opcode = OpCode.New
    size = 3
class LiInst(Instruction):
    operand_types = [Operand.REGISTER, Operand.IMMEDIATE]
    opcode = OpCode.LitInt
    size = 3
class DcInst(Instruction):
    operand_types = [Operand.REGISTER, Operand.IMMEDIATE]
    opcode = OpCode.DupClosed
    size = 3
class LoInst(Instruction):
    operand_types = [Operand.REGISTER, Operand.IMMEDIATE]
    opcode = OpCode.LoadClosed
    size = 3
class SoInst(Instruction):
    operand_types = [Operand.REGISTER, Operand.IMMEDIATE]
    opcode = OpCode.StoreClosed
    size = 3
class LkcInst(Instruction):
    operand_types = [Operand.REGISTER, Operand.IMMEDIATE]
    opcode = OpCode.LockClosed
    size = 3

class ThreeRegInst(Instruction):
    operand_types = [Operand.REGISTER, Operand.REGISTER, Operand.REGISTER]
    size = 4

class AddInst(ThreeRegInst):
    opcode = OpCode.Add
class SubInst(ThreeRegInst):
    opcode = OpCode.Subtract
class MulInst(ThreeRegInst):
    opcode = OpCode.Multiply
class DivInst(ThreeRegInst):
    opcode = OpCode.Divide
class ModInst(ThreeRegInst):
    opcode = OpCode.Modulo
class AndInst(ThreeRegInst):
    opcode = OpCode.And
class OrInst(ThreeRegInst):
    opcode = OpCode.Or
class XorInst(ThreeRegInst):
    opcode = OpCode.Xor
class LtInst(ThreeRegInst):
    opcode = OpCode.Less
class EqInst(ThreeRegInst):
    opcode = OpCode.Equal
class GtInst(ThreeRegInst):
    opcode = OpCode.Greater
class LeInst(ThreeRegInst):
    opcode = OpCode.LessEqual
class GeInst(ThreeRegInst):
    opcode = OpCode.GreaterEqual
class NeInst(ThreeRegInst):
    opcode = OpCode.NotEqual
class CtInst(ThreeRegInst):
    opcode = OpCode.CmpType
class ConsInst(ThreeRegInst):
    opcode = OpCode.Cons
class ExInst(ThreeRegInst):
    opcode = OpCode.Export
class GetInst(ThreeRegInst):
    opcode = OpCode.Get
class SetInst(ThreeRegInst):
    opcode = OpCode.Set
class CfInst(ThreeRegInst):
    opcode = OpCode.CloseFunc

class LaInst(Instruction):
    operand_types = [Operand.REGISTER, Operand.IMMEDIATE, Operand.IMMEDIATE]
    opcode = OpCode.LoadArg
    size = 4
class SaInst(Instruction):
    operand_types = [Operand.REGISTER, Operand.IMMEDIATE, Operand.IMMEDIATE]
    opcode = OpCode.StoreArg
    size = 4
class LlInst(Instruction):
    operand_types = [Operand.REGISTER, Operand.IMMEDIATE, Operand.IMMEDIATE]
    opcode = OpCode.LoadLocal
    size = 4
class SlInst(Instruction):
    operand_types = [Operand.REGISTER, Operand.IMMEDIATE, Operand.IMMEDIATE]
    opcode = OpCode.StoreLocal
    size = 4
class CaInst(Instruction):
    operand_types = [Operand.REGISTER, Operand.IMMEDIATE, Operand.IMMEDIATE]
    opcode = OpCode.CloseArg
    size = 4
class ClInst(Instruction):
    operand_types = [Operand.REGISTER, Operand.IMMEDIATE, Operand.IMMEDIATE]
    opcode = OpCode.CloseLocal
    size = 4

opcode_map = {
    "nop": NopInst,

    "add": AddInst,
    "sub": SubInst,
    "mul": MulInst,
    "div": DivInst,
    "mod": ModInst,

    "and": AndInst,
    "or": OrInst,
    "not": NotInst,
    "xor": XorInst,

    "lt": LtInst,
    "eq": EqInst,
    "gt": GtInst,
    "le": LeInst,
    "ge": GeInst,
    "ne": NeInst,
    "ct": CtInst,

    "push": PushInst,
    "pop": PopInst,

    "car": CarInst,
    "cdr": CdrInst,
    "cons": ConsInst,

    "lc": LcInst,
    "ld": LdInst,
    "st": StInst,
    "lr": LrInst,
    "la": LaInst,
    "sa": SaInst,
    "ll": LlInst,
    "sl": SlInst,
    "ex": ExInst,
    "mov": MovInst,
    "cs": CsInst,
    "ca": CaInst,
    "cl": ClInst,
    "lo": LoInst,
    "so": SoInst,

    "get": GetInst,
    "set": SetInst,
    "app": AppInst,
    "len": LenInst,

    "call": CallInst,
    "tcall": TcallInst,
    "ret": RetInst,
    "j": JInst,
    "jif": JifInst,
    "jim": JimInst,
    "jimif": JimifInst,

    "new": NewInst,
    "sv": SvInst,
    "cf": CfInst,
    "li": LiInst,
    "dc": DcInst,

    "lks": LksInst,
    "uls": UlsInst,
    "lkc": LkcInst,
    "ulc": UlcInst,

    "pr": PrInst,
    }
