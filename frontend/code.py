from vminstructions import *

class Code:
    def __init__(self, instrs, out=None):
        if isinstance(instrs, list):
            self.instrs = instrs
        else:
            self.instrs = [instrs]

        self.out = out

    def __add__(self, other):
        if isinstance(other, Instruction):
            other = Code(other)
        code = Code(self.instrs + other.instrs)
        code.out = None
        return code

    def __iadd__(self, other):
        return self + other

    def __repr__(self):
        ret = f"Code({self.out})\n\t"
        ret += "\n\t".join(repr(i) for i in self.instrs)
        return ret

    def __len__(self):
        return sum([1+len(i.operand_types) for i in self.instrs])
