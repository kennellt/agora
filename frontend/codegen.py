from asm import ExecBlob
from code import *
from funcs import *
from quote import Quote
from vminstructions import *
from vmopcodes import *
from vmtypes import *

class CodeGenError(Exception):
    pass

class CodeGen:
    def __init__(self):
        self._builtin_funcs = {
            "+": self._bi_plus,
            "-": self._bi_minus,
            "*": self._bi_star,
            "/": self._bi_slash,
            "mod": self._bi_mod,
            "<": self._gen_comp_func("<", LtInst),
            "=": self._gen_comp_func("=", EqInst),
            ">": self._gen_comp_func(">", GtInst),
            "<=": self._gen_comp_func("<=", LeInst),
            ">=": self._gen_comp_func(">=", GeInst),
            "!=": self._gen_comp_func("!=", NeInst),
            "and": self._bi_and,
            "cons": self._bi_cons,
            "car": self._bi_car,
            "cdr": self._bi_cdr,
            "list": self._bi_list,
            "empty?": self._bi_empty_p,
            "vector": self._bi_vector,
            "vector-length": self._bi_vector_length,
            "vector-get": self._bi_vector_get,
            "vector-set": self._bi_vector_set,
            "vector-push": self._bi_vector_push,
            "vector-map": self._bi_vector_map,
            "vector-concat": self._bi_vector_concat,
            }

        self._builtin_macros = {
            "define": self._bi_define,
            "set!": self._bi_setbang,
            "if": self._bi_if,
            "quote": self._bi_quote,
            "let": self._bi_let,
            "lambda": self._bi_lambda,
            "export": self._bi_export,
            "lock": self._bi_lock,
            }

        self.reset()

    def reset(self):
        self.funcs = FuncStack(FuncFrame(Function("*toplevel*", 0, 0, [], []), [], []))
        self.reg_pool = []
        self.refill_reg_pool()

    def _bi_plus(self, src_regs, dst_reg=None):
        dst_reg = self.alloc_reg() if not dst_reg else dst_reg
        try:
            code = Code(AddInst(dst_reg, src_regs[0], src_regs[1]))
        except IndexError:
            if len(src_regs) == 1:
                tmp_reg = self.alloc_reg()
                code = Code([LiInst(tmp_reg, 0), AddInst(dst_reg, tmp_reg, src_regs[0])])
            else:
                code = Code(LiInst(dst_reg, 0))
            code.out = dst_reg
            return code
        else:
            code += Code([AddInst(dst_reg, dst_reg, r) for r in src_regs[2:]])
            code.out = dst_reg
            return code

    def _bi_minus(self, src_regs, dst_reg=None):
        dst_reg = self.alloc_reg() if not dst_reg else dst_reg
        try:
            code = Code(SubInst(dst_reg, src_regs[0], src_regs[1]))
        except IndexError:
            if len(src_regs) == 1:
                tmp_reg = self.alloc_reg()
                code = Code([LiInst(tmp_reg, 0), SubInst(dst_reg, tmp_reg, src_regs[0])])
            else:
                raise CodeGenError("Not enough parameters passed to -")
            code.out = dst_reg
            return code
        else:
            code += Code([SubInst(dst_reg, dst_reg, r) for r in src_regs[2:]])
            code.out = dst_reg
            return code

    def _bi_star(self, src_regs, dst_reg=None):
        dst_reg = self.alloc_reg() if not dst_reg else dst_reg
        try:
            code = Code(MulInst(dst_reg, src_regs[0], src_regs[1]))
        except IndexError:
            if len(src_regs) == 1:
                tmp_reg = self.alloc_reg()
                code = Code([LiInst(tmp_reg, 1), MulInst(tmp_reg, dst_reg, src_regs[0])])
            else:
                code = Code(LiInst(dst_reg, 1))
            code.out = dst_reg
            return code
        else:
            code += Code([MulInst(dst_reg, dst_reg, r) for r in src_regs[2:]])
            code.out = dst_reg
            return code

    def _bi_slash(self, src_regs, dst_reg=None):
        dst_reg = self.alloc_reg() if not dst_reg else dst_reg
        try:
            code = Code(DivInst(dst_reg, src_regs[0], src_regs[1]))
        except IndexError:
            if len(src_regs) == 1:
                tmp_reg = self.alloc_reg()
                code = Code([LiInst(tmp_reg, 1), DivInst(tmp_reg, dst_reg, src_regs[0])])
            else:
                raise CodeGenError("Not enough parameters passed to /")
            code.out = dst_reg
            return code
        else:
            code += Code([DivInst(dst_reg, dist_reg, r) for r in src_regs[2:]])
            code.out = dst_reg
            return code

    def _bi_mod(self, src_regs, dst_reg=None):
        dst_reg = self.alloc_reg() if not dst_reg else dst_reg
        if len(src_regs) != 2:
            raise CodeGenError("`mod` takes exactly 2 parameters")
        code = Code(ModInst(dst_reg, src_regs[0], src_regs[1]))
        code.out = dst_reg
        return code

    def _gen_comp_func(self, name, instr):
        # don't include self in args, close over it from _gen_comp_func
        def func(src_regs, dst_reg=None):
            dst_reg = self.alloc_reg() if not dst_reg else dst_reg
            if len(src_regs) != 2:
                raise CodeGenError(f"`{name}` takes exactly 2 parameters")
            return Code(instr(dst_reg, src_regs[0], src_regs[1]), out=dst_reg)
        return func

    def _bi_and(self, src_regs, dst_reg=None):
        dst_reg = self.alloc_reg()  # can't allow overwriting src with dst

        zero = self.alloc_reg()
        tmp = self.alloc_reg()

        code = Code([LiInst(zero, "0"), LiInst(dst_reg, "1")])
        for a in src_regs:
            code += NeInst(tmp, a, zero)
            code += AndInst(dst_reg, dst_reg, tmp)

        code.out = dst_reg
        return code

    def _bi_cons(self, src_regs, dst_reg=None):
        dst_reg = self.alloc_reg() if not dst_reg else dst_reg
        if len(src_regs) != 2:
            raise CodeGenError("`cons` takes exactly 2 parameters")
        return Code(ConsInst(dst_reg, src_regs[0], src_regs[1]), out=dst_reg)

    def _bi_car(self, src_regs, dst_reg=None):
        dst_reg = self.alloc_reg() if not dst_reg else dst_reg
        if len(src_regs) != 1:
            raise CodeGenError("`car` takes exactly one parameter")
        return Code(CarInst(dst_reg, src_regs[0]), out=dst_reg)

    def _bi_cdr(self, src_regs, dst_reg=None):
        dst_reg = self.alloc_reg() if not dst_reg else dst_reg
        if len(src_regs) != 1:
            raise CodeGenError("`cdr` takes exactly one parameter")
        return Code(CdrInst(dst_reg, src_regs[0]), out=dst_reg)

    def _bi_list(self, src_regs, dst_reg=None):
        # can't reuse dst_reg from one of the arguments since we're walking backwards
        dst_reg = self.alloc_reg()
        code = Code(NewInst(dst_reg, 6), out=dst_reg)  # new empty list
        for sr in reversed(src_regs):
            code += ConsInst(dst_reg, sr, dst_reg)
        code.out = dst_reg
        return code

    def _bi_empty_p(self, src_regs, dst_reg=None):
        if len(src_regs) != 1:
            raise CodeGenError("`empty?` takes exactly one parameter")
        dst_reg = self.alloc_reg()  # can't reuse dst_reg

        code = Code(NewInst(dst_reg, 6))  # new empty list
        code += CtInst(dst_reg, dst_reg, src_regs[0])
        code.out = dst_reg
        return code

    def _bi_vector(self, src_regs, dst_reg=None):
        # can't allow reuse of dst_reg
        dst_reg = self.alloc_reg()
        code = Code(NewInst(dst_reg, 5))
        for a in src_regs:
            code += AppInst(dst_reg, a)
        code.out = dst_reg
        return code

    def _bi_vector_length(self, src_regs, dst_reg=None):
        dst_reg = self.alloc_reg() if not dst_reg else dst_reg
        if len(src_regs) != 1:
            raise CodeGenError("`vector-length` takes exactly one parameter")
        return Code(LenInst(dst_reg, src_regs[0]), out=dst_reg)

    def _bi_vector_get(self, src_regs, dst_reg=None):
        dst_reg = self.alloc_reg() if not dst_reg else dst_reg
        if len(src_regs) != 2:
            raise CodeGenError("`vector-get` takes exactly two parameters")
        return Code(GetInst(dst_reg, src_regs[0], src_regs[1]), out=dst_reg)

    def _bi_vector_set(self, src_regs, dst_reg=None):
        dst_reg = self.alloc_reg() if not dst_reg else dst_reg
        if len(src_regs) != 3:
            raise CodeGenError("`vector-set` takes exactly three parameters")
        code = Code(SetInst(src_regs[0], src_regs[2], src_regs[1]))
        code += MovInst(dst_reg, src_regs[0])
        code.out = dst_reg
        return code

    def _bi_vector_push(self, src_regs, dst_reg=None):
        dst_reg = self.alloc_reg() if not dst_reg else dst_reg
        if len(src_regs) != 2:
            raise CodeGenError("`vector-push` takes exactly two parameters")
        code = Code(AppInst(src_regs[0], src_regs[1]))
        code += MovInst(dst_reg, src_regs[0])
        code.out = dst_reg
        return code

    def _bi_vector_map(self, src_regs, dst_reg=None):
        if len(src_regs) != 2:
            raise CodeGenError("`vector-map` takes exactly two parameters")
        func = src_regs[0]
        vec = src_regs[1]

        one_reg = self.alloc_reg()
        ctr_reg = self.alloc_reg()
        cmp_reg = self.alloc_reg()
        len_reg = self.alloc_reg()
        tmp_reg = self.alloc_reg()
        arg_reg = self.alloc_reg()  # arguments to call func with
        dst_reg = self.alloc_reg()  # can't overwrite source reg

        code = Code(LenInst(len_reg, src_regs[1]))
        code += NewInst(dst_reg, 5)
        code += LiInst(one_reg, "1")
        code += LiInst(ctr_reg, "0")
        code += GeInst(cmp_reg, ctr_reg, len_reg)
        code += JimifInst(24, cmp_reg)
        code += GetInst(tmp_reg, src_regs[1], ctr_reg)
        code += NewInst(arg_reg, 5)
        code += AppInst(arg_reg, tmp_reg)
        code += CallInst(func, arg_reg)
        code += LrInst(tmp_reg)
        code += AppInst(dst_reg, tmp_reg)
        code += AddInst(ctr_reg, ctr_reg, one_reg)
        code += JimInst(-32)
        code += NopInst()

        code.out = dst_reg
        return code

    def _bi_vector_concat(self, src_regs, dst_reg=None):
        dst_reg = self.alloc_reg() if not dst_reg else dst_reg
        if len(src_regs) != 2:
            raise CodeGenError("`vector-concat` takes exactly two parameters")
        return Code(AddInst(dst_reg, src_regs[0], src_regs[1]), out=dst_reg)

    def _create_func(self, name, args, exprs):
        func = Function(name, len(args), 0, [], [])
        self.funcs.push(FuncFrame(func, [a.unquote() for a in args], []))

        orig_reg_pool = self.reg_pool
        self.refill_reg_pool()

        funcode = Code([])
        for idx,e in enumerate(exprs):
            part = self.visit(e)
            funcode += part
            if idx == len(exprs) - 1 and part.out:
                funcode += RetInst(part.out)
        func.bytecode = funcode.instrs
        frame = self.funcs.pop()

        self.refill_reg_pool(orig_reg_pool)
        code = self.make_load_const(func)

        if frame.needs_closure():
            funcout = code.out

            closevec = self.alloc_reg()
            code += NewInst(closevec, 5)

            tmp = self.alloc_reg()
            for cd in frame.closed:
                if cd.locat == ClosedValueFrom.Argument:
                    code += CaInst(tmp, cd.param, 0)
                elif cd.locat == ClosedValueFrom.Local:
                    code += ClInst(tmp, cd.param, 0)
                elif cd.locat == ClosedValueFrom.Global:
                    const = self.make_load_const(String(cd.param))
                    code += const
                    code += CsInst(tmp, const.out)
                elif cd.locat == ClosedValueFrom.Duplicate:
                    code += DcInst(tmp, cd.param)
                else:
                    raise CodeGenError("internal: received bad close type")

                code += AppInst(closevec, tmp)

            code += CfInst(funcout, funcout, closevec)
            code.out = funcout
        return code

    def _func_preamble(self, name, sig):
        if not all([isinstance(x, Quote) for x in sig]):
            raise CodeGenError("Function name and arguments must all be symbols")
        elif sig[0].unquote() in (self._builtin_funcs.keys() | self._builtin_macros.keys()):
            raise CodeGenError("Rebinding built-in symbols is not supported")

    def _bi_define(self, src_ast):
        if len(self.funcs) > 1:
            raise CodeGenError("`define` can only be called at the top-level")
        elif not isinstance(src_ast, list):
            raise CodeGenError("`define` takes at least two parameters")

        if isinstance(src_ast[0], list):
            self._func_preamble("define", src_ast[0])

            name = src_ast[0][0].unquote()
            left = self.make_load_const(String(name))
            right = self._create_func(name, src_ast[0][1:], src_ast[1:])
        elif isinstance(src_ast[0], Quote) and isinstance(src_ast[0].unquote(), str):
            if len(src_ast) != 2:
                raise CodeGenError("`define` takes exactly two parameters when defining a value")
            left = self.make_load_const(String(src_ast[0].unquote()))
            right = self.visit(src_ast[1])
        else:
            raise CodeGenError("Bad lval for `define`")
        code = left + right + Code(StInst(right.out, left.out))
        # no code.out required, no value returned
        return code

    def _bi_setbang(self, src_ast):
        if not isinstance(src_ast, list) or len(src_ast) != 2:
            raise CodeGenError("`set!` takes two parameters")
        elif not isinstance(src_ast[0], Quote):
            raise CodeGenError("first parameter of `set!` must be a symbol")

        sym = src_ast[0].unquote()
        value = self.visit(src_ast[1])
        if not value.out:
            raise CodeGenError("`set!` expression must return a value")

        code = value
        code += self.funcs.read_sym(sym, self.alloc_reg())
        code += self.funcs.write_sym(sym, value.out, self.alloc_reg())
        return code

    def compute_cond_jump(self, offset, cond_reg):
        if -128 < offset < 128:
            return Code(JimifInst(offset, cond_reg))
        else:
            code = self.make_load_const(Integer(offset))
            return code + JifInst(code.out, cond_reg)

    def compute_jump(self, offset):
        if -128 < offset < 128:
            return Code(JimInst(offset))
        else:
            code = self.make_load_const(Integer(offset))
            return code + JInst(code.out)

    def _bi_if(self, src_ast):
        if not isinstance(src_ast, list) or len(src_ast) != 3:
            raise CodeGenError("`if` takes three parameters")

        cond = self.visit(src_ast[0])

        orig_reg_pool = self.reg_pool  # save so we can reuse regs in the else-case
        yes = self.visit(src_ast[1])
        self.refill_reg_pool(self.reg_pool)  # restore any regs used in if-case
        no = self.visit(src_ast[2])

        out_reg = self.alloc_reg()
        if yes.out:
            yes += MovInst(out_reg, yes.out)
        else:
            yes += NewInst(out_reg, 0)  # new empty list
        if no.out:
            no += MovInst(out_reg, no.out)
        else:
            no += NewInst(out_reg, 0)  # new empty list

        # below code takes into account trailing `no` jump as well
        yes_jump = self.compute_jump(len(yes))
        code = cond + self.compute_cond_jump(len(no) + len(yes_jump), cond.out) + \
               no + yes_jump + yes + NopInst()  # terminating Nop to ensure valid jump destination
        code.out = out_reg
        return code

    def _bi_quote(self, src_ast):
        if len(src_ast) != 1:
            raise CodeGenError("`quote` takes one parameter")
        return self.visit(Quote(src_ast[0]))

    def _bi_let(self, src_ast):
        if len(src_ast) < 2:
            raise CodeGenError("`let` takes at least two parameters")
        defs = src_ast[0]
        exprs = src_ast[1:]

        if not isinstance(defs, list):
            raise CodeGenError("first argument to `let` must be a list")
        elif any([not isinstance(d, list) or len(d) != 2 or \
                  not isinstance(d[0], Quote) for d in defs]):
            raise CodeGenError("all definitions in `let` must be of form (symbol value)")
        elif len(defs) > 255:
            raise CodeGenError("`let` cannot accept more than 256 bindings")

        # TODO: if we enable (define ...) and (set! ...) like in Scheme, we need
        # to scan the exprs in each let for any more locals that may be introduced
        # so that closing over locals works right
        func = Function("<let>", 0, len(defs), [], [])
        self.funcs.push(FuncFrame(func, [], [d[0].unquote() for d in defs]))

        orig_reg_pool = self.reg_pool
        self.refill_reg_pool()

        defscode = Code([])
        for idx,d in enumerate(defs):
            part = self.visit(d[1])
            defscode += part
            defscode += SlInst(part.out, idx, 0)

        funcode = Code([])
        for idx,e in enumerate(exprs):
            part = self.visit(e)
            funcode += part
            if idx == len(exprs) - 1 and part.out:
                funcode += RetInst(part.out)
        func.bytecode = (defscode + funcode).instrs
        frame = self.funcs.pop()

        self.refill_reg_pool(orig_reg_pool)

        funcreg = self.make_load_const(func)
        if frame.needs_closure():
            funcout = funcreg.out

            closevec = self.alloc_reg()
            funcreg += NewInst(closevec, 5)

            tmp = self.alloc_reg()
            for cd in frame.closed:
                if cd.locat == ClosedValueFrom.Argument:
                    funcreg += CaInst(tmp, cd.param, 0)
                elif cd.locat == ClosedValueFrom.Local:
                    funcreg += ClInst(tmp, cd.param, 0)
                elif cd.locat == ClosedValueFrom.Global:
                    const = self.make_load_const(String(cd.param))
                    funcreg += const
                    funcreg += CsInst(tmp, const.out)
                elif cd.locat == ClosedValueFrom.Duplicate:
                    funcreg += DcInst(tmp, cd.param)
                else:
                    raise CodeGenError("internal: received bad close type")

                funcreg += AppInst(closevec, tmp)

            funcreg += CfInst(funcout, funcout, closevec)
            funcreg.out = funcout

        argsreg = self.alloc_reg()
        code = funcreg + NewInst(argsreg, 5) + CallInst(funcreg.out, argsreg)
        code += LrInst(funcreg.out)
        code.out = funcreg.out
        return code

    def _bi_lambda(self, src_ast):
        if len(src_ast) < 2:
            raise CodeGenError("`lambda` takes at least two parameters")
        args = src_ast[0]
        exprs = src_ast[1:]

        if not isinstance(args, list):
            raise CodeGenError("first argument to `lambda` must be a list")
        elif any([not isinstance(a, Quote) for a in args]):
            raise CodeGenError("all arguments must be symbols")

        return self._create_func("<lambda>", args, exprs)

    def _bi_export(self, src_ast):
        if len(self.funcs) > 1:
            raise CodeGenError("`export` may only be called at the top-level")
        elif not isinstance(src_ast, list):
            raise CodeGenError("`export` takes at least two parameters")
        elif len(src_ast) < 2 or len(src_ast) > 3:
            raise CodeGenError("`export` takes either two or three parameters")

        permreg = self.alloc_reg()
        # code = Code(NewInst(permreg, 5))
        if len(src_ast[0]) == 3:
            if not isinstance(src_ast[0], list):
                raise CodeGenError("`export` permissions must be an s-expression")
            elif len(src_ast[0]) != 3 or not all([isinstance(x, int) and abs(x) < 2 for x in src_ast[0]]):
                raise CodeGenError("`export` permissions must contain 3 boolean elements")

            code = self.make_load_const(Vector([Integer(i) for i in src_ast[0]]), reg=permreg)
        else:
            code = Code(NewInst(permreg, 5))

        if isinstance(src_ast[1], list):
            self._func_preamble("export", src_ast[1])

            name = src_ast[1][0].unquote()
            sym = self.make_load_const(String(name))
            val = self._create_func(name, src_ast[1][1:], src_ast[2:])
        else:
            sym = self.make_load_const(String(src_ast[1].unquote()))
            val = self.visit(src_ast[2])

        code += sym + val + ExInst(sym.out, val.out, permreg)
        # no output value returned
        return code

    def _bi_lock(self, src_ast):
        if len(src_ast) < 2:
            raise CodeGenError("`lock` takes at least two parameters")
        elif not isinstance(src_ast[0], list) or not all([isinstance(x, Quote) for x in src_ast[0]]):
            raise CodeGenError("first parameter to `lock` must be a list of symbols")

        exprs = src_ast[1:]
        body = Code([])
        ret = None
        for idx,e in enumerate(exprs):
            part = self.visit(e)
            body += part
            if idx == len(exprs) - 1 and part.out:
                ret = RetInst(part.out)
                body.out = part.out

        code = Code([])

        one = self.alloc_reg()
        code += LiInst(one, "1")

        tmp = self.alloc_reg()
        locked = []
        for sym in src_ast[0]:
            tp, aux = self.funcs.find_symbol(sym.unquote())
            if tp == "a":
                continue  # args can't be accessed concurrently
            elif tp == "l":
                continue  # locals can't be accessed concurrently
            elif tp == "c":
                code += LkcInst(tmp, aux)
                code += NeInst(tmp, tmp, one)
                code += JimifInst(-10, tmp)
                locked.append(("c", aux))
            elif tp == "g":
                load = self.make_load_const(String(sym.unquote()))
                code += load
                code += LksInst(tmp, load.out)
                code += NeInst(tmp, tmp, one)
                code += JimifInst(-10, tmp)
                locked.append(("g", sym.unquote()))

        code += body
        for lk in reversed(locked):
            if lk[0] == "c":
                code += UlcInst(str(lk[1]))
            elif lk[0] == "g":
                code += self.make_load_const(String(lk[1]), reg=tmp)
                code += UlsInst(tmp)

        if ret:
            code += ret
        code.out = body.out
        return code

    def alloc_reg(self):
        try:
            return f"r{self.reg_pool.pop(0)}"
        except IndexError:
            raise CodeGenError("Out of registers for current frame")

    def refill_reg_pool(self, orig=None):
        if orig:
            self.reg_pool = orig
        else:
            self.reg_pool = [str(x) for x in range(256)]

    def make_load_const(self, value, reg=None):
        if not reg:
            reg = self.alloc_reg()
        return Code(LcInst(reg, self.funcs.add_const(value)), out=reg)

    def process_args(self, ast):
        args = [self.visit(s) for s in ast]
        argvec = self.alloc_reg()
        code = Code(NewInst(argvec, 5))
        for c in args:
            code += c
            code += AppInst(argvec, c.out)
        code.out = argvec
        return code

    def visit(self, ast):
        if isinstance(ast, int):
            if -128 <= ast <= 127:
                reg = self.alloc_reg()
                return Code(LiInst(reg, str(ast)), out=reg)
            else:
                return self.make_load_const(Integer(ast))
        elif isinstance(ast, float):
            return self.make_load_const(Float(ast))
        elif isinstance(ast, str):
            return self.make_load_const(String(ast))
        elif isinstance(ast, Quote):
            sym = ast.unquote()
            if isinstance(sym, str):
                reg = self.alloc_reg()
                return self.funcs.read_sym(sym, reg)
            else:
                raise CodeGenError("Quote handling not implemented for this type yet")
        elif isinstance(ast, list):
            if len(ast) == 0:
                raise CodeGenError("Cannot call nothing")
            elif isinstance(ast[0], Quote):
                sym = ast[0].unquote()
                if sym in self._builtin_funcs:
                    args = [self.visit(s) for s in ast[1:]]
                    code = Code([])
                    for a in args:
                        code += a
                    res = self._builtin_funcs[sym]([a.out for a in args],
                                                   dst_reg=args[0].out if len(args) > 0 else None)
                    # we can reuse a source reg as the dest above because
                    # we are not compacting any immediate loads after this
                    code += res
                    code.out = res.out
                    return code
                elif sym in self._builtin_macros:
                    return self._builtin_macros[sym](ast[1:])
                else:
                    args = self.process_args(ast[1:])

                    reg = self.alloc_reg()
                    func = self.funcs.read_sym(sym, reg)

                    code = func + args
                    code += CallInst(func.out, args.out)
                    code += LrInst(func.out)
                    code.out = func.out
                    return code
            elif isinstance(ast[0], list):
                func = self.visit(ast[0])
                args = self.process_args(ast[1:])
                code = func + args

                code += CallInst(func.out, args.out)
                code += LrInst(func.out)
                code.out = func.out
                return code
            else:
                raise CodeGenError(f"Cannot call type '{type(ast[0])}'")

    def generate(self, ast, name):
        self.reset()
        code = self.visit(ast)
        if code.out:
            code += RetInst(code.out)
        # this effectively becomes a lightweight top-level
        # `define` expression
        func = self.funcs.peek().func
        func.name = name
        func.bytecode = code.instrs
        return ExecBlob(0, name, func)

if __name__ == "__main__":
    import sys
    from lexer import Lexer, Stream
    from parser import Parser

    stream = Stream(sys.stdin)
    lexer = Lexer(stream)
    parser = Parser(lexer)
    codegen = CodeGen()

    enable_type_debug_repr()

    while True:
        print("> ", end="", flush=True)
        try:
            print(codegen.visit(parser.parse()))
            print(f"consts: {codegen.funcs.peek().func.consts}")
        except EOFError:
            break

        codegen.reset()
