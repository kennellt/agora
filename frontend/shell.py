#! /usr/bin/env python3

import msgpack
import socket
import struct
import sys
from argparse import ArgumentParser
from collections import UserList
from dataclasses import dataclass
from getpass import getpass
from typing import Optional

from lexer import Lexer, LexerError, Stream
from parser import Parser, ParserError
from codegen import CodeGen, CodeGenError
from funcs import FuncStackError
from vmtypes import *

@dataclass
class CondensedFrame:
    locals: list[Type]
    stack: list[Type]
    name: str
    args: list[Type]
    pc: int
    retval: Optional[Type]
    regs: {int: Type}

    @classmethod
    def deserialize(cls, ls):
        locals = [Type.deserialize(list(x.keys())[0], list(x.values())[0]) for x in ls[0]]
        stack = [Type.deserialize(list(x.keys())[0], list(x.values())[0]) for x in ls[1]]
        name = ls[2]
        args = [Type.deserialize(list(x.keys())[0], list(x.values())[0]) for x in ls[3]]
        pc = ls[4]
        retval = Type.deserialize(list(ls[5].keys())[0], list(ls[5].values())[0]) if ls[5] else None
        regs = {k: Type.deserialize(list(v.keys())[0], list(v.values())[0]) for k, v in ls[6].items()}
        return cls(locals, stack, name, args, pc, retval, regs)

@dataclass
class ResultBlob:
    version: int
    ident: str
    result: Optional[Type]
    error: Optional[int]
    context: Optional[list[Frame]]

    @classmethod
    def deserialize(cls, ls):
        version = ls[0]
        ident = ls[1]
        result = Type.deserialize(list(ls[2].keys())[0], list(ls[2].values())[0]) if ls[2] else None
        error = ls[3]
        context = [CondensedFrame.deserialize(x) for x in ls[4]] if ls[4] else None
        return cls(version, ident, result, error, context)

if __name__ == "__main__":
    arg_parser = ArgumentParser(description="shell for agora demo compiler")
    arg_parser.add_argument("host", type=str,
                            help="supervisor address to connect to")
    arg_parser.add_argument("-p", "--port", type=int, default=6000,
                            help="supervisor port to connect to")
    args = arg_parser.parse_args(sys.argv[1:])

    stream = Stream(sys.stdin)
    lexer = Lexer(stream)
    parser = Parser(lexer)
    codegen = CodeGen()

    print(f"[agora shell] connecting to {args.host} on {args.port}...", end="")

    sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.connect((args.host, args.port))

    print("connected...")

    username = input("username: ")
    passwd = getpass("password: ")
    sock.sendall(msgpack.dumps([username, passwd]))
    auth = sock.recv(1)
    if auth[0] == 0:
        print("auth failed, disconnecting")
        sock.close()
        sys.exit(1)

    while True:
        print("> ", end="", flush=True)
        try:
            exe = codegen.generate(parser.parse(), "*shell*")
        except EOFError:
            break
        except LexerError as e:
            print(f"input error: {e}")
            continue
        except ParserError as e:
            print(f"format error: {e}")
            continue
        except CodeGenError as e:
            print(f"code error: {e}")
            continue
        except FuncStackError as e:
            print(f"call stack error: {e}")
            continue

        sock.sendall(msgpack.dumps(exe.serialize()))

        buf = b""
        while True:
            inc = sock.recv(1024)
            buf += inc
            if len(inc) != 1024:
                break

        res = ResultBlob.deserialize(msgpack.loads(buf, strict_map_key=False))
        if res.result:
            print(res.result)
        elif res.error:
            print(f"% err {res.error}")
            print("% backtrace:")
            for cf in res.context:
                print(f"% \t{cf.name} {cf.args}")
