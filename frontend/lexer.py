from enum import Enum

class Stream:
    def __init__(self, iostream):
        self.iostream = iostream
        self.buf = ""

    def read(self, num):
        ret = ""
        if len(self.buf) > 0:
            if num <= len(self.buf):
                ret = self.buf[:num]
                self.buf = self.buf[num:]
            else:
                ret = self.buf
                ol = len(self.buf)
                self.buf = ""
                ret += self.iostream.read(num - ol)
        else:
            ret = self.iostream.read(num)

        if len(ret) == 0:
            raise EOFError
        else:
            return ret

    def get(self):
        return self.read(1)

    def unget(self, st):
        self.buf += st

class TokenType(Enum):
    LPAREN = 0
    RPAREN = 1
    SINGLEQUOTE = 2
    DOUBLEQUOTE = 3
    QUASIQUOTE = 4
    COMMA = 5
    COMMENT = 6
    WHITESPACE = 7
    INTEGER = 8
    FLOAT = 9
    SYMBOL = 10
    STRING = 11

class Token:
    def __init__(self, tp, obj):
        self.tp = tp
        self.obj = obj

    def __repr__(self):
        if self.tp == TokenType.LPAREN:
            return "<lparen>"
        elif self.tp == TokenType.RPAREN:
            return "<rparen>"
        elif self.tp == TokenType.SINGLEQUOTE:
            return "<squote>"
        elif self.tp == TokenType.DOUBLEQUOTE:
            return "<dquote>"
        elif self.tp == TokenType.QUASIQUOTE:
            return "<qquote>"
        elif self.tp == TokenType.COMMA:
            return "<comma>"
        elif self.tp == TokenType.COMMENT:
            return f"<comment \"{self.obj}\">"
        elif self.tp == TokenType.WHITESPACE:
            return "<whitesp>"
        elif self.tp == TokenType.INTEGER:
            return f"<integer {self.obj}>"
        elif self.tp == TokenType.FLOAT:
            return f"<float {self.obj}>"
        elif self.tp == TokenType.SYMBOL:
            return f"<symbol \"{self.obj}\">"
        elif self.tp == TokenType.STRING:
            return f"<string \"{self.obj}\">"
        else:
            return "<unknown type {self.tp}>"

class LexerError(Exception):
    pass

class Lexer:
    _lex_simple_map = {
        '(': Token(TokenType.LPAREN, None),
        ')': Token(TokenType.RPAREN, None),
        '\'': Token(TokenType.SINGLEQUOTE, None),
        '`': Token(TokenType.QUASIQUOTE, None),
        ',': Token(TokenType.COMMA, None),
        ' ': Token(TokenType.WHITESPACE, None),
        '\n': Token(TokenType.WHITESPACE, None),
        '\r': Token(TokenType.WHITESPACE, None),
        '\t': Token(TokenType.WHITESPACE, None),
        }

    _lex_split = {'(', ')', '\'', '"', ';', '`', ',',
                  ' ', '\n', '\r', '\t'}

    def __init__(self, stream, break_on_nl=False):
        self.stream = stream
        self.break_on_nl = break_on_nl
        self.reset()

    def __iter__(self):
        return self

    def __next__(self):
        try:
            ret = self.lex()
        except EOFError:
            raise StopIteration

        if ret == None:
            raise StopIteration
        else:
            return ret

    def _read_number(self, buf, coef):
        dot = False

        def ret():
            if dot:
                return Token(TokenType.FLOAT, coef * float(buf))
            else:
                return Token(TokenType.INTEGER, coef * int(buf))

        while True:
            c = self.stream.get()
            if len(c) == 0:
                return ret()
            elif c.isdigit():
                buf += c
            elif c == '.' and not dot:
                dot = True
                buf += c
            elif c == '.' and dot:
                raise LexerError("Number cannot have more than one dot")
            elif c in self._lex_split:
                self.stream.unget(c)
                return ret()
            else:
                raise LexerError("Number contains invalid character")

    def _read_symbol(self, buf):
        while True:
            c = self.stream.get()
            if c in self._lex_split:
                self.stream.unget(c)
                return Token(TokenType.SYMBOL, buf)
            else:
                buf += c

    def reset(self):
        self.string_mode = False

    def lex(self):
        c = self.stream.get()
        if self.break_on_nl and c == "\n":
            return None

        if c == '"':  # this MUST come before the string_mode check!
            self.string_mode = not self.string_mode
            return Token(TokenType.DOUBLEQUOTE, None)
        elif self.string_mode:  # if this comes before '"', we loop
            buf = ""
            while c != '"':
                buf += c
                c = self.stream.get()
            # c is now equal to '"'
            self.stream.unget(c)
            return Token(TokenType.STRING, buf)
        elif c in self._lex_simple_map:
            return self._lex_simple_map[c]
        elif c == ';':
            comment = ""
            while c != "\n":
                comment += c
                c = self.stream.get()
            return Token(TokenType.COMMENT, comment)
        elif c.isdigit():
            return self._read_number(c, 1)
        elif c == '-':
            nxt = self.stream.get()
            if nxt.isdigit():
                return self._read_number(nxt, -1)
            else:
                self.stream.unget(nxt)
                return self._read_symbol(c)
        else:
            return self._read_symbol(c)

if __name__ == "__main__":
    import sys

    stream = Stream(sys.stdin)
    lex = Lexer(stream, break_on_nl=True)

    while True:
        print("> ", end="", flush=True)
        try:
            print(" ".join(repr(x) for x in iter(lex)))
        except LexerError as e:
            print(e)
            lex.reset()
