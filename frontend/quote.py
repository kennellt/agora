
class QuoteError(Exception):
    pass

class Quote:
    def __new__(cls, obj):
        if isinstance(obj, int):
            return obj
        elif isinstance(obj, float):
            return obj
        elif isinstance(obj, str):
            nw = super(Quote, cls).__new__(cls)
            return nw
        elif isinstance(obj, Quote):
            nw = super(Quote, cls).__new(cls)
            return nw
        elif isinstance(obj, list):
            return [Quote("list")] + [Quote(x) for x in obj]
        else:
            raise QuoteError(f"Unknown type {type(obj)}")

    def __init__(self, obj):
        self.obj = obj

    def __repr__(self):
        if isinstance(self.obj, Quote):
            return f"'{repr(self.obj)}"
        else:
            return f"{self.obj}"

    def __eq__(self, other):
        if not isinstance(other, Quote):
            return False
        return self.obj == other.obj

    def __str__(self):
        return repr(self)

    def unquote(self):
        return self.obj

    def issym(self):
        return not isinstance(self.obj, Quote)
