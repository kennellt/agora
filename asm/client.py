#! /usr/bin/env python3

import msgpack
import socket
import struct
import sys
from getpass import getpass

if len(sys.argv) < 2:
    print("Usage: client.py INPUTFILE [INPUTFILE2 ...]")
    sys.exit(1)

sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.connect(("::1", 6000))

username = input("username: ")
passwd = getpass("password: ")
sock.sendall(msgpack.dumps([username, passwd]))
auth = sock.recv(1)
if auth[0] == 0:
    print("auth failed, disconnecting")
    sock.close()
    sys.exit(1)


for name in sys.argv[1:]:
    with open(name, "rb") as fp:
        data = fp.read()
    sock.sendall(data)

    buf = b""
    while True:
        inc = sock.recv(1024)
        buf += inc
        if len(inc) != 1024:
            break
    print(msgpack.loads(buf, strict_map_key=False))
