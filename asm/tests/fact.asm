.meta version 0
.meta id fact
.meta func $entry  ;; begin execution at symbol $entry

.funcstart fact 2 0  ;; factorial function with 2 args
.const String("fact")

la r1 0 0
la r2 1 0
new r3 1

gt r4 r2 r3
jimif 2 r4
ret r1

li r5 1
mul r1 r1 r2
sub r2 r2 r5

lc r7 0
ld r7 r7

new r9 5
app r9 r1
app r9 r2
tcall r7 r9
.funcend

.funcstart $entry 0 0  ;; execution starts here
.const String("fact")
.funcref fact
.const Integer(10)

lc r1 0
lc r2 1

cs r4 r1
new r3 5
app r3 r4
cf r2 r2 r3
st r2 r1

lc r3 2
push r3
li r3 1
push r3
li r3 2
sv r3 r3
call r2 r3
lr r255
ret r255
.funcend
