.meta version 0
.meta id fib
.meta func $entry

.funcstart fib 1 0
.const Integer(1)
.const String("fib")

la r1 0 0
lc r2 0
ge r3 r1 r2
jimif 2 r3
ret r2
lc r4 1
ld r3 r4
sub r4 r1 r2
new r5 5
app r5 r4
call r3 r5
lr r254
sub r4 r4 r2
new r5 5
app r5 r4
call r3 r5
lr r255
add r254 r254 r255
ret r254
.funcend

.funcstart $entry 0 0
.const String("fib")
.funcref fib
.const Integer(10)
.const Integer(1)

lc r1 0
lc r2 1
cs r3 r1
new r4 5
app r4 r3
cf r2 r2 r4
st r2 r1
lc r3 2
new r4 5
app r4 r3
call r2 r4
lr r255
ret r255
.funcend
