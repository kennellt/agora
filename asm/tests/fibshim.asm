.meta version 0
.meta id fib
.meta func $entry

.funcstart $entry 0 0
.const String("fib")
.const Integer(10)
.const Integer(1)

lc r1 0
ld r2 r1
lc r3 1
push r3
lc r4 2
sv r3 r4
call r2 r3
lr r255
ret r255
.funcend
