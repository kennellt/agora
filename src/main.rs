#![allow(dead_code)]

use std::fs;
use std::net::SocketAddr;
use std::str::FromStr;
use clap::{Parser, Subcommand};
use serde_json;

mod acl;
mod closure;
mod command;
mod error;
mod execblob;
mod frame;
mod function;
mod info;
mod instructions;
mod opcodes;
mod message;
mod metavisor;
mod mptraits;
mod resultblob;
mod scope;
mod supervisor;
mod types;
mod user;
mod vm;

use user::UserManager;

#[derive(Debug, Subcommand)]
enum Modes {
    Metavisor {
        #[clap(short, long, value_parser, default_value_t = String::from("[::1]:6001"))]
        mesg: String,

        #[clap(short, long, value_parser, default_value_t = String::from("[::1]:6002"))]
        info: String,

        #[clap(short, long, value_parser, default_value_t = String::from("[::1]:6003"))]
        user: String,

        #[clap(value_parser)]
        users_path: String
    },
    Supervisor {
        #[clap(short, long, value_parser, default_value_t = String::from("[::1]:6000"))]
        user: String,

        #[clap(short, long, value_parser, default_value_t = String::from("[::1]:6001"))]
        mesg: String,

        #[clap(short, long, value_parser, default_value_t = String::from("[::1]:6002"))]
        info: String,
    },
}

#[derive(Debug, Parser)]
struct ToplevelOpts {
    #[clap(subcommand)]
    mode: Modes,
}

use metavisor::Metavisor;
use supervisor::Supervisor;

fn addr_to_sockaddr(addr: String) -> SocketAddr {
    match SocketAddr::from_str(&addr) {
        Ok(a) => a,
        Err(_) => {
            println!("Invalid address: {}", addr);
            std::process::exit(1);
        },
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = ToplevelOpts::parse();
    match args.mode {
        Modes::Metavisor { mesg, info, user, users_path } =>
            Metavisor::new(addr_to_sockaddr(mesg),
                           addr_to_sockaddr(info),
                           addr_to_sockaddr(user),
                           UserManager::new(serde_json::from_str(&fs::read_to_string(users_path)?)?))?.run(),
        Modes::Supervisor { user, mesg, info } =>
            Supervisor::new(addr_to_sockaddr(user),
                            addr_to_sockaddr(mesg),
                            addr_to_sockaddr(info))?.run()?,
    }

    Ok(())
}
