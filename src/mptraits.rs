use std::error;
use std::io::{Read, Write};
use serde::{Deserialize, Serialize};
use rmp_serde::{decode, Deserializer, Serializer};

pub trait SendSerialize {
    fn send(&self, stream: &mut dyn Write) -> Result<(), Box<dyn error::Error>>
    where Self: Serialize {
        let mut buf = Vec::new();
        let mut se = Serializer::new(&mut buf);
        self.serialize(&mut se)?;
        stream.write(&buf)?;
        Ok(())
    }
}

pub trait RecvDeserialize {
    fn recv<'a>(stream: &mut dyn Read) -> Result<Self, decode::Error>
    where Self: Sized + Deserialize<'a> {
        let mut de = Deserializer::new(stream);
        Deserialize::deserialize(&mut de)
    }
}
