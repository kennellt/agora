use std::convert::From;
use std::fmt;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum OpCode {
    Nop,

    Add,
    Subtract,
    Multiply,
    Divide,
    Modulo,

    And,
    Or,
    Not,
    Xor,

    Less,
    Equal,
    Greater,
    LessEqual,
    GreaterEqual,
    NotEqual,
    CmpType,

    Car,
    Cdr,
    Cons,

    Push,
    Pop,

    LoadConst,
    LoadSym,
    StoreSym,
    LoadRet,
    LoadArg,
    StoreArg,
    LoadLocal,
    StoreLocal,
    Export,
    Move,
    CloseSym,
    CloseArg,
    CloseLocal,
    LoadClosed,
    StoreClosed,

    Get,
    Set,
    Insert,
    Append,
    Remove,
    Length,

    Call,
    TailCall,
    Return,
    Jump,
    JumpIf,
    JumpImm,
    JumpImmIf,

    New,  // create a new "empty" instance of any type
    StackVec,
    CloseFunc,
    LitInt,
    DupClosed,

    LockSym,
    UnlockSym,
    LockClosed,
    UnlockClosed,

    PrintReg,  // debug

    // need to implement ACL, frame, IO access ops

    Invalid(u8),
}
/*
pub enum OpCode {
    Nop          = 0x0,

    Add          = 0x10,
    Subtract     = 0x11,
    Multiply     = 0x12,
    Divide       = 0x13,
    Modulo       = 0x14,

    And          = 0x20,
    Or           = 0x21,
    Not          = 0x22,
    Xor          = 0x23,

    Less         = 0x30,
    Equal        = 0x31,
    Greater      = 0x32,
    LessEqual    =0x33,
    GreaterEqual = 0x34,
    NotEqual     = 0x35,
    CmpType      = 0x36,

    Car          = 0x40,
    Cdr          = 0x41,
    Cons         = 0x42,

    Push         = 0x50,
    Pop          = 0x51,

    LoadConst    = 0x60,
    LoadSym      = 0x61,
    StoreSym     = 0x62,
    LoadRet      = 0x63,
    LoadArg      = 0x64,
    StoreArg     = 0x65
    LoadLocal    = 0x66
    StoreLocal   = 0x67
    Export       = 0x68,
    Move         = 0x69,
    CloseSym     = 0x6A,
    CloseArg     = 0x6B,
    CloseLocal   = 0x6C
    LoadClosed   = 0x6D,
    StoreClosed  = 0x6E

    Get          = 0x70,
    Set          = 0x71,
    Insert       = 0x72,
    Append       = 0x73,
    Remove       = 0x74,
    Length       = 0x75,

    Call         = 0x80,
    TailCall     = 0x81,
    Return       = 0x82,
    Jump         = 0x83,
    JumpIf       = 0x84,
    JumpImm      = 0x85,
    JumpImmIf    = 0x86,

    New          = 0x90,  // create a new "empty" instance of any type
    StackVec     = 0x91,
    CloseFunc    = 0x92,
    LitInt       = 0x93,
    DupClosed    = 0x94,

    LockSym      = 0xa0,
    UnlockSym    = 0xa1,
    LockClosed   = 0xa2,
    UnlockClosed = 0xa3,

    PrintReg     = 0xf0,  // debug

    // need to implement ACL, frame, IO access ops

    Invalid(u8),
}
 */

impl OpCode {
    pub fn into_u8(&self) -> u8 {
        match *self {
            OpCode::Nop => 0x00,

            OpCode::Add => 0x10,
            OpCode::Subtract => 0x11,
            OpCode::Multiply => 0x12,
            OpCode::Divide => 0x13,
            OpCode::Modulo => 0x14,

            OpCode::And => 0x20,
            OpCode::Or => 0x21,
            OpCode::Not => 0x22,
            OpCode::Xor => 0x23,

            OpCode::Less => 0x30,
            OpCode::Equal => 0x31,
            OpCode::Greater => 0x32,
            OpCode::LessEqual => 0x33,
            OpCode::GreaterEqual => 0x34,
            OpCode::NotEqual => 0x35,
            OpCode::CmpType => 0x36,

            OpCode::Car => 0x40,
            OpCode::Cdr => 0x41,
            OpCode::Cons => 0x42,

            OpCode::Push => 0x50,
            OpCode::Pop => 0x51,

            OpCode::LoadConst => 0x60,
            OpCode::LoadSym => 0x61,
            OpCode::StoreSym => 0x62,
            OpCode::LoadRet => 0x63,
            OpCode::LoadArg => 0x64,
            OpCode::StoreArg => 0x65,
            OpCode::LoadLocal => 0x66,
            OpCode::StoreLocal => 0x67,
            OpCode::Export => 0x68,
            OpCode::Move => 0x69,
            OpCode::CloseSym => 0x6A,
            OpCode::CloseArg => 0x6B,
            OpCode::CloseLocal => 0x6C,
            OpCode::LoadClosed => 0x6D,
            OpCode::StoreClosed => 0x6E,

            OpCode::Get => 0x70,
            OpCode::Set => 0x71,
            OpCode::Insert => 0x72,
            OpCode::Append => 0x73,
            OpCode::Remove => 0x74,
            OpCode::Length => 0x75,

            OpCode::Call => 0x80,
            OpCode::TailCall => 0x81,
            OpCode::Return => 0x82,
            OpCode::Jump => 0x83,
            OpCode::JumpIf => 0x84,
            OpCode::JumpImm => 0x85,
            OpCode::JumpImmIf => 0x86,

            OpCode::New => 0x90,
            OpCode::StackVec => 0x91,
            OpCode::CloseFunc => 0x92,
            OpCode::LitInt => 0x93,
            OpCode::DupClosed => 0x94,

            OpCode::LockSym => 0xa0,
            OpCode::UnlockSym => 0xa1,
            OpCode::LockClosed => 0xa2,
            OpCode::UnlockClosed => 0xa3,

            OpCode::PrintReg => 0xf0,  // debug

            OpCode::Invalid(op) => op,
        }
    }
}

impl From<u8> for OpCode {
    fn from(value: u8) -> Self {
        match value {
            0x00 => OpCode::Nop,

            0x10 => OpCode::Add,
            0x11 => OpCode::Subtract,
            0x12 => OpCode::Multiply,
            0x13 => OpCode::Divide,
            0x14 => OpCode::Modulo,

            0x20 => OpCode::And,
            0x21 => OpCode::Or,
            0x22 => OpCode::Not,
            0x23 => OpCode::Xor,

            0x30 => OpCode::Less,
            0x31 => OpCode::Equal,
            0x32 => OpCode::Greater,
            0x33 => OpCode::LessEqual,
            0x34 => OpCode::GreaterEqual,
            0x35 => OpCode::NotEqual,
            0x36 => OpCode::CmpType,

            0x40 => OpCode::Car,
            0x41 => OpCode::Cdr,
            0x42 => OpCode::Cons,

            0x50 => OpCode::Push,
            0x51 => OpCode::Pop,

            0x60 => OpCode::LoadConst,
            0x61 => OpCode::LoadSym,
            0x62 => OpCode::StoreSym,
            0x63 => OpCode::LoadRet,
            0x64 => OpCode::LoadArg,
            0x65 => OpCode::StoreArg,
            0x66 => OpCode::LoadLocal,
            0x67 => OpCode::StoreLocal,
            0x68 => OpCode::Export,
            0x69 => OpCode::Move,
            0x6A => OpCode::CloseSym,
            0x6B => OpCode::CloseArg,
            0x6C => OpCode::CloseLocal,
            0x6D => OpCode::LoadClosed,
            0x6E => OpCode::StoreClosed,

            0x70 => OpCode::Get,
            0x71 => OpCode::Set,
            0x72 => OpCode::Insert,
            0x73 => OpCode::Append,
            0x74 => OpCode::Remove,
            0x75 => OpCode::Length,

            0x80 => OpCode::Call,
            0x81 => OpCode::TailCall,
            0x82 => OpCode::Return,
            0x83 => OpCode::Jump,
            0x84 => OpCode::JumpIf,
            0x85 => OpCode::JumpImm,
            0x86 => OpCode::JumpImmIf,

            0x90 => OpCode::New,
            0x91 => OpCode::StackVec,
            0x92 => OpCode::CloseFunc,
            0x93 => OpCode::LitInt,
            0x94 => OpCode::DupClosed,

            0xa0 => OpCode::LockSym,
            0xa1 => OpCode::UnlockSym,
            0xa2 => OpCode::LockClosed,
            0xa3 => OpCode::UnlockClosed,

            0xf0 => OpCode::PrintReg,  // debug

            op => OpCode::Invalid(op),
        }
    }
}

impl fmt::Display for OpCode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            OpCode::Invalid(op) => write!(f, "Invalid [{}]", op),
            _ => write!(f, "{:?} [0x{:0x}]", *self, self.into_u8()),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_convert() {
        for u in 0..255 {
            let op = OpCode::from(u);
            assert_eq!(op.into_u8(), u);
        }
    }
}
