use std::cell::RefCell;
use std::collections::HashMap;
use std::net::{SocketAddr, TcpListener, TcpStream};
use std::rc::Rc;
use std::sync::{Arc, RwLock};
use std::sync::atomic::AtomicBool;
use std::sync::mpsc::{channel, Sender, Receiver};
use std::thread::{Builder, JoinHandle};
use debug_print::debug_println;
use mio::{Events, Interest, Poll, Token, Waker};

use crate::acl::Acl;
use crate::error::Error;
use crate::execblob::ExecBlob;
use crate::function::Function;
use crate::info::Info;
use crate::message::Message;
use crate::mptraits::*;
use crate::supervisor::SupervisorId;
use crate::types::{Type, SerializedType, ClosedId, ClosedType, ExportType};
use crate::user::{UserId, UserManager};
use crate::vm::{Vm, VmId, VmStatus};

type SharedTable<K, V> = Arc<RwLock<HashMap<K, V>>>;
type SharedLock = (SupervisorId, VmId, UserId);

#[derive(Debug)]
pub struct Metavisor {
    mesg_listener: TcpListener,
    info_listener: TcpListener,
    user_listener: TcpListener,
    id_ctr: SupervisorId,
    nodes: HashMap<SupervisorId, (TcpStream, JoinHandle<()>)>,
    table: SharedTable<String, (SerializedType, Acl, Option<SharedLock>)>,
    closed: SharedTable<ClosedType, (SerializedType, Option<SharedLock>)>,
    cid_ctr: Arc<RwLock<ClosedId>>,
    users: UserManager,
}

const SOURCE_SUPERVISOR: Token = Token(0);
const SOURCE_VM: Token = Token(1);

#[derive(Debug)]
struct MetavisorSession {
    stream: mio::net::TcpStream,
    id: SupervisorId,
    table: SharedTable<String, (SerializedType, Acl, Option<SharedLock>)>,
    closed: SharedTable<ClosedType, (SerializedType, Option<SharedLock>)>,
    cid_ctr: Arc<RwLock<ClosedId>>,
    to_meta: Sender<Message>,
    from_vm: Receiver<Message>,
    vms: HashMap<VmId, (UserId, Sender<Message>, JoinHandle<()>)>,
    vid_ctr: u64,
}

fn check_id(sid: SupervisorId, id: u64) {
    if sid != id {
        panic!("protocol error: received message from wrong supervisor: {} != {}", sid, id);
    }
}

const EVENT_CAPACITY: usize = 1024;

impl Metavisor {
    pub fn new(mesg_addr: SocketAddr, info_addr: SocketAddr,
               user_addr: SocketAddr, users: UserManager) -> Result<Self, Box<dyn std::error::Error>> {
        Ok(Metavisor {
            mesg_listener: TcpListener::bind(mesg_addr)?,
            info_listener: TcpListener::bind(info_addr)?,
            user_listener: TcpListener::bind(user_addr)?,
            id_ctr: 1,  // supervisor id 0 reserved for metavisor
            nodes: HashMap::new(),
            table: Arc::new(RwLock::new(HashMap::new())),
            closed: Arc::new(RwLock::new(HashMap::new())),
            cid_ctr: Arc::new(RwLock::new(0)),
            users: users,
        })
    }

    pub fn run(&mut self) {
        loop {
            let (mut istream, _iaddr) = self.info_listener.accept().unwrap();
            let (mstream, _maddr) = self.mesg_listener.accept().unwrap();
            debug_println!("accepted connection from {_maddr}");

            let id = self.id_ctr;
            self.id_ctr += 1;
            Info::SupervisorIdAssignment(id).send(&mut istream).unwrap();
            debug_println!("assigned id {id} to connection");

            Info::UserManagerAssignment(id, self.users.clone()).send(&mut istream).unwrap();
            debug_println!("sent userlist to new supervisor {id}");

            let mut ms = MetavisorSession::new(mstream, id, self.table.clone(),
                                           self.closed.clone(), self.cid_ctr.clone());
            self.nodes.insert(id, (istream, Builder::new().name(format!("supervisor:{id}")).spawn(move || {
                ms.run();
            }).unwrap()));
        }
    }
}

impl MetavisorSession {
    fn new(std_stream: TcpStream, id: SupervisorId,
           table: SharedTable<String, (SerializedType, Acl, Option<SharedLock>)>,
           closed: SharedTable<ClosedType, (SerializedType, Option<SharedLock>)>,
           cid_ctr: Arc<RwLock<ClosedId>>) -> Self {
        std_stream.set_nonblocking(true).unwrap();
        let (to_meta, from_vm) = channel();
        MetavisorSession {
            stream: mio::net::TcpStream::from_std(std_stream),
            id: id,
            table: table,
            closed: closed,
            cid_ctr: cid_ctr,
            to_meta: to_meta,
            from_vm: from_vm,
            vms: HashMap::new(),
            vid_ctr: 0,
        }
    }

    fn check_lock_held(&self, o_lock: &Option<SharedLock>,
                       sid: SupervisorId, vid: VmId, uid: UserId) -> bool {
        if let Some(lock) = o_lock {
            *lock == (sid, vid, uid)
        } else {
            true  // no lock, so we can access the value
        }
    }

    fn read_sym_req(&self, sid: SupervisorId, vid: VmId,
                    uid: UserId, sym: String) -> Message {
        match self.table.read().unwrap().get(&sym) {
            Some((val, acl, lock)) => {
                if !self.check_lock_held(lock, sid, vid, uid) {
                    return Message::ReadSymbolErr(sid, vid, uid);
                }

                if (acl.owner == uid) || acl.read {
                    Message::ReadSymbolOk(sid, vid, uid, val.clone())
                } else if acl.exec {
                    Message::ReadSymbolOk(sid, vid, uid, Type::UnreadExec(sym.clone()).serialize())
                } else {
                    Message::ReadSymbolErr(sid, vid, uid)
                }
            },
            None => Message::ReadSymbolErr(sid, vid, uid),
        }
    }

    fn write_sym_req(&mut self, sid: SupervisorId, vid: VmId,
                     uid: UserId, sym: String, val: SerializedType) -> Message {
        let et = ExportType::deserialize(&val);

        let mut tab = self.table.write().unwrap();
        let mut cls = self.closed.write().unwrap();

        match (tab.get(&sym), et.acl) {
            (Some((o_val, o_acl, lock)), Some(n_acl)) => {
                if !self.check_lock_held(lock, sid, vid, uid) {
                    return Message::WriteSymbolErr(sid, vid, uid);
                }

                // uid rather than n_acl to prevent funny things
                if o_acl.owner == uid {
                    // closed value cannot be present in both metavisor and VM
                    cls.extend(et.closed.iter().map(|(k, v)| (k.clone(), (v.clone(), None))));
                    let t = Type::deserialize(&o_val);
                    let new_val = match t {
                        Type::Closed(ct) => {
                            if let Some((_, lock)) = cls.get(&ct) {
                                if !self.check_lock_held(lock, sid, vid, uid) {
                                    return Message::WriteSymbolErr(sid, vid, uid);
                                }
                                let new_lock = lock.clone();
                                cls.insert(ct, (et.orig, new_lock));
                            } else {
                                cls.insert(ct, (et.orig, None));
                            }
                            o_val.clone()
                        },
                        _ => et.orig,
                    };
                    let new_lock = lock.clone();
                    tab.insert(sym, (new_val, n_acl, new_lock));
                    Message::WriteSymbolOk(sid, vid, uid)
                } else if o_acl.write {
                    // close value cannot be present in both metavisor and VM
                    cls.extend(et.closed.iter().map(|(k, v)| (k.clone(), (v.clone(), None))));
                    let t = Type::deserialize(&o_val);
                    let new_val = match t {
                        Type::Closed(ct) => {
                            if let Some((_, lock)) = cls.get(&ct) {
                                if !self.check_lock_held(lock, sid, vid, uid) {
                                    return Message::WriteSymbolErr(sid, vid, uid);
                                }
                                let new_lock = lock.clone();
                                cls.insert(ct, (et.orig, new_lock));
                            } else {
                                cls.insert(ct, (et.orig, None));
                            }
                            o_val.clone()
                        },
                        _ => et.orig,
                    };
                    let old_acl = o_acl.clone();  // appease borrow checker
                    let new_lock = lock.clone();
                    tab.insert(sym, (new_val, old_acl, new_lock));
                    Message::WriteSymbolOk(sid, vid, uid)
                } else {
                    Message::WriteSymbolErr(sid, vid, uid)
                }
            },
            (Some((o_val, o_acl, lock)), None) => {
                if !self.check_lock_held(lock, sid, vid, uid) {
                    return Message::WriteSymbolErr(sid, vid, uid);
                }

                if o_acl.write || o_acl.owner == uid {
                    // closed value cannot be present in both metavisor and VM
                    cls.extend(et.closed.iter().map(|(k, v)| (k.clone(), (v.clone(), None))));
                    let t = Type::deserialize(&o_val);
                    match t {
                        Type::Closed(ct) => {
                            if let Some((_, lock)) = cls.get(&ct) {
                                if !self.check_lock_held(lock, sid, vid, uid) {
                                    return Message::WriteSymbolErr(sid, vid, uid);
                                }
                                let new_lock = lock.clone();
                                cls.insert(ct, (et.orig, new_lock));
                            } else {
                                cls.insert(ct, (et.orig, None));
                            }
                        },
                        _ => {
                            let old_acl = o_acl.clone();  // appease borrow checker
                            let new_lock = lock.clone();
                            tab.insert(sym, (et.orig, old_acl, new_lock));
                        },
                    }
                    Message::WriteSymbolOk(sid, vid, uid)
                } else {
                    Message::WriteSymbolErr(sid, vid, uid)
                }
            },
            (None, Some(n_acl)) => {
                // don't need to check closed locks here because any closed
                // value exported from a VM cannot already exist in the shared
                // environment, and so can't be overwritten, but let's check
                // anyhow
                cls.extend(et.closed.iter().map(|(k, v)| (k.clone(), (v.clone(), None))));
                tab.insert(sym, (et.orig, n_acl, None));
                Message::WriteSymbolOk(sid, vid, uid)
            },
            (None, None) => {
                // don't allow a symbol to be bound for the first
                // time without an ACL
                Message::WriteSymbolErr(sid, vid, uid)
            },
        }
    }

    fn exec_sym_req(&mut self, o_sid: SupervisorId, o_vid: VmId,
                    c_uid: UserId, sid: SupervisorId, vid: VmId,
                    sym: String, args: Vec<SerializedType>,
                    waker: Arc<Waker>) -> Option<Message> {
        // we can unwrap the Acl field below because you can't call a symbol
        // like this unless there's an Acl that prevented it
        let uid = match self.table.read().unwrap().get(&sym) {
            Some((_, acl, lock)) => {
                if !self.check_lock_held(lock, sid, vid, c_uid) {
                    return Some(Message::ExecSymbolErr(sid, vid, c_uid,
                                                       Error::InvalidSymbol(sym.clone())
                                                       .into_usize()));
                }
                acl.owner
            },
            None => return Some(Message::ExecSymbolErr(sid, vid, c_uid,
                                                       Error::InvalidSymbol(sym.clone())
                                                       .into_usize())),
        };

        debug_println!("spinning up isolated VM as ({}, {}, {}) on behalf of ({}, {}, {})",
                       sid, vid, uid, o_sid, o_vid, c_uid);

        let (to_vm, from_meta) = channel();
        let tx = self.to_meta.clone();
        let wk = waker.clone();
        self.vms.insert(vid, (uid, to_vm, Builder::new().name(format!("iso_vm:{vid}")).spawn(move || {
            Self::handle_iso_vm(o_sid, o_vid, sid, vid, uid,
                                sym, args, tx, from_meta, wk);
        }).unwrap()));
        None
    }

    fn read_closed_req(&self, sid: SupervisorId, vid: VmId,
                       uid: UserId, ct: ClosedType) -> Message {
        match self.closed.read().unwrap().get(&ct) {
            Some((val, lock)) => {
                if !self.check_lock_held(lock, sid, vid, uid) {
                    Message::ReadClosedErr(sid, vid, uid)
                } else {
                    Message::ReadClosedOk(sid, vid, uid, val.clone())
                }
            },
            None => Message::ReadClosedErr(sid, vid, uid),
        }
    }

    fn write_closed_req(&mut self, sid: SupervisorId, vid: VmId,
                        uid: UserId, ct: ClosedType, val: SerializedType) -> Message {
        let mut map = self.closed.write().unwrap();
        match map.get(&ct) {
            Some((_, lock)) => {
                if !self.check_lock_held(lock, sid, vid, uid) {
                    return Message::WriteClosedErr(sid, vid, uid)
                }
                let new_lock = lock.clone();
                map.insert(ct, (val, new_lock));
            },
            None => { map.insert(ct, (val, None)); },
        }
        Message::WriteClosedOk(sid, vid, uid)
    }

    fn close_req(&mut self, sid: SupervisorId, vid: VmId,
                 uid: UserId, sym: String) -> Message {
        // lock table first, then lock closed, as above in WriteSymbolRequest
        let mut tab = self.table.write().unwrap();
        let mut cls = self.closed.write().unwrap();
        match tab.remove(&sym) {
            Some((val, acl, lock)) => {
                let t = Type::deserialize(&val);
                match t {
                    Type::Closed(ct) => {
                        tab.insert(sym, (val, acl, lock));
                        Message::CloseOk(sid, vid, uid, ct)
                    },
                    _ => {
                        let mut ctr = self.cid_ctr.write().unwrap();
                        let ct = (0, 0, *ctr, 0);
                        cls.insert(ct, (val, lock));
                        *ctr += 1;
                        tab.insert(sym, (Type::Closed(ct).serialize(), acl, None));
                        Message::CloseOk(sid, vid, uid, ct)
                    }
                }
            },
            None => {
                Message::CloseErr(sid, vid, uid)
            }
        }
    }

    fn lock_sym_req(&mut self, sid: SupervisorId, vid: VmId,
                    uid: UserId, sym: String) -> Message {
        let mut tab = self.table.write().unwrap();
        if tab.get(&sym).is_none() {
            return Message::LockSymbolErr(sid, vid, uid);
        }
        let (val, acl, lock) = tab.get(&sym).unwrap();
        match Type::deserialize(&val) {
            Type::Closed(ct) => {
                let mut cls = self.closed.write().unwrap();
                let cv = cls.get(&ct).unwrap();  // must exist
                if cv.1.is_none() {
                    let new_val = cv.0.clone();
                    cls.insert(ct, (new_val, Some((sid, vid, uid))));
                    Message::LockSymbolStatus(sid, vid, uid, true)
                } else {
                    Message::LockSymbolStatus(sid, vid, uid, false)
                }
            },
            _ => {
                if lock.is_none() {
                    let new_val = val.clone();
                    let new_acl = acl.clone();
                    tab.insert(sym, (new_val, new_acl, Some((sid, vid, uid))));
                    Message::LockSymbolStatus(sid, vid, uid, true)
                } else {
                    Message::LockSymbolStatus(sid, vid, uid, false)
                }
            }
        }
    }

    fn unlock_sym_req(&mut self, sid: SupervisorId, vid: VmId,
                      uid: UserId, sym: String) -> Message {
        let mut tab = self.table.write().unwrap();
        if tab.get(&sym).is_none() {
            return Message::UnlockSymbolErr(sid, vid, uid);
        }
        let (val, acl, lock) = tab.get(&sym).unwrap();
        match Type::deserialize(&val) {
            Type::Closed(ct) => {
                let mut cls = self.closed.write().unwrap();
                let cv = cls.get(&ct).unwrap();  // must exist
                match cv.1 {
                    Some((l_sid, l_vid, l_uid)) => {
                        if (l_sid, l_vid, l_uid) == (sid, vid, uid) {
                            let new_val = cv.0.clone();
                            cls.insert(ct, (new_val, None));
                            Message::UnlockSymbolOk(sid, vid, uid)
                        } else {
                            Message::UnlockSymbolErr(sid, vid, uid)
                        }
                    },
                    None => Message::UnlockSymbolErr(sid, vid, uid),
                }
            },
            _ => {
                match lock {
                    Some((l_sid, l_vid, l_uid)) => {
                        if (*l_sid, *l_vid, *l_uid) == (sid, vid, uid) {
                            let new_val = val.clone();
                            let new_acl = acl.clone();
                            tab.insert(sym, (new_val, new_acl, None));
                            Message::UnlockSymbolOk(sid, vid, uid)
                        } else {
                            Message::UnlockSymbolErr(sid, vid, uid)
                        }
                    },
                    None => Message::UnlockSymbolErr(sid, vid, uid)
                }
            }
        }
    }

    fn lock_closed_req(&mut self, sid: SupervisorId, vid: VmId,
                       uid: UserId, ct: ClosedType) -> Message {
        let mut cls = self.closed.write().unwrap();
        let cv = match cls.get(&ct) {
            Some(cv) => cv,
            None => return Message::LockClosedErr(sid, vid, uid),
        };
        if cv.1.is_none() {
            let new_val = cv.0.clone();
            cls.insert(ct, (new_val, Some((sid, vid, uid))));
            Message::LockClosedStatus(sid, vid, uid, true)
        } else {
            Message::LockClosedStatus(sid, vid, uid, false)
        }
    }

    fn unlock_closed_req(&mut self, sid: SupervisorId, vid: VmId,
                         uid: UserId, ct: ClosedType) -> Message {
        let mut cls = self.closed.write().unwrap();
        let cv = match cls.get(&ct) {
            Some(cv) => cv,
            None => return Message::UnlockClosedErr(sid, vid, uid),
        };
        match cv.1 {
            Some((l_sid, l_vid, l_uid)) => {
                if (l_sid, l_vid, l_uid) == (sid, vid, uid) {
                    let new_val = cv.0.clone();
                    cls.insert(ct, (new_val, None));
                    Message::UnlockClosedOk(sid, vid, uid)
                } else {
                    Message::UnlockClosedErr(sid, vid, uid)
                }
            },
            None => Message::UnlockClosedErr(sid, vid, uid)
        }
    }

    fn handle_sock(&mut self, mesg: Message, done_wake: &Arc<Waker>) {
        match mesg {
            Message::ReadSymbolRequest(sid, vid, uid, sym) => {
                check_id(sid, self.id);
                self.read_sym_req(sid, vid, uid, sym).send(&mut self.stream).unwrap();
            },
            Message::WriteSymbolRequest(sid, vid, uid, sym, val) => {
                check_id(sid, self.id);
                self.write_sym_req(sid, vid, uid, sym, val).send(&mut self.stream).unwrap();
            },
            Message::ExecSymbolRequest(sid, vid, uid, sym, args) => {
                check_id(sid, self.id);
                let id = self.vid_ctr;
                self.vid_ctr += 1;
                match self.exec_sym_req(sid, vid, uid, 0, id,
                                        sym, args, done_wake.clone()) {
                    Some(m) => m.send(&mut self.stream).unwrap(),
                    None => (),
                }
            },
            Message::ReadClosedRequest(sid, vid, uid, ct) => {
                check_id(sid, self.id);
                self.read_closed_req(sid, vid, uid, ct).send(&mut self.stream).unwrap();
            },
            Message::WriteClosedRequest(sid, vid, uid, ct, val) => {
                check_id(sid, self.id);
                self.write_closed_req(sid, vid, uid, ct, val).send(&mut self.stream).unwrap();
            },
            Message::CloseRequest(sid, vid, uid, sym) => {
                check_id(sid, self.id);
                self.close_req(sid, vid, uid, sym).send(&mut self.stream).unwrap();
            },
            Message::LockSymbolRequest(sid, vid, uid, sym) => {
                check_id(sid, self.id);
                self.lock_sym_req(sid, vid, uid, sym).send(&mut self.stream).unwrap();
            },
            Message::UnlockSymbolRequest(sid, vid, uid, sym) => {
                check_id(sid, self.id);
                self.unlock_sym_req(sid, vid, uid, sym).send(&mut self.stream).unwrap();
            },
            Message::LockClosedRequest(sid, vid, uid, ct) => {
                check_id(sid, self.id);
                self.lock_closed_req(sid, vid, uid, ct).send(&mut self.stream).unwrap();
            },
            Message::UnlockClosedRequest(sid, vid, uid, ct) => {
                check_id(sid, self.id);
                self.unlock_closed_req(sid, vid, uid, ct).send(&mut self.stream).unwrap();
            },
            e => panic!("protocol error: received invalid message {:?}", e),
        };
    }

    fn handle_iso_vm(o_sid: SupervisorId, o_vid: VmId,
                     sid: SupervisorId, vid: VmId, uid: UserId,
                     /*exec: SerializedType,*/sym: String, s_args: Vec<SerializedType>,
                     tx: Sender<Message>, rx: Receiver<Message>, waker: Arc<Waker>) {
        let args: Vec<Type> = s_args.into_iter().map(|a| Type::deserialize(&a)).collect();
        let exec = ExecBlob {
            version: 0,
            id: String::from("*isolated-vm*"),
            func: Rc::new(Function {
                name: String::from("*isolated-top*"),
                arity: 0,
                localct: 0,
                bytecode: vec![
                    0x60, 0x00, 0x00,  // load const 0 (sym) into r0
                    0x61, 0x00, 0x00,  // load value bound to symbol in r0 into r0
                    0x60, 0x01, 0x01,  // load const 1 (s_args) into r1
                    0x80, 0x00, 0x01,  // call sym(s_args)
                    0x63, 0x02,        // load return value into r2
                    0x82, 0x02,        // return retval
                ],
                consts: vec![Type::String(Rc::new(sym)),
                             Type::Vector(Rc::new(RefCell::new(args)))],
            }),
        };

        let running = Arc::new(AtomicBool::new(true));
        let mut vm = Vm::new(sid, vid, uid, running.clone(),
                             Some((tx.clone(), rx, waker.clone()))).unwrap();
        debug_println!("running VM ({}, {}, {})", sid, vid, uid);
        tx.send(match vm.run(&exec) {
            Ok(VmStatus::Finished(ot)) =>
                Message::ExecDone(o_sid, o_vid, vid, uid, Ok(ot.map(|t| t.serialize()))),
            Ok(VmStatus::Paused) => panic!("vm shouldn't be able to pause here"),
            Err(e) => Message::ExecDone(o_sid, o_vid, vid, uid, Err(e.into_usize()))
        }).unwrap();
        waker.wake().unwrap();
    }

    fn handle_channel(&mut self, mesg: Message, done_wake: &Arc<Waker>)
                      -> (VmId, Option<Message>) {
        match mesg {
            Message::ReadSymbolRequest(sid, vid, uid, sym) =>
                (vid, Some(self.read_sym_req(sid, vid, uid, sym))),
            Message::WriteSymbolRequest(sid, vid, uid, sym, val) =>
                (vid, Some(self.write_sym_req(sid, vid, uid, sym, val))),
            Message::ExecSymbolRequest(sid, vid, uid, sym, args) => {
                let id = self.vid_ctr;
                self.vid_ctr += 1;
                (vid, self.exec_sym_req(sid, vid, 0, id, uid,
                                        sym, args, done_wake.clone()))
            }
            Message::ReadClosedRequest(sid, vid, uid, ct) =>
                (vid, Some(self.read_closed_req(sid, vid, uid, ct))),
            Message::WriteClosedRequest(sid, vid, uid, ct, val) =>
                (vid, Some(self.write_closed_req(sid, vid, uid, ct, val))),
            Message::CloseRequest(sid, vid, uid, sym) =>
                (vid, Some(self.close_req(sid, vid, uid, sym))),
            Message::LockSymbolRequest(sid, vid, uid, sym) =>
                (vid, Some(self.lock_sym_req(sid, vid, uid, sym))),
            Message::UnlockSymbolRequest(sid, vid, uid, sym) =>
                (vid, Some(self.unlock_sym_req(sid, vid, uid, sym))),
            Message::LockClosedRequest(sid, vid, uid, ct) =>
                (vid, Some(self.lock_closed_req(sid, vid, uid, ct))),
            Message::UnlockClosedRequest(sid, vid, uid, ct) =>
                (vid, Some(self.unlock_closed_req(sid, vid, uid, ct))),
            e => panic!("protocol error: received invalid message {:?}", e),
        }
    }

    fn run(&mut self) {
        let mut poll = Poll::new().unwrap();
        let mut events = Events::with_capacity(EVENT_CAPACITY);
        poll.registry().register(&mut self.stream, SOURCE_SUPERVISOR, Interest::READABLE).unwrap();

        let done_wake = Arc::new(Waker::new(&poll.registry(), SOURCE_VM).unwrap());

        loop {
            poll.poll(&mut events, None).unwrap();
            for ev in events.iter() {
                match ev.token() {
                    SOURCE_SUPERVISOR => {
                        let mut buf = [0; 1];
                        while match self.stream.try_io(|| { self.stream.peek(&mut buf) }) {
                            Ok(sz) => sz,
                            _ => 0,
                        } > 0 {
                            let mesg = match Message::recv(&mut self.stream) {
                                Ok(m) => m,
                                Err(_e) => {
                                    debug_println!("received bad message: {:?}", _e);
                                    return ();
                                }
                            };

                            debug_println!("metavisor received message: {:?}", mesg);

                            self.handle_sock(mesg, &done_wake);
                        }
                    },
                    SOURCE_VM => {
                        let mesg = self.from_vm.recv().unwrap();
                        debug_println!("metavisor received message: {:?}", mesg);
                        let mesg_c = mesg.clone();
                        let (vid, out) = match mesg {
                            Message::WriteSymbolRequest(sid, vid, uid, sym, _) => {
                                let info = self.vms.get(&vid).unwrap();
                                {  // make sure tab goes out of scope
                                    let tab = self.table.read().unwrap();
                                    let oval = tab.get(&sym);
                                    if oval.is_none() {
                                        info.1.send(Message::WriteSymbolErr(sid, vid, uid)).unwrap();
                                        continue;
                                    }

                                    let val = oval.unwrap();
                                    if info.0 != val.1.owner {
                                        info.1.send(Message::WriteSymbolErr(sid, vid, uid)).unwrap();
                                        continue;
                                    }
                                }

                                self.handle_channel(mesg_c, &done_wake)
                            },
                            Message::WriteClosedRequest(sid, vid, uid, ct, _) => {
                                let info = self.vms.get(&vid).unwrap();
                                if info.0 != ct.3 {
                                    info.1.send(Message::WriteClosedErr(sid, vid, uid)).unwrap();
                                    (vid, None)
                                } else {
                                    self.handle_channel(mesg_c, &done_wake)
                                }
                            },
                            Message::ExecDone(o_sid, o_vid, vid, uid, out) => {
                                let info = self.vms.remove(&vid).unwrap();
                                let _ = info.2.join();
                                self.vms.remove(&vid);
                                let omsg = match out {
                                    Ok(ot) => Message::ExecSymbolOk(o_sid, o_vid,
                                                                    uid, ot),
                                    Err(e) => Message::ExecSymbolErr(o_sid, o_vid,
                                                                     uid, e),
                                };
                                if o_sid == 0 {
                                    self.vms.get(&o_vid).unwrap().1.send(omsg).unwrap();
                                } else {
                                    omsg.send(&mut self.stream).unwrap();
                                }
                                (vid, None)
                            },
                            _ => self.handle_channel(mesg_c, &done_wake),
                        };
                        if let Some(out_mesg) = out {
                            self.vms.get(&vid).unwrap().1.send(out_mesg).unwrap();
                        }
                    },
                    tok => panic!("received unknown token: {:?}", tok),
                }
            }
        }
    }
}
