use serde::Serialize;

use crate::frame::BackTrace;
use crate::types::Type;

#[derive(Debug, Serialize)]
pub struct ResultBlob {
    pub version: u8,
    pub id: String,
    pub result: Option<Type>,
    pub error: Option<usize>,
    pub context: Option<BackTrace>
}
