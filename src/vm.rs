use std::cell::RefCell;
use std::fmt;
use std::rc::Rc;
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::mpsc::{Receiver, Sender};
use debug_print::debug_println;
use mio::Waker;

use crate::acl::Acl;
use crate::closure::Closure;
use crate::error::Error;
use crate::execblob::ExecBlob;
use crate::frame::{BackTrace, Frame};
use crate::function::Function;
use crate::instructions::Instruction;
use crate::opcodes::OpCode;
use crate::message::Message;
use crate::scope::ScopeManager;
use crate::supervisor::SupervisorId;
use crate::user::UserId;
use crate::types::{Type, Execute, Executable};

pub type VmId = u64;

#[derive(Debug)]
pub struct Vm {
    sid: SupervisorId,
    vid: VmId,
    uid: UserId,
    scope: ScopeManager,
    retval: Option<Type>,
    running: Arc<AtomicBool>,
}

#[derive(Debug)]
pub enum VmStatus {
    Paused,
    Finished(Option<Type>),
}

macro_rules! math_func {
    ($name:ident, $op:tt, $checked:ident, $bzbad:expr) => {
        fn $name(&mut self, r1: &u8, r2: &u8, r3: &u8) -> Result<(), Error> {
            let t2 = self.unclose(self.read_reg(r2)?)?;
            let t3 = match self.unclose(self.read_reg(r3)?)? {
                Type::Integer(i) => if $bzbad && i == 0 {
                     return Err(Error::ZeroError)
                } else {
                    Type::Integer(i)
                },
                Type::Float(f) => if $bzbad && f == 0.0 {
                    return Err(Error::ZeroError)
                } else {
                    Type::Float(f)
                },
                x => x
            };
            self.write_reg(r1, match (t2, t3) {
                (Type::Integer(a), Type::Integer(b)) => Type::Integer(a.$checked(b).ok_or(Error::ArithOverflow)?),
                (Type::Integer(a), Type::Float(b)) => Type::Float((a as f64) $op b),
                (Type::Float(a), Type::Integer(b)) => Type::Float(a $op (b as f64)),
                (Type::Float(a), Type::Float(b)) => Type::Float(a $op b),
                (a, b) => return Err(Error::BadTwoOperand(stringify!($name), a.clone(), b.clone())),
            })
        }
    };
}

macro_rules! bit_func {
    ($name:ident, $op:tt) => {
        fn $name(&mut self, r1: &u8, r2: &u8, r3: &u8) -> Result<(), Error> {
            let t2 = self.unclose(self.read_reg(r2)?)?;
            let t3 = self.unclose(self.read_reg(r3)?)?;
            self.write_reg(r1, match (t2, t3) {
                (Type::Integer(a), Type::Integer(b)) => Type::Integer(a $op b),
                (a, b) => return Err(Error::BadTwoOperand(stringify!($name), a.clone(), b.clone())),
            })
        }
    };
}

macro_rules! cmp_func {
    ($name:ident, $op:tt) => {
        fn $name(&mut self, r1: &u8, r2: &u8, r3: &u8) -> Result<(), Error> {
            let t2 = self.unclose(self.read_reg(r2)?)?;
            let t3 = self.unclose(self.read_reg(r3)?)?;
            self.write_reg(r1, match (t2, t3) {
                (Type::Integer(a), Type::Integer(b)) => Type::Integer((a $op b) as i64),
                (Type::Integer(a), Type::Float(b)) => Type::Integer(((a as f64) $op b) as i64),
                (Type::Float(a), Type::Integer(b)) => Type::Integer((a $op (b as f64)) as i64),
                (Type::Float(a), Type::Float(b)) => Type::Integer((a $op b) as i64),
                (a, b) => return Err(Error::BadTwoOperand(stringify!($name), a.clone(), b.clone())),
            })
        }
    };
}

impl Vm {
    pub fn new(sid: SupervisorId, vid: VmId, uid: UserId, running: Arc<AtomicBool>,
               channel: Option<(Sender<Message>, Receiver<Message>, Arc<Waker>)>) -> Result<Self, Error> {
        let fr = Frame::new_rcref(None,
                                  Executable::Function(Function::new(String::from("<interpreter>"),
                                                                     0, 0, vec![], vec![])),
                                  vec![])?;
        Ok(Vm {
            sid: sid,
            vid: vid,
            uid: uid,
            scope: ScopeManager::new(sid, vid, uid, fr, channel),
            retval: None,
            running: running,
        })
    }

    pub fn get_retval(&self) -> Result<Option<Type>, Error> {
        match &self.retval {
            Some(t) => Ok(Some(self.unclose(t.clone())?)),
            None => Ok(None),
        }
    }

    pub fn make_backtrace(&self) -> BackTrace {
        Frame::make_backtrace(self.scope.ctx.clone())
    }

    pub fn reset(&mut self) {
        self.scope.reset();
        self.retval = None;
    }

    pub fn run(&mut self, exec: &ExecBlob) -> Result<VmStatus, Error> {
        self.scope.ctx.borrow_mut().func = Executable::Function(exec.func.clone());
        self.resume()
    }

    pub fn run_sanitize_closed(&mut self, exec: &ExecBlob) -> Result<VmStatus, Error> {
        for c in &exec.func.consts {
            if c.find_all_closed().len() > 0 {
                // prevent front-end from trying to directly access
                // (potentially shared) closed values.  access these
                // from symbol bindings instead.  TODO: revisit this
                // decision when developing front-end debugger!
                return Err(Error::InvalidClosed);
            }
        }
        self.run(exec)
    }

    pub fn run_func(&mut self, exec: Executable, args: Vec<Type>) -> Result<VmStatus, Error> {
        self.scope.ctx.borrow_mut().func = exec.clone();
        self.scope.ctx.borrow_mut().args = args.clone();
        self.resume()
    }

    pub fn resume(&mut self) -> Result<VmStatus, Error> {
        self.running.store(true, Ordering::Release);
        while self.running.load(Ordering::Acquire) {
            debug_println!("Now executing at pc={} in {}",
                           self.scope.ctx.borrow().pc, self.scope.ctx.borrow().func.name());

            let instr = self.scope.ctx.borrow().func.instr_at(self.scope.ctx.borrow().pc)?;
            debug_println!("Decoded instruction {}", instr);

            match instr {
                Instruction::ZeroReg(op) => match op {
                    OpCode::Nop => (),  // do nothing, it's a no-op!
                    _ => panic!("encountered invalid zero-reg opcode {}", op)
                },

                Instruction::OneReg(op, r1) => match op {
                    OpCode::Push => self.push(&r1)?,
                    OpCode::Pop => self.pop(&r1)?,

                    OpCode::LoadRet => self.load_ret(&r1)?,

                    OpCode::Return => {
                        self.return_to_parent(&r1)?;
                        if !self.running.load(Ordering::Acquire) {
                            return Ok(VmStatus::Finished(self.get_retval()?));
                        }
                        continue;  // skip the rest of this loop
                    },
                    OpCode::Jump => self.jump(&r1)?,
                    OpCode::JumpImm => self.jump_imm(&r1)?,

                    OpCode::UnlockSym => self.unlock_sym(&r1)?,
                    OpCode::UnlockClosed => self.unlock_closed(&r1)?,

                    OpCode::PrintReg => {
                        println!("DEBUG: r{}={:?}", r1, self.read_reg(&r1)?);
                    }
                    _ => panic!("encountered invalid one-reg opcode {}", op)
                },

                Instruction::TwoReg(op, r1, r2) => match op {
                    OpCode::Not => self.not(&r1, &r2)?,

                    OpCode::Car => self.car(&r1, &r2)?,
                    OpCode::Cdr => self.cdr(&r1, &r2)?,

                    OpCode::LoadConst => self.load_const(&r1, &r2)?,
                    OpCode::LoadSym => self.load_sym(&r1, &r2)?,
                    OpCode::StoreSym => self.store_sym(&r1, &r2)?,
                    OpCode::Move => self.write_reg(&r1, self.read_reg(&r2)?.clone())?,
                    OpCode::CloseSym => self.close_sym(&r1, &r2)?,
                    OpCode::LoadClosed => self.load_closed(&r1, &r2)?,
                    OpCode::StoreClosed => self.store_closed(&r1, &r2)?,

                    OpCode::Append => self.append(&r1, &r2)?,
                    OpCode::Remove => self.remove(&r1, &r2)?,
                    OpCode::Length => self.length(&r1, &r2)?,

                    OpCode::Call => {
                        self.call(&r1, &r2)?;
                        continue;  // skip the rest of this loop
                    },
                    OpCode::TailCall => {
                        self.tail_call(&r1, &r2)?;
                        continue;  // skip the rest of this loop
                    },
                    OpCode::JumpIf => self.jump_if(&r1, &r2)?,
                    OpCode::JumpImmIf => self.jump_imm_if(&r1, &r2)?,

                    OpCode::New => self.write_reg(&r1, Type::from_typecode(&r2)?)?,
                    OpCode::StackVec => self.stack_vec(&r1, &r2)?,
                    OpCode::LitInt => self.write_reg(&r1, Type::Integer((r2 as i8) as i64))?,
                    OpCode::DupClosed => self.write_reg(&r1, self.scope.dup_closed(r2)?)?,

                    OpCode::LockSym => self.lock_sym(&r1, &r2)?,
                    OpCode::LockClosed => self.lock_closed(&r1, &r2)?,

                    _ => panic!("encountered invalid two-reg opcode {}", op)
                },

                Instruction::ThreeReg(op, r1, r2, r3) => match op {
                    OpCode::Add => self.add_poly(&r1, &r2, &r3)?,
                    OpCode::Subtract => self.subtract(&r1, &r2, &r3)?,
                    OpCode::Multiply => self.multiply(&r1, &r2, &r3)?,
                    OpCode::Divide => self.divide(&r1, &r2, &r3)?,
                    OpCode::Modulo => self.modulo(&r1, &r2, &r3)?,

                    OpCode::And => self.and(&r1, &r2, &r3)?,
                    OpCode::Or => self.or(&r1, &r2, &r3)?,
                    OpCode::Xor => self.xor(&r1, &r2, &r3)?,

                    OpCode::Less => self.less(&r1, &r2, &r3)?,
                    OpCode::Equal => self.equal(&r1, &r2, &r3)?,
                    OpCode::Greater => self.greater(&r1, &r2, &r3)?,
                    OpCode::LessEqual => self.less_equal(&r1, &r2, &r3)?,
                    OpCode::GreaterEqual => self.greater_equal(&r1, &r2, &r3)?,
                    OpCode::NotEqual => self.not_equal(&r1, &r2, &r3)?,
                    OpCode::CmpType => self.cmp_type(&r1, &r2, &r3)?,

                    OpCode::Cons => self.cons(&r1, &r2, &r3)?,

                    OpCode::LoadArg => self.write_reg(&r1, self.unclose(self.scope.read_arg(r2, r3)?)?)?,
                    OpCode::StoreArg => self.scope.write_arg(r2, r3, self.unclose(self.read_reg(&r1)?)?)?,
                    OpCode::CloseArg => self.close_arg(&r1, &r2, &r3)?,

                    OpCode::LoadLocal => self.write_reg(&r1, self.unclose(self.scope.read_local(r2, r3)?)?)?,
                    OpCode::StoreLocal => self.scope.write_local(r2, r3, self.unclose(self.read_reg(&r1)?)?)?,
                    OpCode::CloseLocal => self.close_local(&r1, &r2, &r3)?,

                    OpCode::Get => self.get_from(&r1, &r2, &r3)?,
                    OpCode::Set => self.set_in(&r1, &r2, &r3)?,
                    // OpCode::Insert => self.insert(&r1, &r2, &r3)?,

                    OpCode::Export => self.export(&r1, &r2, &r3)?,
                    OpCode::CloseFunc => self.close_func(&r1, &r2, &r3)?,

                    _ => panic!("encountered invalid three-reg opcode {}", op)
                },
            }

            self.scope.ctx.borrow_mut().pc += match instr {
                Instruction::ZeroReg(_)           => 1,
                Instruction::OneReg(_, _)         => 2,
                Instruction::TwoReg(_, _, _)      => 3,
                Instruction::ThreeReg(_, _, _, _) => 4,
            };

            if self.scope.ctx.borrow().pc >= self.scope.ctx.borrow().func.max_instr_addr() {
                self.implicit_return_to_parent()?;
                if self.scope.is_ctx_root() {
                    return Ok(VmStatus::Finished(self.get_retval()?));
                }
            }
        }

        Ok(VmStatus::Paused)
    }

    fn read_reg(&self, reg: &u8) -> Result<Type, Error> {
        match self.scope.ctx.borrow().regs.get(reg) {
            Some(t) => Ok(t.clone()),
            None => Err(Error::ReadBeforeWrite(*reg)),
        }
    }

    fn write_reg(&mut self, reg: &u8, value: Type) -> Result<(), Error> {
        self.scope.ctx.borrow_mut().regs.insert(*reg, value);
        Ok(())
    }

    fn unclose(&self, closed: Type) -> Result<Type, Error> {
        match closed {
            Type::Closed(cd) => match self.scope.unclose(cd)? {
                Type::Unbound() => Err(Error::InvalidClosed),
                t => Ok(t),
            },
            t => Ok(t),
        }
    }

    fn append(&mut self, r1: &u8, r2: &u8) -> Result<(), Error> {
        let v = match self.unclose(self.read_reg(r1)?)? {
            Type::Vector(v) => v.clone(),
            t => return Err(Error::NotAVector(t)),
        };
        let mut item = vec![self.read_reg(r2)?.clone()];

        v.borrow_mut().append(&mut item);

        Ok(())
    }

    fn remove(&mut self, r1: &u8, r2: &u8) -> Result<(), Error> {
        let v = match self.unclose(self.read_reg(r1)?)? {
            Type::Vector(v) => v.clone(),
            t => return Err(Error::NotAVector(t)),
        };
        let mut idx = match self.unclose(self.read_reg(r2)?)? {
            Type::Integer(i) => i,
            _ => return Err(Error::InvalidVectorIndex),
        };

        if idx > 0 {
            if (idx as usize) >= v.borrow().len() {
                return Err(Error::InvalidVectorIndex);
            }
        } else {
            idx = (v.borrow().len() as i64) + idx;
            if idx < 0 {
                return Err(Error::InvalidVectorIndex);
            }
        }

        v.borrow_mut().remove(idx as usize);
        Ok(())
    }

    fn length(&mut self, r1: &u8, r2: &u8) -> Result<(), Error> {
        let v = match self.unclose(self.read_reg(r2)?)? {
            Type::Vector(v) => v.clone(),
            t => return Err(Error::NotAVector(t)),
        };
        let len = v.borrow().len() as i64;
        self.write_reg(r1, Type::Integer(len))
    }

    fn call(&mut self, r1: &u8, r2: &u8) -> Result<(), Error> {
        let args = match self.read_reg(r2)? {
            Type::Vector(v) => v.borrow().clone(),
            t => return Err(Error::BadArgs(t)),
        };

        let func_type = self.unclose(self.read_reg(r1)?)?;
        if let Type::UnreadExec(sym) = func_type {
            for a in args.iter() {
                for ct in a.find_all_closed() {
                    self.scope.export_closed(ct)?;
                }
            }
            let ret = self.scope.call_isolated_shared(&sym, &args);
            // Call is a 2-reg instruction, meaning it's 3 bytes long
            // Call instructions normally bypass the pc increment
            // since they usually have a call frame
            self.scope.ctx.borrow_mut().pc += 3;
            return ret;
        }

        let func = match self.unclose(self.read_reg(r1)?)? {
            Type::Function(f) => Executable::Function(f.clone()),
            Type::Closure(c) =>  Executable::Closure(c.clone()),
            t => return Err(Error::CantCall(t)),
        };

        // increment pc so we're in the right place when we return back here
        self.scope.ctx.borrow_mut().pc += 3;

        self.scope.ctx = Frame::new_rcref(Some(self.scope.ctx.clone()), func, args)?;

        Ok(())
    }

    fn tail_call(&mut self, r1: &u8, r2: &u8) -> Result<(), Error> {
        let func = match self.unclose(self.read_reg(r1)?)? {
            Type::Function(f) => Executable::Function(f.clone()),
            Type::Closure(c) => Executable::Closure(c.clone()),
            t => return Err(Error::CantCall(t)),
        };

        let args = match self.read_reg(r2)? {
            Type::Vector(v) => v.borrow().clone(),
            t => return Err(Error::BadArgs(t)),
        };

        {  // scope for mutable ctx borrow
            let mut c = self.scope.ctx.borrow_mut();
            c.stack.clear();
            c.func = func;
            c.args = args;
            c.pc = 0;
            c.retval = None;
            c.regs.drain();
        }

        Ok(())
    }

    fn jump(&mut self, r1: &u8) -> Result<(), Error> {
        let offset = match self.read_reg(r1)? {
            Type::Integer(i) => i,
            t => return Err(Error::InvalidJumpOffset(t)),
        };

        if offset < 0 {
            self.scope.ctx.borrow_mut().pc -= offset.abs() as usize;
        } else {
            self.scope.ctx.borrow_mut().pc += offset as usize;
        }
        Ok(())
    }

    fn jump_imm(&mut self, r1: &u8) -> Result<(), Error> {
        let offset = *r1 as i8;
        if offset < 0 {
            self.scope.ctx.borrow_mut().pc -= offset.abs() as usize;
        } else {
            self.scope.ctx.borrow_mut().pc += offset as usize;
        }
        Ok(())
    }

    fn common_jump_if(&mut self, offset: i64, cond: Type) -> bool {
        match cond {
            Type::Empty() => false,
            Type::Integer(i) => {
                if i == 0 {
                    false
                } else {
                    if offset < 0 {
                        self.scope.ctx.borrow_mut().pc -= offset.abs() as usize;
                    } else {
                        self.scope.ctx.borrow_mut().pc += offset as usize;
                    }
                    true
                }
            },
            Type::Float(f) => {
                if f == 0.0 {
                    false
                } else {
                    if offset < 0 {
                        self.scope.ctx.borrow_mut().pc -= offset.abs() as usize;
                    } else {
                        self.scope.ctx.borrow_mut().pc += offset as usize;
                    }
                    true
                }
            },
            _ => {
                if offset < 0 {
                    self.scope.ctx.borrow_mut().pc -= offset.abs() as usize;
                } else {
                    self.scope.ctx.borrow_mut().pc += offset as usize;
                }
                true
            },
        }
    }

    fn jump_if(&mut self, r1: &u8, r2: &u8) -> Result<(), Error> {
        let offset = match self.read_reg(r1)? {
            Type::Integer(i) => i,
            t => return Err(Error::InvalidJumpOffset(t)),
        };
        self.common_jump_if(offset as i64, self.read_reg(r2)?);
        Ok(())
    }

    fn jump_imm_if(&mut self, r1: &u8, r2: &u8) -> Result<(), Error> {
        let offset = *r1 as i8;
        self.common_jump_if(offset as i64, self.read_reg(r2)?);
        Ok(())
    }

    fn implicit_return_to_parent(&mut self) -> Result<(), Error> {
        if self.scope.is_ctx_root() {
            self.retval = None;
            self.running.store(false, Ordering::Release);
        } else {
            match &self.scope.ctx.borrow().parent {
                Some(p) => p.borrow_mut().retval = Some(Type::Nil()),
                None => return Err(Error::CantReturn),
            }
            let parent = self.scope.ctx.borrow().parent.as_ref().unwrap().clone();
            self.scope.ctx = parent;
        }

        Ok(())
    }

    fn return_to_parent(&mut self, r1: &u8) -> Result<(), Error> {
        let t1 = self.unclose(self.read_reg(r1)?)?;
        if self.scope.is_ctx_root() {
            self.retval = Some(t1);
            self.running.store(false, Ordering::Release);
        } else {
            match &self.scope.ctx.borrow().parent {
                Some(p) => p.borrow_mut().retval = Some(t1.clone()),
                None => return Err(Error::CantReturn),
            }
            let parent = self.scope.ctx.borrow().parent.as_ref().unwrap().clone();
            // outside ctx borrow context
            self.scope.ctx = parent;
        }

        Ok(())
    }

    fn load_ret(&mut self, r1: &u8) -> Result<(), Error> {
        let val = match &self.scope.ctx.borrow().retval {
            Some(rv) => rv.clone(),
            None => Type::Nil(),
        };
        self.write_reg(r1, val)
    }

    fn close_sym(&mut self, r1: &u8, r2: &u8) -> Result<(), Error> {
        let sym = match self.read_reg(r2)? {
            Type::String(s) => s.clone(),
            _ => return Err(Error::InvalidSymbolType),
        };
        let t = Type::Closed(self.scope.close_sym(&*sym)?);
        self.write_reg(r1, t)
    }

    fn load_closed(&mut self, r1: &u8, r2: &u8) -> Result<(), Error> {
        match self.scope.load_closed(*r2)? {
            Type::Unbound() => Err(Error::InvalidClosed),
            t => self.write_reg(r1, t)
        }
    }

    fn store_closed(&mut self, r1: &u8, r2: &u8) -> Result<(), Error> {
        let t = self.unclose(self.read_reg(r1)?)?;
        self.scope.store_closed(t, *r2)
    }

    fn not(&mut self, r1: &u8, r2: &u8) -> Result<(), Error> {
        let t2 = self.unclose(self.read_reg(r2)?)?;
        self.write_reg(r1, match t2 {
            Type::Integer(i) => Type::Integer(!i),
            t => return Err(Error::BadOneOperand("not", t.clone()))
        })
    }

    fn car(&mut self, r1: &u8, r2: &u8) -> Result<(), Error> {
        let t2 = match self.unclose(self.read_reg(r2)?)? {
            Type::List(ls) => ls.clone(),
            t => return Err(Error::InvalidListOperand("car", t.clone())),
        };
        self.write_reg(r1, t2.car.clone())
    }

    fn cdr(&mut self, r1: &u8, r2: &u8) -> Result<(), Error> {
        let t2 = match self.unclose(self.read_reg(r2)?)? {
            Type::List(ls) => ls.clone(),
            t => return Err(Error::InvalidListOperand("cdr", t.clone())),
        };
        self.write_reg(r1, t2.cdr.clone())
    }

    fn close_arg(&mut self, r1: &u8, r2: &u8, r3: &u8) -> Result<(), Error> {
        let t = Type::Closed(self.scope.close_arg(*r2, *r3)?);
        self.write_reg(r1, t)
    }

    fn close_local(&mut self, r1: &u8, r2: &u8, r3: &u8) -> Result<(), Error> {
        let t = Type::Closed(self.scope.close_local(*r2, *r3)?);
        self.write_reg(r1, t)
    }

    fn add_poly(&mut self, r1: &u8, r2: &u8, r3: &u8) -> Result<(), Error> {
        let t2 = self.unclose(self.read_reg(r2)?)?;
        let t3 = self.unclose(self.read_reg(r3)?)?;

        match (t2, t3) {
            (Type::Vector(v2), Type::Vector(v3)) => {
                let mut new = v2.borrow().clone();
                new.extend_from_slice(v3.borrow().as_slice());
                self.write_reg(r1, Type::Vector(Rc::new(RefCell::new(new))))
            },
            _ => self.add(r1, r2, r3)
        }
    }

    fn cmp_type(&mut self, r1: &u8, r2: &u8, r3: &u8) -> Result<(), Error> {
        let t2 = self.unclose(self.read_reg(r2)?)?;
        let t3 = self.unclose(self.read_reg(r3)?)?;

        self.write_reg(r1, if t2.to_typecode()? == t3.to_typecode()? {
            Type::Integer(1)
        } else {
            Type::Integer(0)
        })
    }

    fn cons(&mut self, r1: &u8, r2: &u8, r3: &u8) -> Result<(), Error> {
        let t2 = self.read_reg(r2)?;
        let t3 = self.read_reg(r3)?;
        self.write_reg(r1, Type::cons(t2, t3))
    }

    fn load_const(&mut self, r1: &u8, r2: &u8) -> Result<(), Error> {
        let c = self.scope.ctx.borrow().func.get_const(*r2)?;
        match c {
            Type::Unbound() => Err(Error::InvalidConst(*r2)),
            c => self.write_reg(r1, c),
        }
    }

    fn load_sym(&mut self, r1: &u8, r2: &u8) -> Result<(), Error> {
        let sym = match self.unclose(self.read_reg(r2)?)? {
            Type::String(s) => s.clone(),
            _ => return Err(Error::InvalidSymbolType),
        };
        self.write_reg(r1, self.unclose(self.scope.lookup(&*sym)?)?)
    }

    fn store_sym(&mut self, r1: &u8, r2: &u8) -> Result<(), Error> {
        let sym = match self.unclose(self.read_reg(r2)?)? {
            Type::String(s) => s.clone(),
            _ => return Err(Error::InvalidSymbolType),
        };
        self.scope.update(&*sym, self.read_reg(r1)?.clone())
    }

    fn as_int(t: &Type) -> Option<i64> {
        match t {
            Type::Integer(i) => Some(*i),
            _ => None
        }
    }

    fn export(&mut self, r1: &u8, r2: &u8, r3: &u8) -> Result<(), Error> {
        let sym = match self.read_reg(r1)? {
            Type::String(s) => s.clone(),
            _ => return Err(Error::InvalidSymbolType),
        };
        let t2 = self.read_reg(r2)?;
        let perms = match self.read_reg(r3)? {
            Type::Vector(v) => {
                let vb = v.borrow();
                if vb.len() == 3 {
                    Some(Acl {
                        owner: self.uid,
                        read: Self::as_int(vb.get(0).unwrap()).ok_or(Error::InvalidExportPerm)? != 0,
                        write: Self::as_int(vb.get(1).unwrap()).ok_or(Error::InvalidExportPerm)? != 0,
                        exec: Self::as_int(vb.get(2).unwrap()).ok_or(Error::InvalidExportPerm)? != 0,
                    })
                } else if vb.len() == 0 {
                    Some(Acl {
                        owner: self.uid,
                        read: true,
                        write: false,
                        exec: false,
                    })
                } else {
                    return Err(Error::InvalidExportPerm)
                }
            },
            _ => return Err(Error::InvalidExportPerm)
        };
        self.scope.export(&sym, t2, perms)?;
        Ok(())
    }

    fn get_from(&mut self, r1: &u8, r2: &u8, r3: &u8) -> Result<(), Error> {
        let t2 = self.unclose(self.read_reg(r2)?)?;
        let t3 = self.unclose(self.read_reg(r3)?)?;
        match t2 {
            Type::Vector(v) => match t3 {
                Type::Integer(i) =>
                    self.write_reg(r1, v.borrow().get(i as usize)
                                   .ok_or(Error::IndexOutOfBounds(t3.clone()))?
                                   .clone()),
                _ => Err(Error::InvalidVectorIndex),
            },
            Type::HashMap(h) => match t3 {
                Type::Integer(_) | Type::String(_) | Type::Byte(_) =>
                    self.write_reg(r1, h.borrow().get(&t3)
                                   .ok_or(Error::IndexOutOfBounds(t3.clone()))?
                                   .clone()),
                _ => Err(Error::InvalidHashIndex),
            },
            t => Err(Error::InvalidGetType(t)),
        }
    }

    fn set_in(&mut self, r1: &u8, r2: &u8, r3: &u8) -> Result<(), Error> {
        let t2 = self.unclose(self.read_reg(r2)?)?;
        let t3 = self.unclose(self.read_reg(r3)?)?;
        match self.unclose(self.read_reg(r1)?)? {
            Type::Vector(v) => match t3 {
                Type::Integer(i) => {
                    if i < 0 || (i as usize) >= v.borrow().len() {
                        Err(Error::IndexOutOfBounds(t3.clone()))
                    } else {
                        v.borrow_mut()[i as usize] = t2.clone();
                        Ok(())
                    }
                },
                _ => Err(Error::InvalidVectorIndex)
            },
            Type::HashMap(h) => match t3 {
                Type::Integer(_) | Type::String(_) | Type::Byte(_) => {
                    h.borrow_mut().insert(t3.clone(), t2.clone());
                    Ok(())
                },
                _ => Err(Error::InvalidHashIndex),
            },
            t => Err(Error::InvalidSetType(t)),
        }
    }

    fn push(&mut self, r1: &u8) -> Result<(), Error> {
        let t1 = self.read_reg(r1)?;
        self.scope.ctx.borrow_mut().stack.push(t1);
        Ok(())
    }

    fn pop(&mut self, r1: &u8) -> Result<(), Error> {
        let value = self.scope.ctx.borrow_mut().stack.pop().
            ok_or(Error::StackUnderflow)?;
        self.write_reg(r1, value)
    }

    fn unlock_sym(&mut self, r1: &u8) -> Result<(), Error> {
        let sym = match self.read_reg(r1)? {
            Type::String(s) => s.clone(),
            _ => return Err(Error::InvalidSymbolType),
        };
        self.scope.unlock_sym(&sym)
    }

    fn unlock_closed(&mut self, r1: &u8) -> Result<(), Error> {
        self.scope.unlock_closed(r1)
    }

    fn stack_vec(&mut self, r1: &u8, r2: &u8) -> Result<(), Error> {
        let count = match self.read_reg(r2)? {
            Type::Integer(i) => i,
            t => return Err(Error::InvalidStackOffset(t)),
        };

        let mut v = Vec::new();
        for _ in 0..count {
            v.push(self.scope.ctx.borrow_mut().stack.pop().
                   ok_or(Error::StackUnderflow)?);
        }
        self.write_reg(r1, Type::Vector(Rc::new(RefCell::new(v))))
    }

    fn lock_sym(&mut self, r1: &u8, r2: &u8) -> Result<(), Error> {
        let sym = match self.read_reg(r2)? {
            Type::String(s) => s.clone(),
            _ => return Err(Error::InvalidSymbolType),
        };
        let res = self.scope.lock_sym(&sym)?;
        self.write_reg(&r1, if res {
            Type::Integer(1)
        } else {
            Type::Integer(0)
        })
    }

    fn lock_closed(&mut self, r1: &u8, r2: &u8) -> Result<(), Error> {
        let res = self.scope.lock_closed(r2)?;
        self.write_reg(&r1, if res {
            Type::Integer(1)
        } else {
            Type::Integer(0)
        })
    }

    fn close_func(&mut self, r1: &u8, r2: &u8, r3: &u8) -> Result<(), Error> {
        let args = match self.read_reg(r3)? {
            Type::Vector(v) => v.borrow().clone(),
            t => return Err(Error::CantClose(t)),
        };

        let mut closedargs = Vec::new();
        for a in args.iter() {
            match a {
                Type::Closed(cd) => closedargs.push(*cd),
                _ => return Err(Error::CantClose(self.read_reg(r3)?)),
            }
        }

        self.write_reg(r1, match self.read_reg(r2)? {
            Type::Function(f) => Type::Closure(Rc::new(Closure::new(f, closedargs))),
            t => return Err(Error::CantClose(t)),
        })
    }

    math_func!{add, +, checked_add, false}
    math_func!{subtract, -, checked_sub, false}
    math_func!{multiply, *, checked_mul, false}
    math_func!{divide, /, checked_div, true}
    math_func!{modulo, %, checked_rem, true}

    bit_func!{and, &}
    bit_func!{or, |}
    bit_func!{xor, ^}

    cmp_func!{less, <}
    cmp_func!{equal, ==}
    cmp_func!{greater, >}
    cmp_func!{less_equal, <=}
    cmp_func!{greater_equal, >=}
    cmp_func!{not_equal, !=}
}

impl fmt::Display for Vm {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "vm is ")?;
        if self.running.load(Ordering::Acquire) {
            write!(f, "running")?;
        } else {
            write!(f, "stopped")?;
        }
        write!(f, " at ctx {:p}", self.scope.ctx)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_nop() {
        let mut vm = Vm::new(0, 0, 0, Arc::new(AtomicBool::new(true)), None).unwrap();
        let exec = ExecBlob {
            version: 0,
            id: String::from(""),
            func: Function::new(String::from("test"), 0, 0, vec![0x00], vec![]),
        };
        vm.run(&exec).unwrap();
    }

    #[test]
    #[should_panic]
    fn test_invalid() {
        let mut vm = Vm::new(0, 0, 0, Arc::new(AtomicBool::new(true)), None).unwrap();
        let exec = ExecBlob {
            version: 0,
            id: String::from(""),
            func: Function::new(String::from("test"), 0, 0, vec![0xff], vec![]),
        };
        vm.run(&exec).unwrap();
    }

    #[test]
    fn test_ret42() {
        let mut vm = Vm::new(0, 0, 0, Arc::new(AtomicBool::new(true)), None).unwrap();
        let exec = ExecBlob {
            version: 0,
            id: String::from(""),
            func: Function::new(String::from("test"), 0, 0,
                                vec![0x60, 0x01, 0x00, 0x82, 0x01],
                                vec![Type::Integer(42)]),
        };
        vm.run(&exec).unwrap();
        let ret = vm.get_retval().unwrap();
        println!("Program returned {:?}", ret);
        assert_eq!(ret, Some(Type::Integer(42)));
    }

    #[test]
    fn test_add42() {
        let mut vm = Vm::new(0, 0, 0, Arc::new(AtomicBool::new(true)), None).unwrap();
        let exec = ExecBlob {
            version: 0,
            id: String::from(""),
            func: Function::new(String::from("test"), 0, 0,
                                vec![0x60, 0x01, 0x00, 0x60, 0x02, 0x01,
                                     0x10, 0x01, 0x01, 0x02, 0x82, 0x01],
                                vec![Type::Integer(41), Type::Integer(1)]),
        };
        vm.run(&exec).unwrap();
        let ret = vm.get_retval().unwrap();
        println!("Program returned {:?}", ret);
        assert_eq!(ret, Some(Type::Integer(42)));
    }

    #[test]
    fn test_mul() {
        let mut vm = Vm::new(0, 0, 0, Arc::new(AtomicBool::new(true)), None).unwrap();
        let exec = ExecBlob {
            version: 0,
            id: String::from(""),
            func: Function::new(String::from("test"), 0, 0,
                                vec![0x60, 0x01, 0x00, 0x60, 0x02, 0x01,
                                     0x12, 0x01, 0x01, 0x02, 0x82, 0x01],
                                vec![Type::Integer(3), Type::Integer(6)]),
        };
        vm.run(&exec).unwrap();
        let ret = vm.get_retval().unwrap();
        println!("Program returned {:?}", ret);
        assert_eq!(ret, Some(Type::Integer(18)));
    }

    #[test]
    #[should_panic]
    fn test_badadd() {
        let mut vm = Vm::new(0, 0, 0, Arc::new(AtomicBool::new(true)), None).unwrap();
        let exec = ExecBlob {
            version: 0,
            id: String::from(""),
            func: Function::new(String::from("test"), 0, 0,
                                vec![0x60, 0x01, 0x00, 0x60, 0x02, 0x01,
                                     0x10, 0x01, 0x01, 0x02, 0x82, 0x01],
                                vec![Type::Integer(1),
                                     Type::String(Rc::new(String::from("1")))]),
        };
        vm.run(&exec).unwrap();
    }

    #[test]
    fn test_fib() {
        let mut vm = Vm::new(0, 0, 0, Arc::new(AtomicBool::new(true)), None).unwrap();

        let fibstr = String::from("fib");
        let fib = Function::new(fibstr.clone(), 1, 0,
                                vec![0x64, 0x01, 0x00, 0x00,  // load argument 0 in register 1
                                     0x60, 0x02, 0x00,        // load constant 0 (integer 1) in register 2
                                     0x34, 0x03, 0x01, 0x02,  // check r1 >= r2 (arg0 >= 1?)
                                     0x86, 0x02, 0x03,        // jump ahead if it is
                                     0x82, 0x02,              // return 1 if it isn't
                                     0x60, 0x04, 0x01,        // load constant 1 ("fib") in register 4
                                     0x61, 0x03, 0x04,        // load symbol "fib" in register 3
                                     0x11, 0x04, 0x01, 0x02,  // subtract r4 = a1 - 1
                                     0x50, 0x04,              // push r4
                                     0x91, 0x05, 0x02,        // create vector of length 1 from stack in r5
                                     0x80, 0x03, 0x05,        // call fib(r5)
                                     0x63, 0xf0,              // load ret in rf0
                                     0x11, 0x04, 0x04, 0x02,  // r4 = r4 - 1
                                     0x50, 0x04,              // push r4
                                     0x91, 0x05, 0x02,        // create vector of length 1 from stack in r5
                                     0x80, 0x03, 0x05,        // call fib(r5)
                                     0x63, 0xf1,              // load ret in rf1
                                     0x10, 0xf0, 0xf0, 0xf1,  // add rf1 to rf0
                                     0x82, 0xf0,              // return rf0

                                ],
                                vec![Type::Integer(1), Type::String(Rc::new(fibstr.clone()))]);

        let exec = ExecBlob {
            version: 0,
            id: String::from(""),
            func: Function::new(String::from(""), 0, 0,
                                vec![0x60, 0x01, 0x00,  // load const 0 ("fib") in r1
                                     0x60, 0x02, 0x01,  // load const 1 (fib function) in r2
                                     0x62, 0x02, 0x01,  // store "fib" -> fib
                                     0x60, 0x03, 0x02,  // load const 2 (10) in r3
                                     0x50, 0x03,        // push r3
                                     0x60, 0x04, 0x03,  // load const 3 (1) in r4
                                     0x91, 0x03, 0x04,  // create vector of length 1 from stack in r3
                                     0x80, 0x02, 0x03,  // call fib(r3) (aka fib(10))
                                     0x63, 0xf0,        // load ret in rf0
                                     0x82, 0xf0,        // return rf0
                                ],
                                vec![Type::String(Rc::new(fibstr.clone())),
                                     Type::Function(fib),
                                     Type::Integer(10),
                                     Type::Integer(1),
                                ]),
        };
        vm.run(&exec).unwrap();
        let ret = vm.get_retval().unwrap();
        println!("Program returned {:?}", ret);
        assert_eq!(ret, Some(Type::Integer(144)));
    }

    #[test]
    fn test_fact() {
        let mut vm = Vm::new(0, 0, 0, Arc::new(AtomicBool::new(true)), None).unwrap();
        let factstr = String::from("fact");
        let fact = Function::new(factstr.clone(), 2, 0,
                                 vec![0x64, 0x01, 0x00, 0x00, // load arg[0]
                                      0x64, 0x02, 0x01, 0x00, // load arg[1]
                                      0x90, 0x03, 0x01, // new int (value 0) in r3
                                      0x32, 0x04, 0x02, 0x03,  // r2 > r3
                                      0x86, 0x02, 0x04, // jump if r4
                                      0x82, 0x01,       // return r1
                                      0x93, 0x05, 0x01, // literal int 1 in r5
                                      0x12, 0x01, 0x01, 0x02,  // r1 = r1 * r2
                                      0x11, 0x02, 0x02, 0x05,  // r2 = r2 - r5
                                      0x60, 0x07, 0x00, // load const[0]
                                      0x61, 0x07, 0x07, // load sym r7
                                      0x50, 0x02,       // push r2
                                      0x50, 0x01,       // push r1
                                      0x93, 0x08, 0x02, // literal int 2 in r8
                                      0x91, 0x09, 0x08, // setup args vector
                                      0x81, 0x07, 0x09, // tail call

                                 ],
                                 vec![Type::String(Rc::new(factstr.clone())),
                                 ]);
        let exec = ExecBlob {
            version: 0,
            id: String::from(""),
            func: Function::new(String::from("fact"), 0, 0,
                                vec![0x60, 0x01, 0x00,  // load const 0 ("fact")
                                     0x60, 0x02, 0x01,  // load const 1 (fact fn)
                                     0x62, 0x02, 0x01,  // store "fact" -> fact fn
                                     0x60, 0x03, 0x02,  // load const 2 in r3
                                     0x50, 0x03,        // push r3
                                     0x93, 0x03, 0x01,  // literal int 1 in r3
                                     0x50, 0x03,        // push r3
                                     0x93, 0x03, 0x02,  // literal int 2 in r3
                                     0x91, 0x03, 0x03,  // create arg vector
                                     0x80, 0x02, 0x03,  // call fact(r3)
                                     0x63, 0xf0,        // load ret in rf0
                                     0x82, 0xf0,        // return rf0
                                ],
                                vec![Type::String(Rc::new(factstr.clone())),
                                     Type::Function(fact),
                                     Type::Integer(10),
                                ]),
        };
        vm.run(&exec).unwrap();
        let ret = vm.get_retval().unwrap();
        println!("Program returned {:?}", ret);
        assert_eq!(ret, Some(Type::Integer(3628800)));
    }

    #[test]
    fn test_multirun() {
        let mut vm = Vm::new(0, 0, 0, Arc::new(AtomicBool::new(true)), None).unwrap();

        let first = ExecBlob {
            version: 0,
            id: String::from("first"),
            func: Function::new(String::from("first"), 0, 0,
                                vec![0x93, 0x01, 0x03,  // load literal 3
                                     0x93, 0x02, 0x04,  // load literal 4
                                     0x10, 0x03, 0x01, 0x02,  // r3 = r1 + r2
                                     0x60, 0x04, 0x00,  // local const[0]
                                     0x62, 0x03, 0x04,  // store r3
                                     0x82, 0x03,        // return r3
                                ],
                                vec![Type::String(Rc::new(String::from("foo")))]),
        };
        let second = ExecBlob {
            version: 0,
            id: String::from("second"),
            func: Function::new(String::from("second"), 0, 0,
                                vec![0x60, 0x01, 0x00,  // load const[0]
                                     0x61, 0x02, 0x01,  // load value
                                     0xf0, 0x02,
                                     0x93, 0x03, 0x01,  // r3 = 1
                                     0x10, 0x03, 0x02, 0x03,  // r3 = r2 + r3
                                     0x82, 0x03,        // return r3
                                ],
                                vec![Type::String(Rc::new(String::from("foo")))]),
        };

        vm.run(&first).unwrap();
        let ret1 = vm.get_retval().unwrap();
        println!("First ret: {:?}", ret1);
        assert_eq!(ret1, Some(Type::Integer(7)));

        vm.reset();
        vm.run(&second).unwrap();
        let ret2 = vm.get_retval().unwrap();
        println!("Second ret: {:?}", ret2);
        assert_eq!(ret2, Some(Type::Integer(8)));
    }
}
