use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;
use serde::{Serialize, Deserialize};

use crate::error::Error;
use crate::types::{Type, ClosedType, Execute, Executable};

#[derive(Debug, Serialize, Deserialize)]
pub struct Frame {
    pub parent: Option<Rc<RefCell<Frame>>>,
    pub locals: Vec<Type>,
    pub stack: Vec<Type>,
    pub func: Executable,
    pub args: Vec<Type>,
    pub pc: usize,
    pub retval: Option<Type>,
    pub regs: HashMap<u8, Type>,
}

#[derive(Debug, Serialize)]
pub struct CondensedFrame {
    // no parent pointer
    pub locals: Vec<Type>,
    pub stack: Vec<Type>,
    pub name: String,  // function name only
    pub args: Vec<Type>,
    pub pc: usize,
    pub retval: Option<Type>,
    pub regs: HashMap<u8, Type>,
}

pub type BackTrace = Vec<CondensedFrame>;

macro_rules! make_read_field {
    ($name:ident, $field:ident, $err:expr) => {
        pub fn $name(&self, index: u8, level: u8) -> Result<Type, Error> {
            if level == 0 {
                self.$field.get(index as usize).map(|t| t.clone()).ok_or($err(index))
            } else {
                self.parent.as_ref().map_or(Err($err(index)), |p| p.borrow().$name(index, level - 1))
            }
        }
    };
}

macro_rules! make_write_field {
    ($name:ident, $field:ident, $err:expr) => {
        pub fn $name(&mut self, index: u8, level: u8, value: &Type) -> Result<Option<ClosedType>, Error>
        {
            if level == 0 {
                if (index as usize) >= self.$field.len() {
                    Err($err(index))
                } else {
                    if let Some(Type::Closed(ct)) = self.$field.get(index as usize) {
                        Ok(Some(*ct))
                    } else {
                        self.$field[index as usize] = value.clone();
                        Ok(None)
                    }
                }
            } else {
                self.parent.as_ref().map_or(Err($err(index)), |p| p.borrow_mut().$name(index, level - 1, value))
            }
        }
    };
}

impl Frame {
    pub fn new(parent: Option<Rc<RefCell<Frame>>>, func: Executable, args: Vec<Type>) -> Result<Self, Error> {
        if args.len() != func.arity() as usize {
            Err(Error::ArityMismatch)
        } else {
            Ok(Frame {
                parent: parent,
                locals: vec![Type::Unbound(); func.localct().into()],
                stack: Vec::new(),
                func: func,
                args: args,
                pc: 0,
                retval: None,
                regs: HashMap::new(),
            })
        }
    }

    pub fn new_rcref(parent: Option<Rc<RefCell<Frame>>>, func: Executable, args: Vec<Type>) -> Result<Rc<RefCell<Self>>, Error> {
        Ok(Rc::new(RefCell::new(Self::new(parent, func, args)?)))
    }

    pub fn make_backtrace(start: Rc<RefCell<Self>>) -> BackTrace {
        let mut cur = start;
        let mut out = Vec::new();

        loop {
            out.push(CondensedFrame {
                locals: cur.borrow().locals.clone(),
                stack: cur.borrow().stack.clone(),
                name: cur.borrow().func.name(),
                args: cur.borrow().args.clone(),
                pc: cur.borrow().pc,
                retval: cur.borrow().retval.clone(),
                regs: cur.borrow().regs.clone(),
            });
            if cur.borrow().parent.is_some() {
                let tmp = cur.borrow().parent.as_ref().unwrap().clone();
                cur = tmp;
            } else {
                return out;
            }
        }
    }

    make_read_field!(read_local, locals, Error::InvalidLocal);
    make_read_field!(read_arg, args, Error::InvalidArg);
    make_write_field!(write_local, locals, Error::InvalidLocal);
    make_write_field!(write_arg, args, Error::InvalidArg);
}
