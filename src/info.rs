use std::collections::HashSet;
use serde::{Deserialize, Serialize};

use crate::mptraits::*;
use crate::supervisor::SupervisorId;
use crate::types::ClosedId;
use crate::user::UserManager;

#[derive(Debug, Deserialize, Serialize)]
pub enum Info {
    SupervisorIdAssignment(SupervisorId),

    UserManagerAssignment(SupervisorId, UserManager),

    ListVmsRequest,
    ListVmsResponse(SupervisorId),

    MarkClosedRequest,
    MarkClosedResponse(SupervisorId, HashSet<ClosedId>),
}

impl SendSerialize for Info {}
impl RecvDeserialize for Info {}
