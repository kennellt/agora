use std::cell::RefCell;
use std::collections::{HashMap, HashSet};
use std::hash::{Hash, Hasher};
use std::rc::Rc;
use serde::{Serialize, Deserialize};
use rmp_serde;

use crate::acl::Acl;
use crate::closure::Closure;
use crate::error::Error;
use crate::frame::Frame;
use crate::function::Function;
use crate::instructions::InstructionResult;
use crate::scope::ScopeManager;
use crate::supervisor::SupervisorId;
use crate::user::UserId;
use crate::vm::VmId;

pub type ClosedId = u64;
pub type ClosedType = (SupervisorId, VmId, ClosedId, UserId);

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename = "t")]
pub enum Type {
    #[serde(rename = "n")]
    Nil(),
    #[serde(rename = "ub")]
    Unbound(),
    #[serde(rename = "e")]
    Empty(),
    #[serde(rename = "i")]
    Integer(i64),
    #[serde(rename = "f")]
    Float(f64),
    #[serde(rename = "s")]
    String(Rc<String>),
    #[serde(rename = "b")]
    Byte(u8),
    #[serde(rename = "v")]
    Vector(Rc<RefCell<Vec<Type>>>),
    #[serde(rename = "l")]
    List(Rc<Cons>),
    #[serde(rename = "h")]
    HashMap(Rc<RefCell<HashMap<Type, Type>>>),
    #[serde(rename = "fn")]
    Function(Rc<Function>),
    #[serde(rename = "cl")]
    Closure(Rc<Closure>),
    #[serde(rename = "cd")]
    Closed(ClosedType),
    #[serde(rename = "fr")]
    Frame(Rc<RefCell<Frame>>),
    #[serde(rename = "ue")]
    UnreadExec(String),
    // IoHandle, // FILL IN
    // Acl, // FILL IN
    // Session, // FILL IN
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Cons {
    pub car: Type,
    pub cdr: Type,
}

pub type SerializedType = Vec<u8>;

impl Type {
    pub fn cons(car: Type, cdr: Type) -> Type {
        Type::List(Rc::new(Cons { car: car, cdr: cdr }))
    }

    pub fn from_typecode(tc: &u8) -> Result<Type, Error> {
        match *tc {
            0 => Ok(Type::Empty()),
            1 => Ok(Type::Integer(0)),
            2 => Ok(Type::Float(0.0)),
            3 => Ok(Type::String(Rc::new(String::from("")))),
            4 => Ok(Type::Byte(0)),
            5 => Ok(Type::Vector(Rc::new(RefCell::new(Vec::new())))),
            6 => Ok(Type::Empty()),
            7 => Ok(Type::HashMap(Rc::new(RefCell::new(HashMap::new())))),
            tc => Err(Error::InvalidTypeCode(tc)),
        }
    }

    pub fn to_typecode(&self) -> Result<u8, Error> {
        match self {
            Type::Nil()         => Ok((-1 as i8) as u8),
            Type::Unbound()     => Err(Error::UnboundValue),
            Type::Empty()       => Ok(0),
            Type::Integer(_)    => Ok(1),
            Type::Float(_)      => Ok(2),
            Type::String(_)     => Ok(3),
            Type::Byte(_)       => Ok(4),
            Type::Vector(_)     => Ok(5),
            Type::List(_)       => Ok(6),
            Type::HashMap(_)    => Ok(7),
            Type::Function(_)   => Ok(8),
            Type::Closure(_)    => Ok(9),
            Type::Closed(_)     => Err(Error::InvalidClosed),
            Type::Frame(_)      => Ok(10),
            Type::UnreadExec(_) => Ok(11),
        }
    }

    pub fn serialize(&self) -> SerializedType {
        rmp_serde::to_vec(&self).unwrap()
    }

    pub fn deserialize(value: &SerializedType) -> Self {
        rmp_serde::from_slice(value).unwrap()
    }

    fn closed_walker(&self, set: &mut HashSet<ClosedType>) {
        match self {
            Type::Vector(v) => v.borrow().iter().for_each(|e| Self::closed_walker(&e, set)),
            Type::HashMap(h) => h.borrow().values().for_each(|e| Self::closed_walker(&e, set)),
            Type::List(ls) => {
                Self::closed_walker(&ls.car, set);
                Self::closed_walker(&ls.cdr, set);
            },
            Type::Closure(cs) => cs.get_ctx().unwrap().iter().for_each(|ct| { set.insert(*ct); }),
            Type::Closed(ct) => { set.insert(*ct); },
            _ => ()
        }
    }

    pub fn find_all_closed(&self) -> HashSet<ClosedType> {
        let mut set = HashSet::new();
        self.closed_walker(&mut set);
        set
    }
}

impl PartialEq for Type {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Type::Empty(), Type::Empty()) => true,
            (Type::Integer(a), Type::Integer(b)) => a == b,
            (Type::Float(a), Type::Float(b)) => a == b,
            (Type::String(a), Type::String(b)) => a == b,
            (Type::Byte(a), Type::Byte(b)) => a == b,
            _ => false,
        }
    }
}

impl Eq for Type {}

impl Hash for Type {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match self {
            Type::Integer(i) => i.hash(state),
            Type::String(s) => s.hash(state),
            Type::Byte(b) => b.hash(state),
            _ => ()
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ExportType {
    pub orig: SerializedType,
    pub closed: HashMap<ClosedType, SerializedType>,
    pub acl: Option<Acl>,
}

impl ExportType {
    fn walker(scope: &mut ScopeManager, t: &Type, map: &mut HashMap<ClosedType, SerializedType>) {
        match t {
            Type::Vector(v) => v.borrow().iter().for_each(|e| Self::walker(scope, &e, map)),
            Type::HashMap(h) => h.borrow().values().for_each(|e| Self::walker(scope, &e, map)),
            Type::List(ls) => {
                Self::walker(scope, &ls.car, map);
                Self::walker(scope, &ls.cdr, map);
            },
            Type::Closure(cs) => cs.get_ctx().unwrap().iter()
                .for_each(|ct| match scope.remove_closed(*ct) {
                    Some(val) => {
                        map.insert(*ct, val.serialize());
                        Self::walker(scope, &val, map);
                    },
                    None => (),
                }),
            Type::Closed(ct) => match scope.remove_closed(*ct) {
                Some(val) => {
                    map.insert(*ct, val.serialize());
                    Self::walker(scope, &val, map);
                },
                None => (),
            },
            _ => (),
        }
    }

    pub fn new(scope: &mut ScopeManager, orig: &Type, perms: Option<Acl>) -> Self {
        let mut map = HashMap::new();
        Self::walker(scope, &orig, &mut map);
        Self {
            orig: orig.serialize(),
            closed: map,
            acl: perms,
        }
    }

    pub fn serialize(&self) -> SerializedType {
        rmp_serde::to_vec(&self).unwrap()
    }

    pub fn deserialize(value: &SerializedType) -> Self {
        rmp_serde::from_slice(value).unwrap()
    }
}

pub trait Execute {
    fn name(&self) -> String;
    fn arity(&self) -> u8;
    fn localct(&self) -> u8;
    fn get_const(&self, idx: u8) -> Result<Type, Error>;
    fn instr_at(&self, idx: usize) -> InstructionResult;
    fn max_instr_addr(&self) -> usize;
    fn get_ctx(&self) -> Option<&Vec<ClosedType>>;
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum Executable {
    Function(Rc<Function>),
    Closure(Rc<Closure>),
}

impl Execute for Executable {
    fn name(&self) -> String {
        match self {
            Executable::Function(f) => f.name(),
            Executable::Closure(c) => c.name(),
        }
    }

    fn arity(&self) -> u8 {
        match self {
            Executable::Function(f) => f.arity(),
            Executable::Closure(c) => c.arity(),
        }
    }

    fn localct(&self) -> u8 {
        match self {
            Executable::Function(f) => f.localct(),
            Executable::Closure(c) => c.localct(),
        }
    }

    fn get_const(&self, idx: u8) -> Result<Type, Error> {
        match self {
            Executable::Function(f) => f.get_const(idx),
            Executable::Closure(c) => c.get_const(idx),
        }
    }

    fn instr_at(&self, idx: usize) -> InstructionResult {
        match self {
            Executable::Function(f) => f.instr_at(idx),
            Executable::Closure(c) => c.instr_at(idx),
        }
    }

    fn max_instr_addr(&self) -> usize {
        match self {
            Executable::Function(f) => f.max_instr_addr(),
            Executable::Closure(c) => c.max_instr_addr(),
        }
    }

    fn get_ctx(&self) -> Option<&Vec<ClosedType>> {
        match self {
            Executable::Function(f) => f.get_ctx(),
            Executable::Closure(c) => c.get_ctx(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rmp_serde;

    #[test]
    fn test_serialize() {
        let t = Type::Vector(Rc::new(RefCell::new(
            vec![Type::Empty(),
                 Type::Integer(1),
                 Type::Integer(2),
                 Type::Integer(3)])));
        println!("{:?}", rmp_serde::to_vec(&t).unwrap());

        let f = Type::Function(Function::new(String::from("foo"), 1, 0, vec![], vec![]));
        println!("{:?}", rmp_serde::to_vec(&f).unwrap());
    }
}
