use std::error;
use std::fmt;

use crate::types::Type;

#[derive(Debug)]
pub enum Error {
    OutOfBytes(usize),
    InvalidInstruction(u8, usize),
    ReadBeforeWrite(u8),
    BadOneOperand(&'static str, Type),
    BadTwoOperand(&'static str, Type, Type),
    CantReturn,
    NotYetReturned,
    InvalidConst(u8),
    InvalidTypeCode(u8),
    InvalidSymbolType,
    InvalidSymbol(String),
    InvalidListOperand(&'static str, Type),
    InvalidGetType(Type),
    InvalidVectorIndex,
    InvalidHashIndex,
    IndexOutOfBounds(Type),
    CantCall(Type),
    BadArgs(Type),
    ArityMismatch,
    InvalidArg(u8),
    InvalidJumpOffset(Type),
    InvalidStackOffset(Type),
    StackUnderflow,
    CantClose(Type),
    ArithOverflow,
    NotAVector(Type),
    ZeroError,
    NoChannel,
    InvalidClosed,
    NotAClosure,
    InvalidLocal(u8),
    InvalidExportPerm,
    CallFailed,
    UnboundValue,
    InvalidSetType(Type),
}

impl Error {
    pub fn into_usize(&self) -> usize {
        match *self {
            Self::OutOfBytes(_) => 0,
            Self::InvalidInstruction(_, _) => 1,
            Self::ReadBeforeWrite(_) => 2,
            Self::BadOneOperand(_, _) => 3,
            Self::BadTwoOperand(_, _, _) => 4,
            Self::CantReturn => 5,
            Self::NotYetReturned => 6,
            Self::InvalidConst(_) => 7,
            Self::InvalidTypeCode(_) => 8,
            Self::InvalidSymbolType => 9,
            Self::InvalidSymbol(_) => 10,
            Self::InvalidListOperand(_, _) => 11,
            Self::InvalidGetType(_) => 12,
            Self::InvalidVectorIndex => 13,
            Self::InvalidHashIndex => 14,
            Self::IndexOutOfBounds(_) => 15,
            Self::CantCall(_) => 16,
            Self::BadArgs(_) => 17,
            Self::ArityMismatch => 18,
            Self::InvalidArg(_) => 19,
            Self::InvalidJumpOffset(_) => 20,
            Self::InvalidStackOffset(_) => 21,
            Self::StackUnderflow => 22,
            Self::CantClose(_) => 23,
            Self::ArithOverflow => 24,
            Self::NotAVector(_) => 25,
            Self::ZeroError => 26,
            Self::NoChannel => 27,
            Self::InvalidClosed => 28,
            Self::NotAClosure => 29,
            Self::InvalidLocal(_) => 30,
            Self::InvalidExportPerm => 31,
            Self::CallFailed => 32,
            Self::UnboundValue => 33,
            Self::InvalidSetType(_) => 34,
        }
    }
}

impl error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::OutOfBytes(idx) => write!(f, "instruction decoder ran out of bytes at index {}", idx),
            Error::InvalidInstruction(op, idx) => write!(f, "invalid instruction decoded: {} at {}", op, idx),
            Error::ReadBeforeWrite(r) => write!(f, "read before write of register {}", r),
            Error::BadOneOperand(op, t1) => write!(f, "cannot do {} on {:?}", op, t1),
            Error::BadTwoOperand(op, t1, t2) => write!(f, "cannot do {} on {:?} and {:?}", op, t1, t2),
            Error::CantReturn => write!(f, "no parent frame to return to"),
            Error::NotYetReturned => write!(f, "no return value yet"),
            Error::InvalidConst(idx) => write!(f, "no constant at index {}", idx),
            Error::InvalidTypeCode(tc) => write!(f, "cannot create type {}, only basic and container types supported", tc),
            Error::InvalidSymbolType => write!(f, "can only load a string symbol name"),
            Error::InvalidSymbol(s) => write!(f, "symbol '{}' not found", s),
            Error::InvalidListOperand(op, t) => write!(f, "cannot {} on {:?}", op, t),
            Error::InvalidGetType(t) => write!(f, "cannot get on {:?}", t),
            Error::InvalidVectorIndex => write!(f, "can only index a vector with an int"),
            Error::InvalidHashIndex => write!(f, "hashmap can only be indexed by int, string, or byte"),
            Error::IndexOutOfBounds(t) => write!(f, "index out of bounds {:?}", t),
            Error::CantCall(t) => write!(f, "type {:?} cannot be called", t),
            Error::BadArgs(t) => write!(f, "bad argument type: {:?}", t),
            Error::ArityMismatch => write!(f, "arity mismatch"),
            Error::InvalidArg(idx) => write!(f, "no argument with index {}", idx),
            Error::InvalidJumpOffset(t) => write!(f, "cannot jump to offset of type {:?}", t),
            Error::InvalidStackOffset(t) => write!(f, "cannot use stack offset of {:?}", t),
            Error::StackUnderflow => write!(f, "stack underflow"),
            Error::CantClose(t) => write!(f, "cannot close type {:?}", t),
            Error::ArithOverflow => write!(f, "arithmetic overflow"),
            Error::NotAVector(t) => write!(f, "cannot do vector operation on {:?}", t),
            Error::ZeroError => write!(f, "cannot use zero value on mathematical operation RHS"),
            Error::NoChannel => write!(f, "no channel available for message-passing"),
            Error::InvalidClosed => write!(f, "invalid closed value"),
            Error::NotAClosure => write!(f, "not a closure"),
            Error::InvalidLocal(idx) => write!(f, "invalid local index {}", idx),
            Error::InvalidExportPerm => write!(f, "invalid export permission bit"),
            Error::CallFailed => write!(f, "call failed"),
            Error::UnboundValue => write!(f, "unbound value"),
            Error::InvalidSetType(t) => write!(f, "cannot set on {:?}", t),
        }
    }
}
