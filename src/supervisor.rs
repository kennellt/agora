use std::collections::HashMap;
use std::default::Default;
use std::error;
use std::fmt;
use std::io::prelude::*;
use std::net::{Shutdown, SocketAddr};
use std::sync::Arc;
use std::sync::atomic::AtomicBool;
use std::sync::mpsc::{channel, Receiver, Sender};
use std::thread::Builder;
use debug_print::debug_println;
use mio::{Events, Interest, Poll, Token, Waker};
use mio::net::{TcpListener, TcpStream};
use serde::{Deserialize, Serialize};
use rmp_serde::{decode, Deserializer, Serializer};

use crate::execblob::ExecBlob;
use crate::info::Info;
use crate::message::Message;
use crate::mptraits::*;
use crate::resultblob::ResultBlob;
use crate::user::UserManager;
use crate::vm::{Vm, VmId, VmStatus};

pub type SupervisorId = u64;

pub struct Supervisor {
    id: SupervisorId,
    user_listener: TcpListener,
    mesg_conn: TcpStream,
    info_conn: TcpStream,
    vid_ctr: VmId,
    users: Arc<UserManager>,
}

#[derive(Debug)]
struct NoSupervisorId {}
impl error::Error for NoSupervisorId {}
impl fmt::Display for NoSupervisorId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "did not receive supervisor ID from metavisor")
    }
}

#[derive(Debug, Deserialize)]
struct LocalAuth {
    name: String,
    passwd: String,
}

impl RecvDeserialize for LocalAuth {}

const LOCAL_AUTH_OK: u8 = 1;
const LOCAL_AUTH_ERR: u8 = 0;

const EVENT_CAPACITY: usize = 1024;

impl Supervisor {
    pub fn new(user: SocketAddr, mesg: SocketAddr, info: SocketAddr) -> Result<Self, Box<dyn error::Error>> {
        Ok(Supervisor {
            id: 0,
            user_listener: TcpListener::bind(user)?,
            mesg_conn: TcpStream::connect(mesg)?,
            info_conn: TcpStream::connect(info)?,
            vid_ctr: 0,
            users: Arc::new(Default::default()),
        })

    }

    fn handle_vm(mut stream: TcpStream, sid: SupervisorId, vid: VmId,
                 tx: Sender<Message>, rx: Receiver<Message>, waker: Arc<Waker>,
                 users: Arc<UserManager>) {
        let mut poll = Poll::new().unwrap();
        let mut events = Events::with_capacity(EVENT_CAPACITY);
        const INPUT: Token = Token(0);
        poll.registry().register(&mut stream, INPUT, Interest::READABLE).unwrap();

        poll.poll(&mut events, None).unwrap();
        let uid = match LocalAuth::recv(&mut stream) {
            Ok(la) => {
                if users.check_name_and_passwd(&la.name, &la.passwd) {
                    stream.write(&[LOCAL_AUTH_OK]).unwrap();
                    *users.get_id_by_name(&la.name).unwrap()
                } else {
                    debug_println!("received invalid username or password for VM #{}", vid);
                    stream.write(&[LOCAL_AUTH_ERR]).unwrap();
                    stream.shutdown(Shutdown::Both).unwrap();
                    return;
                }
            },
            Err(_e) => {
                debug_println!("received bad auth for VM #{}: {_e}", vid);
                stream.write(&[LOCAL_AUTH_ERR]).unwrap();
                stream.shutdown(Shutdown::Both).unwrap();
                return;
            },
        };
        debug_println!("authenticated user {uid}");
        events.clear();

        let running = Arc::new(AtomicBool::new(true));  // put this here for now, not currently allowing pauses
        let mut vm = Vm::new(sid, vid, uid, running.clone(), Some((tx, rx, waker.clone()))).unwrap();
        loop {
            poll.poll(&mut events, None).unwrap();  // wait for stream to be writable
            events.clear();  // clear out INPUT event(s)

            let mut de = Deserializer::new(std::io::Read::by_ref(&mut stream));
            let exe: ExecBlob = match Deserialize::deserialize(&mut de) {
                Ok(e) => e,
                Err(decode::Error::InvalidMarkerRead(e)) => {
                    if e.kind() == std::io::ErrorKind::UnexpectedEof {
                        debug_println!("Detected EOF on vm:{vid}, shutting down");
                        stream.shutdown(Shutdown::Both).unwrap();
                        return;
                    } else {
                        panic!("Unhandlable error {:?}", e)
                    }
                },
                e => panic!("Unhandlable error {:?}", e),
            };

            vm.reset();
            let res = match vm.run_sanitize_closed(&exe) {
                Ok(VmStatus::Finished(ot)) => {
                    debug_println!("VM #{vid} produced value {ot:?}");
                    ResultBlob {
                        version: 0,
                        id: exe.id,
                        result: ot,
                        error: None,
                        context: None,
                    }
                },
                Ok(_) => panic!("cannot handle paused VMs yet"),
                Err(e) => {
                    debug_println!("VM #{vid} produced error {e:?}");
                    ResultBlob {
                        version: 0,
                        id: exe.id,
                        result: None,
                        error: Some(e.into_usize()),
                        context: Some(vm.make_backtrace()),
                    }
                },
            };
            vm.reset();  // clear out any held locks on error

            let mut buf = Vec::new();
            let mut se = Serializer::new(&mut buf);
            res.serialize(&mut se).unwrap();
            stream.write(&buf).unwrap();
        }
    }

    pub fn run(&mut self) -> Result<(), Box<dyn error::Error>> {
        // polling and event-handler code loosely follows that found in the mio docs
        // at https://docs.rs/mio/latest/x86_64-unknown-linux-gnu/mio/guide/index.html
        let mut poll = Poll::new()?;
        let mut events = Events::with_capacity(EVENT_CAPACITY);

        const SOURCE_UPSTREAM: Token = Token(0);
        poll.registry().register(&mut self.info_conn, SOURCE_UPSTREAM, Interest::READABLE)?;
        poll.registry().register(&mut self.mesg_conn, SOURCE_UPSTREAM, Interest::READABLE)?;

        'get_id: loop {  // first we must get an ID from the metavisor
            poll.poll(&mut events, None)?;
            for ev in events.iter() {
                match ev.token() {
                    SOURCE_UPSTREAM => {
                        self.id = match Info::recv(&mut self.info_conn)? {
                            Info::SupervisorIdAssignment(id) => id,
                            _ => return Err(Box::new(NoSupervisorId {})),
                        };
                        debug_println!("received id {}", self.id);
                        break 'get_id;
                    },
                    tok => panic!("received unknown toke {:?}", tok),
                }
            }
        }

        poll.poll(&mut events, None)?;
        self.users = match Info::recv(&mut self.info_conn)? {
            Info::UserManagerAssignment(sid, um) => {
                if sid != self.id {
                    panic!("protocol error: received message for wrong supervisor: {:?}", sid);
                }
                Arc::new(um.clone())
            },
            _ => panic!("protocol error: did not receive user manager"),
        };
        debug_println!("received userlist");
        events.clear();

        let (to_sup, from_vms) = channel();
        let mut tx_vms: HashMap<VmId, Sender<Message>> = HashMap::new();

        const SOURCE_CLIENT: Token = Token(1);
        poll.registry().register(&mut self.user_listener, SOURCE_CLIENT, Interest::READABLE)?;

        const SOURCE_VM: Token = Token(2);
        let from_vm_waker = Arc::new(Waker::new(poll.registry(), SOURCE_VM)?);

        let mut conn_idx: usize = 3;
        let mut conns: HashMap<Token, TcpStream> = HashMap::new();

        loop {
            poll.poll(&mut events, None)?;
            for ev in events.iter() {
                match ev.token() {
                    SOURCE_UPSTREAM => {
                        let mut buf = [0; 1];
                        while match self.mesg_conn.try_io(|| { self.mesg_conn.peek(&mut buf) }) {
                            Ok(sz) => sz,
                            _ => 0,
                        } > 0 {
                            let mesg = match Message::recv(&mut self.mesg_conn) {
                                Ok(m) => {
                                    debug_println!("received message from metavisor: {:?}", m);
                                    m
                                },
                                Err(e) => panic!("received bad message from metavisor: {:?}", e),
                            };
                            let vid = match mesg {
                                Message::ReadSymbolOk(_, vid, _, _) => vid,
                                Message::ReadSymbolErr(_, vid, _) => vid,
                                Message::WriteSymbolOk(_, vid, _) => vid,
                                Message::WriteSymbolErr(_, vid, _) => vid,
                                Message::ReadClosedOk(_, vid, _, _) => vid,
                                Message::ReadClosedErr(_, vid, _) => vid,
                                Message::WriteClosedOk(_, vid, _) => vid,
                                Message::WriteClosedErr(_, vid, _) => vid,
                                Message::CloseOk(_, vid, _, _) => vid,
                                Message::CloseErr(_, vid, _) => vid,
                                Message::ExecSymbolOk(_, vid, _, _) => vid,
                                Message::ExecSymbolErr(_, vid, _, _) => vid,
                                Message::LockSymbolStatus(_, vid, _, _) => vid,
                                Message::LockSymbolErr(_, vid, _) => vid,
                                Message::UnlockSymbolOk(_, vid, _) => vid,
                                Message::UnlockSymbolErr(_, vid, _) => vid,
                                Message::LockClosedStatus(_, vid, _, _) => vid,
                                Message::LockClosedErr(_, vid, _) => vid,
                                Message::UnlockClosedOk(_, vid, _) => vid,
                                Message::UnlockClosedErr(_, vid, _) => vid,
                                e => panic!("received invalid message from metavisor: {:?}", e),
                            };
                            match tx_vms.get(&vid) {
                                Some(tx) => tx.send(mesg)?,
                                None => { debug_println!("metavisor sent message for missing VM #{}", vid); },
                            };
                        }
                    },
                    SOURCE_CLIENT => {
                        let (mut stream, _addr) = self.user_listener.accept()?;
                        debug_println!("accepted connection from {_addr} (conn #{})", conn_idx - 3);

                        let tok = Token(conn_idx);
                        poll.registry().register(&mut stream, tok, Interest::WRITABLE)?;
                        conns.insert(tok, stream);
                        conn_idx += 1;
                    },
                    SOURCE_VM => {
                        loop {
                            let mesg: Message = match from_vms.try_recv() {
                                Ok(m) => m,
                                Err(_) => break,
                            };
                            mesg.send(&mut self.mesg_conn).unwrap();
                        }
                    },
                    tok => {
                        if conns.get(&tok).unwrap().peer_addr().is_err() {
                            continue;
                        }
                        let mut stream = conns.remove(&tok).unwrap();
                        poll.registry().deregister(&mut stream)?;

                        let sid = self.id;
                        let vid = self.vid_ctr;
                        self.vid_ctr += 1;
                        debug_println!("assigned id {vid} to connection #{}", usize::from(tok) - 3);

                        let (to_vm, from_sup) = channel();
                        let tx = to_sup.clone();
                        let waker = from_vm_waker.clone();
                        let um = self.users.clone();
                        tx_vms.insert(vid, to_vm);
                        Builder::new().name(format!("vm:{vid}")).spawn(move || {
                            Supervisor::handle_vm(stream, sid, vid, tx, from_sup, waker, um);
                        })?;
                    },
                }
            }
        }
    }
}
