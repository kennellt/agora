use serde::{Deserialize, Serialize};

use crate::mptraits::*;
use crate::supervisor::SupervisorId;
use crate::types::{SerializedType, ClosedType};
use crate::user::UserId;
use crate::vm::VmId;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum Message {
    ReadSymbolRequest(SupervisorId, VmId, UserId, String),
    ReadSymbolOk(SupervisorId, VmId, UserId, SerializedType),
    ReadSymbolErr(SupervisorId, VmId, UserId),

    WriteSymbolRequest(SupervisorId, VmId, UserId, String, SerializedType),
    WriteSymbolOk(SupervisorId, VmId, UserId),
    WriteSymbolErr(SupervisorId, VmId, UserId),

    ExecSymbolRequest(SupervisorId, VmId, UserId, String, Vec<SerializedType>),
    ExecSymbolOk(SupervisorId, VmId, UserId, Option<SerializedType>),
    ExecSymbolErr(SupervisorId, VmId, UserId, usize),

    // reserved for use in metavisor only!
    ExecDone(SupervisorId, VmId, VmId, UserId, Result<Option<SerializedType>, usize>),

    ReadClosedRequest(SupervisorId, VmId, UserId, ClosedType),
    ReadClosedOk(SupervisorId, VmId, UserId, SerializedType),
    ReadClosedErr(SupervisorId, VmId, UserId),

    WriteClosedRequest(SupervisorId, VmId, UserId, ClosedType, SerializedType),
    WriteClosedOk(SupervisorId, VmId, UserId),
    WriteClosedErr(SupervisorId, VmId, UserId),

    CloseRequest(SupervisorId, VmId, UserId, String),
    CloseOk(SupervisorId, VmId, UserId, ClosedType),
    CloseErr(SupervisorId, VmId, UserId),

    LockSymbolRequest(SupervisorId, VmId, UserId, String),
    LockSymbolStatus(SupervisorId, VmId, UserId, bool),
    LockSymbolErr(SupervisorId, VmId, UserId),

    UnlockSymbolRequest(SupervisorId, VmId, UserId, String),
    UnlockSymbolOk(SupervisorId, VmId, UserId),
    UnlockSymbolErr(SupervisorId, VmId, UserId),

    LockClosedRequest(SupervisorId, VmId, UserId, ClosedType),
    LockClosedStatus(SupervisorId, VmId, UserId, bool),
    LockClosedErr(SupervisorId, VmId, UserId),

    UnlockClosedRequest(SupervisorId, VmId, UserId, ClosedType),
    UnlockClosedOk(SupervisorId, VmId, UserId),
    UnlockClosedErr(SupervisorId, VmId, UserId),
}

impl SendSerialize for Message {}
impl RecvDeserialize for Message {}
