use std::cell::RefCell;
use std::collections::{HashMap, VecDeque};
use std::rc::Rc;
use std::sync::Arc;
use std::sync::mpsc::{Receiver, Sender};
use debug_print::debug_println;
use mio::Waker;

use crate::acl::Acl;
use crate::error::Error;
use crate::frame::Frame;
use crate::message::Message;
use crate::supervisor::SupervisorId;
use crate::types::{Type, ClosedId, ClosedType, ExportType, Execute, Executable};
use crate::user::UserId;
use crate::vm::VmId;

#[derive(Debug)]
pub struct ScopeManager {
    sid: SupervisorId,
    vid: VmId,
    uid: UserId,
    pub root: Rc<RefCell<Frame>>,
    pub ctx: Rc<RefCell<Frame>>,
    pub channel: Option<(Sender<Message>, Receiver<Message>, Arc<Waker>)>,
    pub globals: HashMap<String, Type>,
    close_ctr: ClosedId,
    pub closed: HashMap<ClosedId, Type>,
    sym_locks: HashMap<String, u64>,
    closed_locks: HashMap<ClosedType, u64>,
    lock_ctr: u64,
}

macro_rules! make_read_passthrough {
    ($name:ident) => {
        pub fn $name(&self, index: u8, level: u8) -> Result<Type, Error> {
            self.ctx.borrow().$name(index, level)
        }
    };
}

macro_rules! make_write_passthrough {
    ($name:ident) => {
        pub fn $name(&mut self, index: u8, level: u8, value: Type) -> Result<(), Error> {
            let ret = self.ctx.borrow_mut().$name(index, level, &value)?;
            ret.map_or(Ok(()), |ct| self.write_closed(ct, &value))
        }
    };
}

macro_rules! make_close_field {
    ($name:ident, $read_fn:ident, $write_fn:ident) => {
        pub fn $name(&mut self, index: u8, level: u8) -> Result<ClosedType, Error> {
            let orig = match self.$read_fn(index, level)? {
                Type::Closed(ct) => return Ok(ct),
                t => t,
            };

            let cid = self.close_ctr;
            self.close_ctr += 1;

            let ct = (self.sid, self.vid, cid, self.uid);
            self.closed.insert(cid, orig.clone());
            self.$write_fn(index, level, Type::Closed(ct))?;

            Ok(ct)
        }
    };
}

impl ScopeManager {
    pub fn new(sid: SupervisorId, vid: VmId, uid: UserId, root: Rc<RefCell<Frame>>,
               channel: Option<(Sender<Message>, Receiver<Message>, Arc<Waker>)>) -> Self {
        ScopeManager {
            sid: sid,
            vid: vid,
            uid: uid,
            root: root.clone(),
            ctx: root.clone(),
            channel: channel,
            globals: HashMap::new(),
            close_ctr: 0,
            closed: HashMap::new(),
            sym_locks: HashMap::new(),
            closed_locks: HashMap::new(),
            lock_ctr: 0,
        }
    }

    pub fn reset(&mut self) {
        self.ctx = self.root.clone();
        self.root.borrow_mut().stack.clear();
        self.root.borrow_mut().args.clear();
        self.root.borrow_mut().pc = 0;
        self.root.borrow_mut().retval = None;
        self.root.borrow_mut().regs.drain();

        let mut slv = Vec::with_capacity(self.sym_locks.len());
        for (k, v) in self.sym_locks.iter() {
            slv.push((k, v));
        }
        slv.as_mut_slice().sort();
        let mut sl = VecDeque::from(slv);

        let mut clv = Vec::with_capacity(self.closed_locks.len());
        for (k, v) in self.closed_locks.iter() {
            clv.push((k, v));
        }
        clv.as_mut_slice().sort();
        let mut cl = VecDeque::from(clv);

        while sl.len() > 0 && cl.len() > 0 {
            match (sl.back(), cl.back()) {
                (Some(s), Some(c)) => {
                    if s.1 > c.1 {
                        debug_println!("unlocking {:?}", s);
                        self.send(Message::UnlockSymbolRequest(self.sid,
                                                               self.vid,
                                                               self.uid,
                                                               s.0.clone()));
                        sl.pop_back();
                    } else {
                        debug_println!("unlocking {:?}", c);
                        self.send(Message::UnlockClosedRequest(self.sid,
                                                               self.vid,
                                                               self.uid,
                                                               *c.0));
                        cl.pop_back();
                    }
                    self.recv();  // don't care about response
                },
                (Some(s), None) => {
                    debug_println!("unlocking {:?}", s);
                    self.send(Message::UnlockSymbolRequest(self.sid,
                                                           self.vid,
                                                           self.uid,
                                                           s.0.clone()));
                    sl.pop_back();
                    self.recv();  // don't care about response
                },
                (None, Some(c)) => {
                    debug_println!("unlocking {:?}", c);
                    self.send(Message::UnlockClosedRequest(self.sid,
                                                           self.vid,
                                                           self.uid,
                                                           *c.0));
                    cl.pop_back();
                    self.recv();  // don't care about response
                },
                (None, None) => break,  // shouldn't happen
            }
        }
    }

    pub fn is_ctx_root(&self) -> bool {
        std::ptr::eq(self.ctx.as_ptr(), self.root.as_ptr())
    }

    pub fn push(&mut self, func: Executable, args: Vec<Type>) -> Result<(), Error> {
        let fr = Frame::new_rcref(Some(self.ctx.clone()), func, args)?;
        self.ctx = fr;
        Ok(())
    }

    pub fn pop(&mut self) -> Result<(), Error> {
        if self.ctx.borrow().parent.is_none() {
            Err(Error::CantReturn)
        } else {
            let parent = self.ctx.borrow().parent.as_ref().unwrap().clone();
            self.ctx = parent;
            Ok(())
        }
    }

    fn send(&self, mesg: Message) {
        let ch = self.channel.as_ref().unwrap();
        ch.0.send(mesg).unwrap();
        ch.2.wake().unwrap();
    }

    fn recv(&self) -> Message {
        self.channel.as_ref().unwrap().1.recv().unwrap()
    }

    pub fn lookup(&self, key: &String) -> Result<Type, Error> {
        match self.globals.get(key) {
            Some(t) => return Ok(t.clone()),
            None => (),
        }

        if self.channel.is_none() {
            return Err(Error::InvalidSymbol(key.clone()));
        }

        let msg = Message::ReadSymbolRequest(self.sid, self.vid, self.uid, key.clone());
        self.send(msg);
        match self.recv() {
            Message::ReadSymbolOk(_, _, _, r) => Ok(Type::deserialize(&r)),
            Message::ReadSymbolErr(_, _, _) => Err(Error::InvalidSymbol(key.clone())),
            e => panic!("protocol error: expected read result or error, got {:?}", e),
        }
    }

    pub fn update(&mut self, key: &String, value: Type) -> Result<(), Error> {
        match self.globals.get(key) {
            Some(Type::Closed(ct)) => {
                let copy = *ct;
                return self.write_closed(copy, &value);
            },
            Some(_t) => {
                self.globals.insert(key.clone(), value.clone());
                return Ok(());
            },
            None => (),
        }

        if self.channel.is_some() {
            let read = Message::ReadSymbolRequest(self.sid, self.vid, self.uid, key.clone());
            self.send(read);
            match self.recv() {
                Message::ReadSymbolOk(_, _, _, _) => {
                    let write = Message::WriteSymbolRequest(self.sid, self.vid, self.uid, key.clone(),
                                                            ExportType::new(self, &value, None).serialize());
                    self.send(write);
                    match self.recv() {
                        Message::WriteSymbolOk(_, _, _) => return Ok(()),
                        Message::WriteSymbolErr(_, _, _) => return Err(Error::InvalidSymbol(key.clone())),  // TODO: more specific error
                        e => panic!("protocol error: expected write result or error, got {:?}", e),
                    }
                },
                Message::ReadSymbolErr(_, _, _) => (),  // fall through to ctx insertion below
                e => panic!("protocol error: expected read result or error, got {:?}", e),
            }
        }

        self.globals.insert(key.clone(), value.clone());

        Ok(())
    }

    make_read_passthrough!(read_arg);
    make_read_passthrough!(read_local);
    make_write_passthrough!(write_arg);
    make_write_passthrough!(write_local);

    make_close_field!(close_arg, read_arg, write_arg);
    make_close_field!(close_local, read_local, write_local);

    pub fn close_sym(&mut self, sym: &String) -> Result<ClosedType, Error> {
        match self.globals.get(sym) {
            Some(Type::Closed(ct)) => Ok(*ct),
            Some(t) => {
                let cid = self.close_ctr;
                self.close_ctr += 1;
                self.closed.insert(cid, t.clone());

                let ct = (self.sid, self.vid, cid, self.uid);
                self.globals.insert(sym.clone(), Type::Closed(ct));
                Ok(ct)
            },
            None => {
                self.send(Message::CloseRequest(self.sid, self.vid, self.uid, sym.clone()));
                match self.recv() {
                    Message::CloseOk(_, _, _, ct) => Ok(ct),
                    Message::CloseErr(_, _, _) => {
                        let cid = self.close_ctr;
                        self.close_ctr += 1;
                        self.closed.insert(cid, Type::Unbound());

                        let ct = (self.sid, self.vid, cid, self.uid);
                        self.globals.insert(sym.clone(), Type::Closed(ct));
                        Ok(ct)
                    },
                    e => panic!("protocol error: expected closed result or error, got {:?}", e),
                }
            },
        }
    }

    pub fn dup_closed(&self, index: u8) -> Result<Type, Error> {
        let cs = match &self.ctx.borrow().func {
            Executable::Closure(c) => c.clone(),
            Executable::Function(_) => return Err(Error::NotAClosure),
        };
        let ctx = cs.get_ctx().unwrap();

        if (index as usize) >= ctx.len() {
            return Err(Error::InvalidClosed);
        }

        Ok(Type::Closed(*ctx.get(index as usize).unwrap()))
    }

    pub fn load_closed(&self, index: u8) -> Result<Type, Error> {
        let cs = match &self.ctx.borrow().func {
            Executable::Closure(c) => c.clone(),
            Executable::Function(_) => return Err(Error::NotAClosure),
        };
        let ctx = cs.get_ctx().unwrap();

        if (index as usize) >= ctx.len() {
            return Err(Error::InvalidClosed);
        }

        let cd = ctx.get(index as usize).unwrap();
        if (cd.0 != self.sid) || (cd.1 != self.vid) {
            self.send(Message::ReadClosedRequest(self.sid, self.vid, self.uid, *cd));
            return match self.recv() {
                Message::ReadClosedOk(_, _, _, r) => Ok(Type::deserialize(&r)),
                Message::ReadClosedErr(_, _, _) => Err(Error::InvalidClosed),
                e => panic!("protocol error: expected read result or error, got {:?}", e),
            };
        }
        match self.closed.get(&cd.2) {
            Some(t) => Ok(t.clone()),
            None => {
                self.send(Message::ReadClosedRequest(self.sid, self.vid, self.uid, *cd));
                match self.recv() {
                    Message::ReadClosedOk(_, _, _, r) => Ok(Type::deserialize(&r)),
                    Message::ReadClosedErr(_, _, _) => Err(Error::InvalidClosed),
                    e => panic!("protocol error: expected read result or error, got {:?}", e),
                }
            },
        }
    }

    pub fn store_closed(&mut self, value: Type, index: u8) -> Result<(), Error> {
        let cs = match &self.ctx.borrow().func {
            Executable::Closure(c) => c.clone(),
            Executable::Function(_) => return Err(Error::NotAClosure),
        };
        let ctx = cs.get_ctx().unwrap();

        if (index as usize) >= ctx.len() {
            return Err(Error::InvalidClosed);
        }

        let cd = ctx.get(index as usize).unwrap();
        if (cd.0 != self.sid) || (cd.1 != self.vid) || self.closed.get(&cd.2).is_none() {
            self.send(Message::WriteClosedRequest(self.sid, self.vid, self.uid, *cd, value.serialize()));
            match self.recv() {
                Message::WriteClosedOk(_, _, _) => Ok(()),
                Message::WriteClosedErr(_, _, _) => Err(Error::InvalidClosed),
                e => panic!("protocol error: expected write result or error, got {:?}", e),
            }
        } else {
            self.closed.insert(cd.2, value);
            Ok(())
        }
    }

    pub fn unclose(&self, cd: ClosedType) -> Result<Type, Error> {
        if (cd.0 == self.sid) && (cd.1 == self.vid) {
            match self.closed.get(&cd.2) {
                Some(t) => return Ok(t.clone()),
                None => (),
            }
        }

        self.send(Message::ReadClosedRequest(self.sid, self.vid, self.uid, cd));
        match self.recv() {
            Message::ReadClosedOk(_, _, _, r) => Ok(Type::deserialize(&r)),
            Message::ReadClosedErr(_, _, _) => Err(Error::InvalidClosed),
            e => panic!("protocol error: expected read result or error, got {:?}", e),
        }
    }

    pub fn export(&mut self, key: &String, value: Type, perms: Option<Acl>) -> Result<(), Error> {
        if self.channel.is_none() {
            return Err(Error::NoChannel);
        }

        let mut val = value.clone();
        let mut remove = false;

        if let Some(Type::Closed(ct)) = self.globals.get(key) {
            if (ct.0 == self.sid) && (ct.1 == self.vid) {
                match self.closed.get(&ct.2) {
                    Some(Type::Unbound()) => {
                        // we're trying to export a recursive function that hasn't
                        // been closed over itself yet
                        self.closed.insert(ct.2, value.clone());
                        val = Type::Closed(*ct);
                        remove = true;
                    },
                    _ => (),
                }
            }
        }

        if remove {
            self.globals.remove(key);
        }

        let mesg = Message::WriteSymbolRequest(self.sid, self.vid, self.uid, key.clone(),
                                               ExportType::new(self, &val, perms).serialize());
        self.send(mesg);
        match self.recv() {
            Message::WriteSymbolOk(_, _, _) => (),
            Message::WriteSymbolErr(_, _, _) => (),
            e => panic!("protocol error: expected write result or error, got {:?}", e),
        }

        Ok(())
    }

    fn write_closed(&mut self, ct: ClosedType, value: &Type) -> Result<(), Error> {
        if (ct.0 != self.sid) || (ct.1 != self.vid) || self.closed.get(&ct.2).is_none() {
            self.send(Message::WriteClosedRequest(self.sid, self.vid, self.uid, ct, value.serialize()));
            match self.recv() {
                Message::WriteClosedOk(_, _, _) => return Ok(()),
                Message::WriteClosedErr(_, _, _) => return Err(Error::InvalidClosed),
                e => panic!("protocol error: expected write result or error, got {:?}", e),
            }
        } else {
            self.closed.insert(ct.2, value.clone());
            Ok(())
        }
    }

    pub fn remove_closed(&mut self, ct: ClosedType) -> Option<Type> {
        if (ct.0 != self.sid) || (ct.1 != self.vid) {
            None
        } else {
            self.closed.remove(&ct.2)
        }
    }

    pub fn export_closed(&mut self, ct: ClosedType) -> Result<(), Error> {
        if (ct.0 == self.sid) && (ct.1 == self.vid) {
            if let Some(cv) = self.closed.remove(&ct.2) {
                self.send(Message::WriteClosedRequest(self.sid, self.vid, self.uid,
                                                      ct, cv.serialize()));
                match self.recv() {
                    Message::WriteClosedOk(_, _, _) => Ok(()),
                    Message::WriteClosedErr(_, _, _) => Err(Error::InvalidClosed),
                    e => panic!("protocol error: expected write result or error, got {:?}", e),
                }
            } else {
                Ok(())
            }
        } else {
            Ok(())
        }
    }

    pub fn call_isolated_shared(&self, sym: &String, args: &Vec<Type>) -> Result<(), Error> {
        self.send(Message::ExecSymbolRequest(self.sid, self.vid, self.uid, sym.clone(),
                                             args.into_iter().map(|t| t.serialize()).collect()));
        match self.recv() {
            Message::ExecSymbolOk(_, _, _, ret) => {
                self.ctx.borrow_mut().retval = ret.as_ref().map(|t| Type::deserialize(t));
                Ok(())
            },
            Message::ExecSymbolErr(_, _, _, _e) => Err(Error::CallFailed),
            e => panic!("protocol error: expected exec result or error, got {:?}", e),
        }
    }

    pub fn lock_sym(&mut self, sym: &String) -> Result<bool, Error> {
        if self.sym_locks.get(sym).is_some() || self.globals.get(sym).is_some() {
            return Ok(true);
        }

        self.send(Message::LockSymbolRequest(self.sid, self.vid, self.uid, sym.clone()));
        match self.recv() {
            Message::LockSymbolStatus(_, _, _, success) => {
                if success {
                    self.sym_locks.insert(sym.clone(), self.lock_ctr);
                    self.lock_ctr += 1;
                }
                Ok(success)
            },
            Message::LockSymbolErr(_, _, _) => Err(Error::InvalidSymbol(sym.clone())),
            e => panic!("protocol error: expected lock result or error, got {:?}", e),
        }
    }

    pub fn lock_closed(&mut self, index: &u8) -> Result<bool, Error> {
        let cs = match &self.ctx.borrow().func {
            Executable::Closure(c) => c.clone(),
            Executable::Function(_) => return Err(Error::NotAClosure),
        };
        let ctx = cs.get_ctx().unwrap();

        if (*index as usize) >= ctx.len() {
            return Err(Error::InvalidClosed);
        }

        let cd = ctx.get(*index as usize).unwrap();
        if self.closed_locks.get(cd).is_some() ||
            (cd.0 == self.sid && cd.1 == self.vid &&
             self.closed.get(&cd.2).is_some()) {
            return Ok(true);
        }


        self.send(Message::LockClosedRequest(self.sid, self.vid, self.uid, *cd));
        match self.recv() {
            Message::LockClosedStatus(_, _, _, success) => {
                if success {
                    self.closed_locks.insert(*cd, self.lock_ctr);
                    self.lock_ctr += 1;
                }
                Ok(success)
            },
            Message::LockClosedErr(_, _, _) => Err(Error::InvalidClosed),
            e => panic!("protocol error: expected lock result or error, got {:?}", e),
        }
    }

    pub fn unlock_sym(&mut self, sym: &String) -> Result<(), Error> {
        if self.sym_locks.get(sym).is_none() {
            return Err(Error::InvalidSymbol(sym.clone()));
        } else if self.globals.get(sym).is_some() {
            return Ok(())
        }

        self.send(Message::UnlockSymbolRequest(self.sid, self.vid, self.uid, sym.clone()));
        match self.recv() {
            Message::UnlockSymbolOk(_, _, _) => {
                self.sym_locks.remove(sym);
                Ok(())
            },
            Message::UnlockSymbolErr(_, _, _) => Err(Error::InvalidSymbol(sym.clone())),
            e => panic!("protocol error: expected unlock result or err, got {:?}", e),
        }
    }

    pub fn unlock_closed(&mut self, index: &u8) -> Result<(), Error> {
        let cs = match &self.ctx.borrow().func {
            Executable::Closure(c) => c.clone(),
            Executable::Function(_) => return Err(Error::NotAClosure),
        };
        let ctx = cs.get_ctx().unwrap();

        if (*index as usize) >= ctx.len() {
            return Err(Error::InvalidClosed);
        }

        let cd = ctx.get(*index as usize).unwrap();
        if self.closed_locks.get(cd).is_none() {
            return Err(Error::InvalidClosed);
        } else if cd.0 == self.sid && cd.1 == self.vid &&
            self.closed.get(&cd.2).is_some() {
            return Ok(());
        }

        self.send(Message::UnlockClosedRequest(self.sid, self.vid, self.uid, *cd));
        match self.recv() {
            Message::UnlockClosedOk(_, _, _) => {
                self.closed_locks.remove(cd);
                Ok(())
            },
            Message::UnlockClosedErr(_, _, _) => Err(Error::InvalidClosed),
            e => panic!("protocol error: expected unlock result or err, got {:?}", e),
        }
    }
}
