use std::rc::Rc;
use serde::{Serialize, Deserialize};

use crate::function::Function;

#[derive(Debug, Serialize, Deserialize)]
pub struct ExecBlob {
    pub version: u8,
    pub id: String,
    pub func: Rc<Function>
}
