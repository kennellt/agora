use std::rc::Rc;
use serde::{Serialize, Deserialize};

use crate::error::Error;
use crate::function::Function;
use crate::instructions::InstructionResult;
use crate::types::{Type, ClosedType, Execute};

#[derive(Debug, Serialize, Deserialize)]
pub struct Closure {
    func: Rc<Function>,
    ctx: Vec<ClosedType>,
}

impl Closure {
    pub fn new(func: Rc<Function>, ctx: Vec<ClosedType>) -> Self {
        Closure { func: func, ctx: ctx }
    }
}

impl Execute for Closure {
    fn name(&self) -> String {
        self.func.name()
    }

    fn arity(&self) -> u8 {
        self.func.arity()
    }

    fn localct(&self) -> u8 {
        self.func.localct()
    }

    fn get_const(&self, idx: u8) -> Result<Type, Error> {
        self.func.get_const(idx)
    }

    fn instr_at(&self, idx: usize) -> InstructionResult {
        self.func.instr_at(idx)
    }

    fn max_instr_addr(&self) -> usize {
        self.func.max_instr_addr()
    }

    fn get_ctx(&self) -> Option<&Vec<ClosedType>> {
        Some(&self.ctx)
    }
}
