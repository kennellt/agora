use std::collections::HashMap;
use std::default::Default;
use std::fmt;
use base16ct;
use rand::prelude::*;
use sha2::{Sha256, Digest};
use serde::{Deserialize, Serialize};

pub type UserId = u64;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct User {
    pub id: UserId,
    pub name: String,
    pub passwd: String,
}

impl User {
    pub fn new(name: String, id: u64, passwd: String) -> Self {
        User {
            id: id,
            name: name,
            passwd: passwd,
        }
    }
}

impl fmt::Display for User {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "user '{}' (id {})", self.name, self.id)
    }
}

pub type UserToken = String;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct UserManager {
    users: HashMap<UserId, User>,
    names: HashMap<String, UserId>,
    tokens: HashMap<UserToken, UserId>,
}

impl UserManager {
    pub fn new(users: Vec<User>) -> Self {
        let mut rng = thread_rng();
        UserManager {
            users: users.iter().map(|u| (u.id, u.clone())).collect(),
            names: users.iter().map(|u| (u.name.clone(), u.id)).collect(),
            tokens: users.iter().map(|u| {
                let tmp: Vec<u8> = (0..31).map(|_| rng.gen::<u8>()).collect();
                (base16ct::lower::encode_string(&Sha256::digest(tmp)), u.id)
            }).collect(),
        }
    }

    pub fn get_user_by_id(&self, uid: &UserId) -> Option<&User> {
        self.users.get(uid)
    }

    pub fn get_user_by_token(&self, tok: &UserToken) -> Option<&User> {
        self.tokens.get(tok).map(|uid| self.users.get(uid)).flatten()
    }

    pub fn get_user_by_name(&self, name: &String) -> Option<&User> {
        self.names.get(name).map(|uid| self.users.get(uid)).flatten()
    }

    pub fn get_id_by_name(&self, name: &String) -> Option<&UserId> {
        self.names.get(name)
    }

    pub fn get_id_by_token(&self, tok: &UserToken) -> Option<&UserId> {
        self.tokens.get(tok)
    }

    pub fn check_name_and_passwd(&self, name: &String, passwd: &String) -> bool {
        match self.get_user_by_name(name) {
            Some(u) => {
                if base16ct::lower::encode_string(&Sha256::digest(passwd)) == u.passwd {
                    true
                } else {
                    false
                }
            },
            None => false
        }
    }
}

impl Default for UserManager {
    fn default() -> Self {
        UserManager {
            users: HashMap::new(),
            names: HashMap::new(),
            tokens: HashMap::new(),
        }
    }
}
