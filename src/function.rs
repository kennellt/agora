use std::rc::Rc;
use serde::{Serialize, Deserialize};

use crate::error::Error;
use crate::instructions::{Instruction, InstructionResult};
use crate::types::{Type, ClosedType, Execute};

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct Function {
    pub name: String,
    pub arity: u8,
    pub localct: u8,
    pub bytecode: Vec<u8>,
    pub consts: Vec<Type>,
}

impl Function {
    pub fn new(name: String, arity: u8, localct: u8, bytecode: Vec<u8>, consts: Vec<Type>) -> Rc<Self> {
        Rc::new(Function {
            name: name,
            arity: arity,
            localct: localct,
            bytecode: bytecode,
            consts: consts
        })
    }
}

impl Execute for Function {
    fn name(&self) -> String {
        self.name.clone()
    }

    fn arity(&self) -> u8 {
        self.arity
    }

    fn localct(&self) -> u8 {
        self.localct
    }

    fn get_const(&self, idx: u8) -> Result<Type, Error> {
        match self.consts.get(idx as usize) {
            Some(t) => Ok(t.clone()),
            None => Err(Error::InvalidConst(idx)),
        }
    }

    fn instr_at(&self, idx: usize) -> InstructionResult {
        Instruction::from_bytes(&self.bytecode, idx)
    }

    fn max_instr_addr(&self) -> usize {
        self.bytecode.len()
    }

    fn get_ctx(&self) -> Option<&Vec<ClosedType>> {
        None
    }
}
