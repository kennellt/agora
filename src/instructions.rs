use std::fmt;

use crate::error::Error;
use crate::opcodes::OpCode;

#[derive(Clone, Copy, PartialEq)]
pub enum Instruction {
    ZeroReg(OpCode),
    OneReg(OpCode, u8),
    TwoReg(OpCode, u8, u8),
    ThreeReg(OpCode, u8, u8, u8),
}

pub type InstructionResult = Result<Instruction, Error>;

macro_rules! get_reg {
    ($raw:expr, $idx:expr) => {
        $raw.get($idx).ok_or(Error::OutOfBytes($idx))?
    };
}

macro_rules! make_threereg {
    ($op:expr, $raw:expr, $idx:expr) => {
        Ok(Instruction::ThreeReg($op, *get_reg!($raw, $idx + 1),
                                 *get_reg!($raw, $idx + 2),
                                 *get_reg!($raw, $idx + 3)))
    };
}

macro_rules! make_tworeg {
    ($op:expr, $raw:expr, $idx:expr) => {
        Ok(Instruction::TwoReg($op, *get_reg!($raw, $idx + 1),
                               *get_reg!($raw, $idx + 2)))
    };
}

macro_rules! make_onereg {
    ($op:expr, $raw:expr, $idx:expr) => {
        Ok(Instruction::OneReg($op, *get_reg!($raw, $idx + 1)))
    };
}

impl Instruction {
    pub fn opcode(&self) -> OpCode {
        match *self {
            Instruction::ZeroReg(op)           => op,
            Instruction::OneReg(op, _)         => op,
            Instruction::TwoReg(op, _, _)      => op,
            Instruction::ThreeReg(op, _, _, _) => op,
        }
    }

    pub fn from_bytes(value: &Vec<u8>, idx: usize) -> InstructionResult  {
        if idx >= value.len() {
             return Err(Error::OutOfBytes(idx))
        }

        match OpCode::from(value[idx]) {
            OpCode::Nop => Ok(Instruction::ZeroReg(OpCode::Nop)),

            OpCode::Add => make_threereg!(OpCode::Add, value, idx),
            OpCode::Subtract => make_threereg!(OpCode::Subtract, value, idx),
            OpCode::Multiply => make_threereg!(OpCode::Multiply, value, idx),
            OpCode::Divide => make_threereg!(OpCode::Divide, value, idx),
            OpCode::Modulo => make_threereg!(OpCode::Modulo, value, idx),

            OpCode::And => make_threereg!(OpCode::And, value, idx),
            OpCode::Or => make_threereg!(OpCode::Or, value, idx),
            OpCode::Not => make_tworeg!(OpCode::Not, value, idx),
            OpCode::Xor => make_threereg!(OpCode::Xor, value, idx),

            OpCode::Less => make_threereg!(OpCode::Less, value, idx),
            OpCode::Equal => make_threereg!(OpCode::Equal, value, idx),
            OpCode::Greater => make_threereg!(OpCode::Greater, value, idx),
            OpCode::LessEqual => make_threereg!(OpCode::LessEqual, value, idx),
            OpCode::GreaterEqual => make_threereg!(OpCode::GreaterEqual, value, idx),
            OpCode::NotEqual => make_threereg!(OpCode::NotEqual, value, idx),
            OpCode::CmpType => make_threereg!(OpCode::CmpType, value, idx),

            OpCode::Car => make_tworeg!(OpCode::Car, value, idx),
            OpCode::Cdr => make_tworeg!(OpCode::Cdr, value, idx),
            OpCode::Cons => make_threereg!(OpCode::Cons, value, idx),

            OpCode::Push => make_onereg!(OpCode::Push, value, idx),
            OpCode::Pop => make_onereg!(OpCode::Pop, value, idx),

            OpCode::LoadConst => make_tworeg!(OpCode::LoadConst, value, idx),
            OpCode::LoadSym => make_tworeg!(OpCode::LoadSym, value, idx),
            OpCode::StoreSym => make_tworeg!(OpCode::StoreSym, value, idx),
            OpCode::LoadRet => make_onereg!(OpCode::LoadRet, value, idx),
            OpCode::LoadArg => make_threereg!(OpCode::LoadArg, value, idx),
            OpCode::StoreArg => make_threereg!(OpCode::StoreArg, value, idx),
            OpCode::LoadLocal => make_threereg!(OpCode::LoadLocal, value, idx),
            OpCode::StoreLocal => make_threereg!(OpCode::StoreLocal, value, idx),
            OpCode::Export => make_threereg!(OpCode::Export, value, idx),
            OpCode::Move => make_tworeg!(OpCode::Move, value, idx),
            OpCode::CloseSym => make_tworeg!(OpCode::CloseSym, value, idx),
            OpCode::CloseArg => make_threereg!(OpCode::CloseArg, value, idx),
            OpCode::CloseLocal => make_threereg!(OpCode::CloseLocal, value, idx),
            OpCode::LoadClosed => make_tworeg!(OpCode::LoadClosed, value, idx),
            OpCode::StoreClosed => make_tworeg!(OpCode::StoreClosed, value, idx),

            OpCode::Get => make_threereg!(OpCode::Get, value, idx),
            OpCode::Set => make_threereg!(OpCode::Set, value, idx),
            OpCode::Insert => make_threereg!(OpCode::Insert, value, idx),
            OpCode::Append => make_tworeg!(OpCode::Append, value, idx),
            OpCode::Remove => make_tworeg!(OpCode::Remove, value, idx),
            OpCode::Length => make_tworeg!(OpCode::Length, value, idx),

            OpCode::Call => make_tworeg!(OpCode::Call, value, idx),
            OpCode::TailCall => make_tworeg!(OpCode::TailCall, value, idx),
            OpCode::Return => make_onereg!(OpCode::Return, value, idx),
            OpCode::Jump => make_onereg!(OpCode::Jump, value, idx),
            OpCode::JumpIf => make_tworeg!(OpCode::JumpIf, value, idx),
            OpCode::JumpImm => make_onereg!(OpCode::JumpImm, value, idx),
            OpCode::JumpImmIf => make_tworeg!(OpCode::JumpImmIf, value, idx),

            OpCode::New => make_tworeg!(OpCode::New, value, idx),
            OpCode::StackVec => make_tworeg!(OpCode::StackVec, value, idx),
            OpCode::CloseFunc => make_threereg!(OpCode::CloseFunc, value, idx),
            OpCode::LitInt => make_tworeg!(OpCode::LitInt, value, idx),
            OpCode::DupClosed => make_tworeg!(OpCode::DupClosed, value, idx),

            OpCode::LockSym => make_tworeg!(OpCode::LockSym, value, idx),
            OpCode::UnlockSym => make_onereg!(OpCode::UnlockSym, value, idx),
            OpCode::LockClosed => make_tworeg!(OpCode::LockClosed, value, idx),
            OpCode::UnlockClosed => make_onereg!(OpCode::UnlockClosed, value, idx),

            OpCode::PrintReg => make_onereg!(OpCode::PrintReg, value, idx),

            op => Err(Error::InvalidInstruction(op.into_u8(), idx))
        }
    }
}

impl fmt::Display for Instruction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Instruction::ZeroReg(op) => write!(f, "{}", op),
            Instruction::OneReg(op, r1) => write!(f, "{} 0x{:0x}", op, r1),
            Instruction::TwoReg(op, r1, r2) => write!(f, "{} 0x{:0x} 0x{:0x}", op, r1, r2),
            Instruction::ThreeReg(op, r1, r2, r3) => write!(f, "{} 0x{:0x} 0x{:0x} 0x{:0x}", op, r1, r2, r3),
        }
    }
}
