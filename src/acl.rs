use std::fmt;
use serde::{Deserialize, Serialize};

// use crate::types::SerializedType;
use crate::user::UserId;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Acl {
    pub owner: UserId,
    pub read: bool,
    pub write: bool,
    pub exec: bool,
    // pub func: Option<SerializedType>,  // TODO: deserialize to Executable in metavisor
}

impl fmt::Display for Acl {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Acl({})({},{},{})({})", self.owner,
               self.read as u8, self.write as u8, self.exec as u8, false)  // TODO
               // self.func.is_some())
    }
}
