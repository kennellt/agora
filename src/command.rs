use serde::{Deserialize, Serialize};

use crate::execblob::ExecBlob;
use crate::mptraits::*;
use crate::resultblob::ResultBlob;
use crate::supervisor::SupervisorId;
use crate::vm::VmId;

pub type CommandToken = String;

#[derive(Debug, Deserialize)]
pub enum CommandRequest {
    AuthRequest(String, String),
    ListSupervisorsRequest(CommandToken),
    ListVmsRequest(CommandToken),
    CreateVmRequest(CommandToken),
    KillVmRequest(CommandToken, VmId),
    AttachVmRequest(CommandToken, VmId),
    LoadVmRequest(CommandToken, ExecBlob),
    RunVmRequest(CommandToken),
    PauseVmRequest(CommandToken),
    ResetVmRequest(CommandToken),
    DetachVmRequest(CommandToken),
}

impl RecvDeserialize for CommandRequest {}

#[derive(Debug, Serialize)]
pub enum CommandResponse {
    AuthResponse(Option<String>),
    ListSupervisorsResponse(Option<Vec<SupervisorId>>),
    ListVmsResponse(Option<Vec<(SupervisorId, VmId)>>),
    CreateVmResponse(Option<VmId>),
    KillVmResponse(bool),
    AttachVmResponse(Option<ResultBlob>),
    LoadVmResponse(bool),
    RunVmResponse(Option<ResultBlob>),
    PauseVmResponse(bool),
    ResetVmResponse(bool),
    DetachVmResponse(bool),
}

impl SendSerialize for CommandResponse {}
