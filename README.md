# Agora: A Multi-User Interpreter Backend

## What is Agora?
Agora is a bytecode virtual machine and two supervisory systems that
work together to allow multiple users to share bindings between their
interpreter sessions.  Users execute compiled bytecode as they would
under many interpreter environments for other dynamic langauges, with
the exception that this environment provides an extra layer of scope
which is shared across all users.  Currently there is only one such
scope, though in the future we hope to allow the creation of many of
these shared environments, with controls to determine access to them.

## How to use Agora
This repository provides all the code necessary to run the Agora
backend (contained under `src/`) along with a *very* primitive
example frontend (contained under `frontend/`).  (Seriously, this
compiler should *not* be considered production-ready).

### Building the Backend
The backend can be built with:
```
$ cargo build --release
```
(omit the `--release` flag to obtain a debug build).

### Running the Backend
The backend runs as two programs: the "supervisor," which handles
the execution of one or more VMs, and the "metavisor," which
tracks one or more (potentially network-distributed) supervisors.
These can be run (locally) with:
```
$ cargo run --release metavisor users.json
```
and
```
$ cargo run --release supervisor
```
Additionally, as seen above, the metavisor requires a JSON file
containing user information.  Here is an example user file; **note
that user IDs start at 1**:
```
[
    {
        "id": 1,
        "name": "test1",
        "passwd": "1b4f0e9851971998e732078544c96b36c3d01cedf7caa332359d6f1d83567014"
    }
]
```
The password field is the result of running e.g. `echo -n test1 | sha256sum`,
where "test1" was used as the password.

### Running the Sample Frontend
From inside the frontend directory, run the following (output shown
as an example of what to expect):
```
$ ./shell.py localhost
[agora shell] connecting to localhost on 6000...connected...
username: <e.g. test1>
password: <e.g. test1>
>
```
Once you have connected to a supervisor instance as above, you have
access to a minimal, LISP-like environment.  Below is an exmaple
of defining a `map` function:
```
> (define (map fn data) (if (empty? data)
                            '()
                            (cons (fn (car data))
                                  (map fn (cdr data)))))
> (map (lambda (x) (+ x 1) '(1 2 3)))
(2 3 4)
>
```
